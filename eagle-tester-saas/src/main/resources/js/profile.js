$(document).ready(function() {

    $.getJSON("../profile", function(data) {
        console.log(data);
        $("#username").text(data.name);
        $("#email").text(data.email);
        $("#organization").text(data.organization);
        $("#registrationTime").text(new Date(data.registrationTime).toLocaleDateString());
        $("#expirationTime").text(new Date(data.expirationTime).toLocaleDateString());
        if (data.paymentStatus == "canceled") $('#unSubscribeButton').css('display', 'none');
    });

    var unSubscribeButton = $('#unSubscribeButton');
    unSubscribeButton.click(function () {
        $("#msg").text("canceling, please wait... ");
        // $.post({
        //     url: '../payment/cancel',
        //     method: 'POST'
        // }).then(function(data) {
        //     console.log(data);
        //     if (data.status === 200) {
        //         $("#msg").text("Success. Redirecting...");
        //         window.location.href = "index.html";
        //     } else {
        //         $("#msg").text("Error cancel. Please try again. If the problem persists email eagle.tester@mobileenerlytics.com");
        //     }
        // });

        $.post("../payment/cancel",{},
            function(data,status){
                alert("Data: " + data + "\nStatus: " + status);
                if (status == "success") {
                    $("#msg").text("Success. Redirecting...");
                    window.location.href = "index.html";
                } else {
                    $("#msg").text("Error cancel. Please try again. If the problem persists email eagle.tester@mobileenerlytics.com");
                }
            });

    })

});