// From https://stackoverflow.com/questions/5448545/how-to-retrieve-get-parameters-from-javascript
function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

$(function(){
    $("#header_inst").load("instructions_header.html");
    $("#footer").load("footer.html");

    var subscriptionType = findGetParameter("subscription");

    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn'),
        allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);
        var $divId = $target.children().attr('id');
        $("#"+$divId).load("instructions_" + $divId + ".html", function(){
            if(subscriptionType == 'manual') {
                $(".server").not(".manual").remove();
                $(".cloud").not(".manual").remove();
                $(".step-jenkins").remove();
                $(".step-tests").remove();
                $(".step-view").remove();
            } else if(subscriptionType == 'server') {
                $(".cloud").not(".server").remove();
                $(".manual").not(".server").remove();
            } else {
                $(".server").not(".cloud").remove();
                $(".manual").not(".cloud").remove();
            }
        });
        navListItems.removeClass('btn-selected');
        $item.addClass('btn-selected');
        allWells.hide();
        $target.show();
        $('html, body').animate({
            scrollTop: $item.offset().top - 40
        }, 500);
    });

    allPrevBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            prevStepSteps = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");
            prevStepSteps[0].click();
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepSteps = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a");
            nextStepSteps[0].click();
    });


     if(subscriptionType == 'server') {
             $("div.setup-panel div a")[0].click();
         } else {
             $("div.setup-panel div a")[1].click();
     }
});
