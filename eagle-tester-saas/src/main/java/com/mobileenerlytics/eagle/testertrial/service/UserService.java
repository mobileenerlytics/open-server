package com.mobileenerlytics.eagle.testertrial.service;

import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.service.ProjectService;
import com.mobileenerlytics.eagle.testerpro.util.Constants;
import com.mobileenerlytics.eagle.testertrial.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProjectService projectService;


    public boolean createUser(String email, String username, String firstname, String lastname, String pass, String organization, String roleName) {
        logger.debug("createUser: {} {} {} {} {}", email, username, pass, organization);

        if (userRepository.findByUsername(username) != null) {
            logger.error("User already exists for username: {}", username);
            return false;
        }
        if(userRepository.findByEmail(email) != null) {
            logger.error("User already exists for email: {}", email);
            return false;
        }

        final User user = new User(email, username, firstname, lastname, User.hashPassword(pass), organization,
                new java.util.Date(), roleName);
        userRepository.save(user);
        Project project = projectService.queryOneOrCreate("default", username);
        if (roleName.equals(Constants.USER_ROLE_MANUAL)) {
            projectService.updateProjectAttribute(project.getId(), "tourStatus", "false");
        }

        return true;
    }

    public boolean updateUserAttribute(User user, String attribute, String value) {
        switch (attribute) {
            case "email":
                user.setEmail(value);
                userRepository.save(user);
                return true;
            case "firstname":
                user.setFirstname(value);
                userRepository.save(user);
                return true;
            case "lastname":
                user.setLastname(value);
                userRepository.save(user);
                return true;
            case "organization":
                user.setOrganization(value);
                userRepository.save(user);
                return true;
            default:
                return false;
        }
    }

    public User queryUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User queryUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User queryUserByResetToken(String resetToken) { return userRepository.findByResetToken(resetToken); }

    public void saveUser(User user) { userRepository.save(user); }
}
