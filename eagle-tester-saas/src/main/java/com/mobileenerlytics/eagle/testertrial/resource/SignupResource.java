package com.mobileenerlytics.eagle.testertrial.resource;

import com.mobileenerlytics.eagle.testerpro.service.CRMService;
import com.mobileenerlytics.eagle.testerpro.util.Constants;
import com.mobileenerlytics.eagle.testertrial.service.UserService;
import org.apache.commons.validator.routines.EmailValidator;
import java.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.*;

@Component
@Path("/signup")
public class SignupResource {
    private static Logger logger = LoggerFactory.getLogger(SignupResource.class);

    @Autowired
    private UserService userService;

    @Autowired
    private CRMService crmLogger;

    @POST
    public Response post(@Context HttpHeaders headers,
                         @QueryParam("org") String org,
                         @QueryParam("firstname") String firstname,
                         @QueryParam("lastname") String lastname,
                         @QueryParam("role") String role
                         ) {
        Set<String> roles = new HashSet<>(Arrays.asList(Constants.USER_ROLE_DEFAULT, Constants.USER_ROLE_DEMO, Constants.USER_ROLE_MANUAL));
        if (!roles.contains(role)) role = Constants.USER_ROLE_DEFAULT;
        final String authHeader = headers.getHeaderString(HttpHeaders.AUTHORIZATION);
        if (authHeader == null) {
            String msg = "Signup attempted without user credentials in auth header.";
            logger.error(msg);
            throw new BadRequestException(msg);
        }

        final String credentialsStr = authHeader.split(" ")[1];
        if(credentialsStr == null || credentialsStr.length() == 0) {
            String msg = "Signup attempted with invalid data in auth header.";
            logger.error(msg);
            throw new BadRequestException(msg);
        }

        // username:password:email format accepted
        final String[] credentials = new String(Base64.getDecoder().decode(credentialsStr)).split(":");
        if (credentials.length != 3) {
            String msg = "Signup attempted with incomplete data in auth header.";
            logger.error(msg);
            throw new BadRequestException(msg);
        }

        String username = credentials[0];
        String password = credentials[1];
        String email = credentials[2];

        if (userService.queryUserByUsername(username) != null) {
            String msg = "An account already exists with this username.";
            logger.error(msg);
            throw new BadRequestException(msg);
        }

        if(userService.queryUserByEmail(email) != null) {
            String msg = "An account already exists with this email address.";
            logger.error(msg);
            throw new BadRequestException(msg);
        }

        if (username.length() < 4) {
            String msg = "Signup attempted with username smaller than 4 characters.";
            logger.error(msg);
            throw new BadRequestException(msg);
        }

        if (password.length() < 8) {
            String msg = "Signup attempted with password smaller than 8 characters.";
            logger.error(msg);
            throw new BadRequestException(msg);
        }

        if(! EmailValidator.getInstance().isValid(email)) {
            String msg = "Signup attempted with invalid email.";
            logger.error(msg + " {}", email);
            throw new BadRequestException(msg);
        }

        // All data consistency check is done.


        boolean success = userService.createUser(email, username, firstname, lastname, password, org, role);
        if(!success)
            throw new InternalServerErrorException("An error was encountered during signup. Please try again later.");

        Map<String, String> properties = new HashMap<>();
        properties.put(CRMService.PROP_EMAIL, email);
        properties.put(CRMService.PROP_FIRSTNAME, firstname);
        properties.put(CRMService.PROP_LASTNAME, lastname);
        properties.put(CRMService.PROP_COMPANY, org);
        properties.put(CRMService.PROP_ROLE, role);

        crmLogger.logPropertyUpdate(email, properties);
        crmLogger.logLifecycleEvent(email, CRMService.CRMLifecyleEvent.SIGNUP);

        return Response.ok().build();
    }
}
