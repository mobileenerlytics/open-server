package com.mobileenerlytics.eagle.testertrial.resource;

import com.mobileenerlytics.eagle.testerpro.service.EmailService;
import com.mobileenerlytics.eagle.testertrial.entity.User;
import com.mobileenerlytics.eagle.testertrial.service.UserService;
import java.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

@Component
@Path("reset")
public class PassResetResource {
    private static Logger logger = LoggerFactory.getLogger(PassResetResource.class);

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    /** Re-usable function to generate a password reset token string using a random UUID. Redundant check
       to ensure generated token is not already used. Encodes generated token as Base64-URL compatible. **/

    private String generateResetToken() {
        String unique;
        int resetCounter = 0;
        do {
            unique = UUID.randomUUID().toString();
            resetCounter ++;
        } while(resetCounter < 3 && userService.queryUserByResetToken(unique) != null);
        if(resetCounter == 3) {
            return null;
        }
        return Base64.getUrlEncoder().withoutPadding().encodeToString(unique.getBytes());
    }

    /** Main POST body at default path (reset/pass). Accepts email address as input from HTML form. **/
    @POST
    @Path("pass")
    public Response post(@QueryParam("email") String email) {
        /** Check to see if a user exists with the specified email. If so, query by email for the user. **/
        User user = userService.queryUserByEmail(email);
        if(user == null)
            throw new NotAcceptableException("This email address does not belong to an existing account.");

        /** Generate a new reset token and its corresponding reset URL and set the expiration date to one day from now. **/
        String newResetToken = generateResetToken();
        if (newResetToken == null) {
            throw new ServerErrorException("Failed to generate a reset token", INTERNAL_SERVER_ERROR);
        }

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 1);
        String newResetURL = emailService.generateResetURL(newResetToken);

        /** Assign token value and expiration date/time to user requesting the reset **/
        user.setResetToken(newResetToken);
        user.setResetTokenExpire(c.getTime());

        /** Update the user model with token information **/
        userService.saveUser(user);

        /** Create email subject and body. Inject unique reset url for user. **/
        String subject = "Mobile Enerlytics Tester Password Reset";
        String body = "Hello " + user.getFirstname() + ",\n\n" +
                "We received a request to reset your password. Below is your temporary password reset link. \n" +
                "This link will expire in 24 hours. Please follow the link to reset your password immediately. \n\n" +
                "Username: " + user.getUsername() + "\n" +
                "Temporary Reset Link: " + newResetURL + "\n\n" +
                "If you did not make this request, please ignore this email.\n\n\n" +
                "Mobile Enerlytics \n" +

                "support@mobileenerlytics.com";

        /** Use existing email service to send the email message to the user from default noreply account **/
        try {
            if(!emailService.sendEmail(email, body, subject))
                throw new ServerErrorException("Failed to send email", INTERNAL_SERVER_ERROR);
        } catch (MessagingException e) {
            logger.error("Error encountered: " + e);
            throw new ServerErrorException("Failed to send email", INTERNAL_SERVER_ERROR);
        }

        return Response.ok().build();
    }

    /** Post body for sub-path (reset/new) for handling password update once link has been clicked on by user **/
    @POST
    @Path("new")
    public Response post(@Context HttpHeaders headers, @QueryParam("resetToken") String resetToken, @QueryParam("p1") String passHash1, @QueryParam("p2") String passHash2) {
        /** Query user by their reset token. **/
        /** Assign temporary variables for the current date/time and the user's reset token expiration date/time **/
        User user = userService.queryUserByResetToken(resetToken);
        if(user == null) {
            String msg = "Invalid reset link. Please request a new token. Redirecting to forgot password page..";
            logger.error(msg);
            throw new NotAcceptableException(msg);
        }

        Date now = new Date();
        Date resetExpired = user.getResetTokenExpire();

        /** Check to see if the reset token expiration date has already passed. If so, set the values
         for the user's reset token and its expiration date/time to null so that the reset token becomes
         invalid and the user can request a new reset token and link. **/
        if (resetExpired.before(now)) {
            user.setResetToken(null);
            user.setResetTokenExpire(null);

            /** Update the user model with null token and token expiration date/time **/
            userService.saveUser(user);

            String msg = "Password change attempted with an expired reset link. Please request a new reset link.";
            logger.error(msg);
            throw new NotAcceptableException(msg);
        }

        /** Decode new password from hash in url parameters **/
        String newPass1 = new String(Base64.getDecoder().decode(passHash1));
        String newPass2 = new String(Base64.getDecoder().decode(passHash2));


        /** Redundant checks to ensure entered passwords are the same and meet the length requirements **/
        if (!newPass1.equals(newPass2)) {
            String msg = "Password reset attempted with mismatching new passwords.";
            logger.error(msg);
            throw new NotAcceptableException(msg);
        }

        if (newPass1.length() < 8) {
            String msg = "Password reset attempted with new password smaller than 8 characters.";
            logger.error(msg);
            throw new NotAcceptableException(msg);
        }

        /** Set the user's password to be the new password that they entered. **/
        user.setPassword(User.hashPassword(newPass1));
        user.setResetToken(null);
        user.setResetTokenExpire(null);

        /** Update the user model with new password. **/
        userService.saveUser(user);
        return Response.ok().build();
    }
}