package com.mobileenerlytics.eagle.testertrial.resource;

import com.mobileenerlytics.eagle.testerpro.util.web.HeaderUtils;
import com.mobileenerlytics.eagle.testertrial.entity.User;
import com.mobileenerlytics.eagle.testertrial.service.LicenseService;
import com.mobileenerlytics.eagle.testertrial.service.PayService;
import com.mobileenerlytics.eagle.testertrial.service.UserService;
import net.nicholaswilliams.java.licensing.License;
import net.nicholaswilliams.java.licensing.encryption.PasswordProvider;
import net.nicholaswilliams.java.licensing.encryption.PrivateKeyDataProvider;
import net.nicholaswilliams.java.licensing.exception.KeyNotFoundException;
import net.nicholaswilliams.java.licensing.licensor.LicenseCreator;
import net.nicholaswilliams.java.licensing.licensor.LicenseCreatorProperties;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Date;

@Component
@Path("api/license")
public class LicenseResource implements PrivateKeyDataProvider, PasswordProvider {
    @Autowired
    private HeaderUtils headerUtils;

    @Autowired
    private UserService userService;

    @Autowired
    private LicenseService licenseService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLicense(@Context HttpHeaders httpHeaders) {
        String userName = headerUtils.getUsernameFromHeader(httpHeaders);
        User user = userService.queryUserByUsername(userName);
        byte[] licenseData = licenseService.createLicense(user);
        String trns = Base64.encodeBase64String(licenseData);
        return Response.status(Response.Status.OK).entity(trns).build();
    }

    @Override
    public byte[] getEncryptedPrivateKeyData() throws KeyNotFoundException {
        try {
            return IOUtils.toByteArray(LicenseResource.class.getResourceAsStream(
                    "/certs/enerlytics.private.key"));
        } catch (IOException e) {
            throw new KeyNotFoundException(
                    "The private key file was not found.", e);
        }
    }

    @Override
    public char[] getPassword() {
        return "ycndajxc".toCharArray();
    }
}
