package com.mobileenerlytics.eagle.testertrial.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mobileenerlytics.eagle.testerpro.util.Constants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.security.auth.Subject;
import java.security.Principal;
import java.util.Date;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "User")
@Getter @Setter @NoArgsConstructor
public class User implements Principal {
    @Id
    @JsonProperty("_id")
    private String id;

    @JsonIgnore
    @Indexed(unique = true, sparse = true)
    @Field("resetToken")
    private String resetToken;

    @JsonIgnore
    @Field("resetTokenExpire")
    private Date resetTokenExpire;

    @Indexed(unique = true)
    @Field("email")
    private String email;

    @Indexed(unique=true)
    @Field("username")
    private String username;

    @Field("role")
    private String role;

    @Field("lastname")
    private String lastname;

    @Field("firstname")
    private String firstname;

    // Don't send password over in /api/profile
    @JsonIgnore
    @Field("password")
    private String password;

    @Field("organization")
    private String organization;

    @Field("registrationTime")
    private Date registrationTime;

    @JsonIgnore
    @Field("payCustomerId")
    private String payCustomerId; // set as "DebugPayId" will not need pay.

    @Field("expiredDate")
    private Date expiredDate; // if expiredDate exist, then will be idenified contrator customer

    public User(String email, String username, String firstname, String lastname,  String password, String organization, Date registrationTime,
                String role) {
        this.email = email;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.organization = organization;
        this.registrationTime = registrationTime;
        this.role = (role == null) ? role : Constants.USER_ROLE_DEFAULT;
    }

    public boolean isPassword(String givenPassword) {
        String hashedPassword = hashPassword(givenPassword);
        return password.equals(hashedPassword);
    }

    public static String hashPassword(String password_) {
        final SHA3.DigestSHA3 md = new SHA3.DigestSHA3(256);
        md.update(password_.getBytes());
        return Hex.toHexString(md.digest());
    }

    @Override
    public String getName() {
        return username;
    }

    @Override
    public boolean implies(Subject subject) {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
