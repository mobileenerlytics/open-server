package com.mobileenerlytics.eagle.testertrial.service;

import com.mobileenerlytics.eagle.testerpro.util.Constants;
import com.mobileenerlytics.eagle.testertrial.entity.User;
import com.stripe.exception.StripeException;
import com.stripe.model.*;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Log4j
public class PayService {

    @Autowired
    private UserService userService;

    public Subscription createSubscription(String planId, String couponId, String stripeToken, User user) throws StripeException {
        Subscription subscription;
        Map<String, Object> subscriptionParams = new HashMap<String, Object>();

        // items
        Customer customer = createCustomer(user, stripeToken);
        Plan plan = createPlan(planId);
        Map<String, Object> itemParams = new HashMap<String, Object>();
        itemParams.put("plan", plan.getId());
        Map<String, Object> items = new HashMap<String, Object>();
        items.put("0", itemParams);

        // apply Coupon
        if (couponId != null && !couponId.isEmpty()) {
            Coupon.retrieve(couponId);  // throws an exception if the coupon is invalid
            subscriptionParams.put("coupon", couponId);
        }

        // create subscription
        subscriptionParams.put("customer", customer.getId());
        subscriptionParams.put("items", items);
        subscription = Subscription.create(subscriptionParams);
        return subscription;
    }

    public Customer createCustomer(User user, String token) throws StripeException {
        String customerID = user.getPayCustomerId();
        Customer customer = null;
        if (customerID == null) {
            Map<String, Object> customerParams = new HashMap<>();
            customerParams.put("email", user.getEmail());
            customer = Customer.create(customerParams);

            Map<String, Object> cardParams = new HashMap<>();
            cardParams.put("source", token);
            customer.getSources().create(cardParams);
            user.setPayCustomerId(customer.getId());
            userService.saveUser(user);
        } else
            customer = Customer.retrieve(customerID);
        return customer;
    }

    public Plan createPlan(String planId) throws StripeException {
        Plan plan = null;
        plan = Plan.retrieve(planId);
        return plan;
    }


    public double getPrice(int mau, int monthCnt) {
        if (monthCnt == 12) return 10 * mau * 100;
        else return mau * monthCnt * 100;
    }

    public static boolean isValid(User user) {
        Date expirationDate = getExpiration(user);
        if(expirationDate == null) {
            return false;
        }

        return expirationDate.after(new Date());
    }

    public static Date getExpiration(User user) {
        if (user.getExpiredDate() != null) {
            return user.getExpiredDate();
        }

        if (user.getPayCustomerId() == null) return null;
        if (user.getRole() != null && (user.getRole().equals(Constants.USER_ROLE_DEMO)
                || user.getRole().equals(Constants.USER_ROLE_MANUAL))) {
            log.info("The role of user " + user.getUsername() + " is: " + user.getRole()
                    + ", donot need check payment status!");
            return DateUtils.addDays(new Date(), 1);
        }
        if (user.getPayCustomerId().equals("DebugPayId")) {
            log.info("The user " + user.getUsername() + " is for debug, skip checking payment status!");
            return DateUtils.addDays(new Date(), 1);
        }

        Customer customer = null;
        Subscription subscription = null;
        long expiredTime = 0;
        try {
            customer = Customer.retrieve(user.getPayCustomerId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        CustomerSubscriptionCollection subscriptions = customer.getSubscriptions();
        if (subscriptions.getData().size() == 0) {
            return null;
        }
        subscription = subscriptions.getData().get(0);
        expiredTime = subscription.getCurrentPeriodEnd();
        // todo
        // user.setPaymentAccount(subscription.getPlan().getAmount());
        return new Date(expiredTime * 1000);
    }

    public boolean isFreeTrial(final String couponId) throws StripeException {
        if (couponId == null || couponId.isEmpty())
            return false;

        Coupon coupon = Coupon.retrieve(couponId);
        return coupon.getPercentOff().equals(new BigInteger("100"));
    }
}
