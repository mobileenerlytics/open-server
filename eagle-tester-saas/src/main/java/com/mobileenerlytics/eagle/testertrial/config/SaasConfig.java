package com.mobileenerlytics.eagle.testertrial.config;

import com.mobileenerlytics.eagle.testerpro.util.web.HeaderUtils;
import com.mobileenerlytics.eagle.testertrial.util.SaasHeaderUtils;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("saas")
public class SaasConfig {
    @Bean
    public ResourceConfig getResourceConfig() {
        return new SaasJerseyConfig();
    }

    @Bean
    public HeaderUtils getUtil() {
        return new SaasHeaderUtils();
    }
}
