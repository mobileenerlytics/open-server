package com.mobileenerlytics.eagle.testertrial.service;

import com.mobileenerlytics.eagle.testertrial.entity.User;
import com.mobileenerlytics.eagle.testertrial.resource.LicenseResource;
import net.nicholaswilliams.java.licensing.License;
import net.nicholaswilliams.java.licensing.licensor.LicenseCreator;
import net.nicholaswilliams.java.licensing.licensor.LicenseCreatorProperties;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class LicenseService {

    @Autowired
    private PayService payService;

    public byte[] createLicense(User user) {
        LicenseCreatorProperties.setPrivateKeyDataProvider(new LicenseResource());
        LicenseCreatorProperties.setPrivateKeyPasswordProvider(new LicenseResource());
        LicenseCreator creator = LicenseCreator.getInstance();

        // To account for possible timezone changes
        Date dayAfterExpiration = DateUtils.addDays(payService.getExpiration(user), 1);
        Date registrationDate = user.getRegistrationTime();
        Date dayBeforeRegistration = DateUtils.addDays(registrationDate, -1);

        License license = new License.Builder().
                withIssuer("Mobile Enerlytics").
                withNumberOfLicenses(1).
                withHolder(user.getUsername()).
                addFeature("EAGLE").
                withGoodAfterDate(dayBeforeRegistration.getTime()).
                withGoodBeforeDate(dayAfterExpiration.getTime()).
                withIssueDate(registrationDate.getTime()).
                build();

        byte[] licenseData = LicenseCreator.getInstance().signAndSerializeLicense(license, "ycndajxc".toCharArray());
        return licenseData;
    }
}
