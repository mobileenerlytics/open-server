package com.mobileenerlytics.eagle.testertrial.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

@Path("api/auth")
public class AuthResource {

    @Context
    HttpHeaders httpHeaders;

    @Context
    ContainerRequestContext requestContext;

    @GET
    public Response Get() {
        // todo , in future , we can generate this token, with the exipation time. and check it without query the db.
        String token = null;

        final String authzHeader = httpHeaders.getHeaderString(HttpHeaders.AUTHORIZATION);
        if(authzHeader != null) {
            String[] userPass = authzHeader.split(" ");
            token = userPass[1];
        }

        String host = requestContext.getUriInfo().getBaseUri().getHost();
        String domainName = host.equals("localhost") ? "" : ";domain=.mobileenerlytics.com";
        String cookie = "eagleHash=" + token + domainName + ";path=/";

        return Response.ok().entity(cookie).build();
    }
}
