package com.mobileenerlytics.eagle.testertrial;


import com.mobileenerlytics.eagle.testerpro.ProMain;
import com.mobileenerlytics.eagle.testerpro.util.web.StaticResource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrialMain extends ProMain {
    public static void main(String args[]) throws Exception {
        SpringApplication app = new SpringApplication(TrialMain.class);
        app.setAdditionalProfiles("saas");
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        StaticResource.otherResolvers = this.getClass();
        super.run(args);
    }
}
