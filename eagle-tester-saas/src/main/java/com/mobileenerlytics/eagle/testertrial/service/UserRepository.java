package com.mobileenerlytics.eagle.testertrial.service;

import com.mobileenerlytics.eagle.testertrial.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    User findByResetToken(String resetToken);
    User findByUsername(String username);
    User findByEmail(String email);
}
