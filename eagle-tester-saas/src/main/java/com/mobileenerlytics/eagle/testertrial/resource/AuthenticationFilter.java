package com.mobileenerlytics.eagle.testertrial.resource;

import com.mobileenerlytics.eagle.testertrial.entity.User;
import com.mobileenerlytics.eagle.testertrial.service.PayService;
import com.mobileenerlytics.eagle.testertrial.service.UserService;
import com.mobileenerlytics.eagle.testertrial.util.TrialSecurityContext;
import java.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.*;
import javax.ws.rs.ext.Provider;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

@Component
@Provider
@Priority(Priorities.AUTHENTICATION)
@PreMatching
// needs to happen before authorization
public class AuthenticationFilter implements ContainerRequestFilter {
    private static Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);
    private static final String COOKIE_KEY = "eagleHash";

    @Autowired
    private UserService userService;

    @Autowired
    private PayService payService;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        final List<PathSegment> segmentList = requestContext.getUriInfo().getPathSegments();
        final PathSegment first = segmentList.get(0);
        String firstStr = first.getPath();
        if (!firstStr.equals("api") && !firstStr.equals("") && !firstStr.equals("index.html")) return;

        // todo will define the api access level in future, auth api donot need pay.
        boolean isAuth = firstStr.equals("api") && segmentList.get(1).getPath().equals("auth");
        boolean isApi = firstStr.equals("api");
        final SecurityContext oldContext = requestContext.getSecurityContext();
        final String authzHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        String hash = null;
        if(authzHeader != null) { // when we do the login, or doing an upload
            String[] userPass = authzHeader.split(" ");
            hash = userPass[1];
        } else {  // when we query api, with the cookies
            Map<String, Cookie> keyValue = requestContext.getCookies();
            if(keyValue.containsKey(COOKIE_KEY)) {
                Cookie cookie = keyValue.get(COOKIE_KEY);
                hash = cookie.getValue();
            }
        }

        if (hash == null) {
            String msg = "Error, no login info.";
            logger.info(msg);
            Response response = Response.status(Response.Status.UNAUTHORIZED).entity(msg).build();
            requestContext.abortWith(response);
        }
        else {
            String decoded = new String(Base64.getDecoder().decode(hash));
            String[] split = decoded.split(":");
            User user = userService.queryUserByUsername(split[0]);
            if (user == null )  {
                String msg = "Error, no such user exists, please check your username!";
                logger.info(msg);
                Response response = Response.status(Response.Status.UNAUTHORIZED).entity(msg).build();
                requestContext.abortWith(response);
            }
            else if (!user.isPassword(split[1])) {
                String msg = "Error, password is not matched with the user name, please check your username and password!";
                logger.info(msg);
                Response response = Response.status(Response.Status.UNAUTHORIZED).entity(msg).build();
                requestContext.abortWith(response);
            }
            else if (!payService.isValid(user) && !user.getRole().equals("manual")) {
                String msg = "Error, payment required to access our product!";
                logger.info(msg);
                Response response = Response.status(Response.Status.PAYMENT_REQUIRED).entity(msg).build();
                requestContext.abortWith(response);
            }
            else {
                logger.info("Auth successfully");
                requestContext.setSecurityContext(new TrialSecurityContext(user, oldContext));
                return;
            }
        }
        if (!isApi)
            handleUrlRequest(requestContext);
    }

    private void handleUrlRequest(ContainerRequestContext requestContext) {
        // todo , add error page here, then can tell the user failture reason, [token failed, payment status failed]
        String page = "login.html";
        try {
            URI redirectUri = new URI(requestContext.getUriInfo().getBaseUri().toString() + page);
            Response response = Response.temporaryRedirect(redirectUri).entity("Auth failed, redirecting to login").build();
            requestContext.abortWith(response);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }


}
