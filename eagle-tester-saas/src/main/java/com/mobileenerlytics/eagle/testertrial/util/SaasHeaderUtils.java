package com.mobileenerlytics.eagle.testertrial.util;

import com.mobileenerlytics.eagle.testerpro.util.web.HeaderUtils;
import java.util.Base64;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import java.util.Map;

public class SaasHeaderUtils extends HeaderUtils {
    private static final String COOKIE_KEY = "eagleHash";

    @Override
    public String getUsernameFromHeader(HttpHeaders httpHeaders) {
        String hash = null;
        final String authzHeader = httpHeaders.getHeaderString(HttpHeaders.AUTHORIZATION);
        if(authzHeader != null) {
            String[] userPass = authzHeader.split(" ");
            hash = userPass[1];
        } else {
            Map<String, Cookie> keyValue = httpHeaders.getCookies();
            if(keyValue.containsKey(COOKIE_KEY)) {
                Cookie cookie = keyValue.get(COOKIE_KEY);
                hash = cookie.getValue();
            }
        }
        if(hash == null)
            throw new ForbiddenException("User not logged in");
        String decoded = new String(Base64.getDecoder().decode(hash));
        String[] split = decoded.split(":");
        if(split == null || split.length == 0)
            throw new ForbiddenException("User not logged in");

        return split[0];
    }
}
