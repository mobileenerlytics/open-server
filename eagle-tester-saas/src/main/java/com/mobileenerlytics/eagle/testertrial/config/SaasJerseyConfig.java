package com.mobileenerlytics.eagle.testertrial.config;

import com.mobileenerlytics.eagle.testerpro.util.web.CORSResponseFilter;
import com.mobileenerlytics.eagle.testertrial.resource.AuthenticationFilter;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

public class SaasJerseyConfig extends ResourceConfig {

    public SaasJerseyConfig() {
        register(MultiPartFeature.class);
        register(CORSResponseFilter.class);
        register(LoggingFeature.class);
        register(RolesAllowedDynamicFeature.class);
        register(AuthenticationFilter.class);
        property(ServerProperties.RESPONSE_SET_STATUS_OVER_SEND_ERROR, true);
        packages("com.mobileenerlytics.eagle.testerpro", "com.mobileenerlytics.eagle.testertrial");
    }
}
