package com.mobileenerlytics.eagle.testertrial.resource;

import com.mobileenerlytics.eagle.testerpro.util.web.HeaderUtils;
import com.mobileenerlytics.eagle.testertrial.entity.User;
import com.mobileenerlytics.eagle.testertrial.service.UserService;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("api/profile")
public class ProfileResource {
    private static Logger logger = LoggerFactory.getLogger(ProfileResource.class);

    @Autowired
    private UserService userService;

    @Autowired
    HeaderUtils headerUtils;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserProfile(
            @Context HttpHeaders httpHeaders,
            @QueryParam("token") String token ) {
        String userName = headerUtils.getUsernameFromHeader(httpHeaders);
        User user = userService.queryUserByUsername(userName);
        return Response.ok(user).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{userName}/{attribute}/{value}")
    @RolesAllowed("default")
    public Response post(
            @Context HttpHeaders httpHeaders,
            @PathParam("userName") String userName,
            @PathParam("attribute") String attribute,
            @PathParam("value") String value,
            @FormDataParam("password") String password // optional ,required only when user is trying to update password
    ){
        User user = userService.queryUserByUsername(userName);
        if ((attribute.equals("password"))) {
            value = User.hashPassword(value);
            if (!user.isPassword(password)) {
                logger.info("password is not matched");
                throw new ForbiddenException("Error, password does not match with the user name, please check your password!");
            }
        }

        boolean updateUser = userService.updateUserAttribute(user, attribute, value);
        if (!updateUser)
            throw new InternalServerErrorException("Error occurred while updating the user");

        return Response.ok().build();
    }

}
