package com.mobileenerlytics.eagle.testertrial.util;

import com.mobileenerlytics.eagle.testerpro.util.web.ProSecurityContext;
import com.mobileenerlytics.eagle.testertrial.entity.User;

import javax.ws.rs.core.SecurityContext;

public class TrialSecurityContext extends ProSecurityContext {
    private final User user;

    public TrialSecurityContext(User user, SecurityContext oldContext) {
        super(oldContext);
        this.user = user;
    }

    @Override
    public User getUserPrincipal() {
        return this.user;
    }

    @Override
    public String getAuthenticationScheme() {
        return SecurityContext.BASIC_AUTH;
    }

    @Override
    public boolean isUserInRole(String role) {
        return user.getRole().equals(role);
    }

    public User getUser() {return this.user;}

}
