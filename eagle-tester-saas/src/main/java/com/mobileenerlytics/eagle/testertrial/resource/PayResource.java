package com.mobileenerlytics.eagle.testertrial.resource;


import com.mobileenerlytics.eagle.testerpro.service.CRMService;
import com.mobileenerlytics.eagle.testerpro.util.Constants;
import com.mobileenerlytics.eagle.testerpro.util.web.HeaderUtils;
import com.mobileenerlytics.eagle.testertrial.entity.User;
import com.mobileenerlytics.eagle.testertrial.service.PayService;
import com.mobileenerlytics.eagle.testertrial.service.UserService;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.*;
import org.apache.commons.lang.time.DateUtils;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.math.BigInteger;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@Path("payment")
public class PayResource {

    @Autowired
    private UserService userService;

    @Autowired
    private PayService payService;

    @Value("${stripe.apiKey}")
    public void setApiKey(String apiKey) {
        Stripe.apiKey = apiKey;
    }

    @Autowired
    private CRMService crmLogger;

    @Autowired
    private HeaderUtils headerUtils;

    @POST
    @Path("cancel")
    public Response post(
            @Context HttpHeaders httpHeaders) {
        httpHeaders.getCookies();
        String userName = headerUtils.getUsernameFromHeader(httpHeaders);
        User user = userService.queryUserByUsername(userName);
        try {
            Customer customer = Customer.retrieve(user.getPayCustomerId());
            CustomerSubscriptionCollection subscriptions = customer.getSubscriptions();
            if (subscriptions.getData().size() == 0)
                throw new BadRequestException("server failed, cannot retrieve subscriptions");

            Subscription sub = subscriptions.getData().get(0);
            Map<String, Object> params = new HashMap<>();
            params.put("at_period_end", true); // cancel at the period end
            sub = sub.cancel(params);
            userService.saveUser(user);
        } catch (StripeException exception) {
            exception.printStackTrace();
            throw new BadRequestException(exception.getMessage());
        }
        return Response.ok().build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("plan")
    public Response plan(
            @QueryParam("planId") String planId) {
        if (planId == null || planId.isEmpty())
            throw new BadRequestException();
        try {
            Plan plan = Plan.retrieve(planId);
            return Response.ok().entity(plan).build();
        } catch (StripeException e) {
            e.printStackTrace();
            throw new BadRequestException(e.getMessage());
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("coupon")
    public Response coupon(
            @QueryParam("couponId") String couponId) {
        if (couponId == null || couponId.isEmpty())
            throw new BadRequestException();
        try {
            Coupon coupon = Coupon.retrieve(couponId);
            return Response.ok().entity(coupon).build();
        } catch (StripeException e) {
            e.printStackTrace();
            throw new BadRequestException(e.getMessage());
        }
    }

    @POST
    public Response post(
            @Context SecurityContext context,
            @Context HttpHeaders httpHeaders,
            @FormDataParam("coupon") String couponId,
            @FormDataParam("plan") String planId,
            @FormDataParam("stripeToken") String stripeToken) {

        // retrieve user
        String userName = headerUtils.getUsernameFromHeader(httpHeaders);
        User user = userService.queryUserByUsername(userName);
        if (user == null)
            throw new ForbiddenException("Before starting payment, please login first.");

        // request payment to stripe
        Subscription subscription = null;
        try {
            subscription = payService.createSubscription(planId, couponId, stripeToken, user);
            String status = subscription.getStatus();
            if (status.equals("active") || status.equals("succeeded")) {
                if (payService.isFreeTrial(couponId))
                    crmLogger.logLifecycleEvent(user.getEmail(), CRMService.CRMLifecyleEvent.FREE_TRIAL);
                else
                    crmLogger.logLifecycleEvent(user.getEmail(), CRMService.CRMLifecyleEvent.PAID);
                return Response.ok().build();
            }
            throw new BadRequestException("payment failed, status is  " + status);
        } catch(StripeException e) {
            throw new BadRequestException(e.getMessage());
        }
    }
}
