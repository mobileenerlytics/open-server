package com.mobileenerlytics.eagle.testertrial.entity;

import com.mobileenerlytics.eagle.testerpro.util.Constants;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;

public class UserTest extends TestBase {


    @Test
    @Ignore
    public void canPersistUser() throws IOException, SQLException {
        User created = createTestUser();
        userService.createUser("122@gmal.com", "yby4215098820" + new Date().toString(),"first","last", "12ß34" + new Date().toString(), "org", Constants.USER_ROLE_DEFAULT);
        assertThat(created != null);
    }


}
