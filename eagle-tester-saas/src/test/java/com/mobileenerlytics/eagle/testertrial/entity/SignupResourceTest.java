package com.mobileenerlytics.eagle.testertrial.entity;

import com.mobileenerlytics.eagle.testerpro.service.CRMService;
import com.mobileenerlytics.eagle.testertrial.resource.SignupResource;
import com.mobileenerlytics.eagle.testertrial.service.UserService;
import java.util.Base64;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.HttpHeaders;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class SignupResourceTest {
    @SpyBean
    private SignupResource signupResource;

    @MockBean
    private CRMService crmService;

    @MockBean
    UserService userService;

    @MockBean
    HttpHeaders headers;

    User user = new User();

    @Before
    public void setup() {
        when(headers.getHeaderString(HttpHeaders.AUTHORIZATION))
                .thenReturn("Basic " + Base64.getEncoder().withoutPadding().encodeToString("user:password:test@email.me".getBytes()));
    }

    @Test(expected = NullPointerException.class)
    public void failOnNullHeader() {
        signupResource.post(null, "Test", "First", "Last", null);
    }

    @Test(expected = Exception.class)
    public void failOnEmptyCreds() {
        when(headers.getHeaderString(HttpHeaders.AUTHORIZATION))
                .thenReturn("Basic ");
        signupResource.post(headers, "Test", "First", "Last", null);
    }


    @Test(expected = BadRequestException.class)
    public void failOnUsernameExists() {
        when(userService.queryUserByUsername("user")).thenReturn(user);
        signupResource.post(headers, "Org", "First", "Last", "default");
    }

    @Test(expected = BadRequestException.class)
    public void failOnEmailExists() {
        when(userService.queryUserByEmail("test@email.me")).thenReturn(user);
        signupResource.post(headers, "Org", "First", "Last", "default");
    }

    @Test(expected = BadRequestException.class)
    public void failOnInvalidEmail() {
        when(headers.getHeaderString(HttpHeaders.AUTHORIZATION))
                .thenReturn("Basic " + Base64.getEncoder().withoutPadding().encodeToString("user:password:blah".getBytes()));
        signupResource.post(headers, "Org", "First", "Last", "default");
    }

    @Test
    public void succeeds() {
        when(userService.createUser(anyString(), anyString(), anyString(), anyString(), anyString(), anyString(), anyString()))
                .thenReturn(true);
        signupResource.post(headers, "Org", "First", "Last", null);
        verify(userService, times(1)).createUser(matches("test@email.me"),
                matches("user"), matches("First"),
                matches("Last"), matches("password"), matches("Org"), matches("default"));
    }
}
