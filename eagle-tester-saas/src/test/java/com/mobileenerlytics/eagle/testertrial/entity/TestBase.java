package com.mobileenerlytics.eagle.testertrial.entity;

import com.mobileenerlytics.eagle.testertrial.service.UserService;

public class TestBase {
    public UserService userService;

//    @Before
//    public void setup() throws Exception {
//        if (server == null) {
//            app.run(new String[]{});
//            server = app.getServerInstance();
//        }
//    }

//    @After
//    public void clean(){
//        if (server != null)
//        server.shutdownNow();
//    }

    public User createTestUser() {
        String name = "testtest222";
        User user = userService.queryUserByUsername(name);
        if (user != null) return user;
        /* todo: clean this up. Where is testtest coming from? */
        userService.createUser((int)(Math.random() * 1000) + "1@gmail.com" , name, name, name,"test", "org", "default");
        return userService.queryUserByUsername("testtest");
    }
}