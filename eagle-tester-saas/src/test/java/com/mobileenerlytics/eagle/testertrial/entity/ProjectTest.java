package com.mobileenerlytics.eagle.testertrial.entity;

import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.service.ProjectService;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class ProjectTest extends TestBase {

    @Autowired
    private ProjectService projectService;


    @Test
    @Ignore
    public void canPersistProject() {
        projectService.queryOneOrCreate("default", "username");
    }


    @Test
    @Ignore
    public void canQueryDefaultProjectAfterCreateUser() {
        User testUser = createTestUser();
        List<Project> projects = projectService.queryProjectByUsername(testUser.getName());
        assertThat(projects.get(0).getName().equals("default"));
    }

    @Test
    @Ignore
    public void canQueryDefaultProjectAfterRunningBaseApplication() {
        List<Project> projects = projectService.queryProjectByUsername("demo");
        assertThat(projects.get(0).getName().equals("default"));
    }

}
