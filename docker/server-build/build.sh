## You must already be logged in.
## Otherwise run "docker login" first
docker build -t server-build .
docker tag server-build mobileenerlytics/server-build:latest
docker push mobileenerlytics/server-build:latest
