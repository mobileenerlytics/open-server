curl -X POST -is -u $BB_USER:$BB_PW \
  -H 'Content-Type: application/json' \
 https://api.bitbucket.org/2.0/repositories/mobileenerlytics/seleniumscripts/pipelines/ \
  -d '
  {
    "target": {
      "ref_type": "branch", 
      "type": "pipeline_ref_target", 
      "ref_name": "master"
    }
  }'
