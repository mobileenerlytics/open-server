# [master, release]
BRANCH=$1
PORT=$2
cd eagle-tester-saas/build/distributions/

rm eagle-tester-saas-boot*.tar
#rename
mv eagle-tester-saas*.tar eagle-tester-saas.tar

# copy tar to server
scp -P 9413 eagle-tester-saas.tar server@205.233.11.32:~/$BRANCH/

# copy properties, restart.sh file
cd ../../../
# rsync -arvz -e 'ssh -p 9413' ./config/application.$BRANCH.properties server@205.233.11.32:~/$BRANCH/application.properties
rsync -arvz -e 'ssh -p 9413' ./scripts/tar-restart.sh server@205.233.11.32:~/$BRANCH/

#docker file todo
#rsync -arvz -e 'ssh -p 9413' ./scripts/Dockerfile server@205.233.11.32:~/$BRANCH/

# cd current branch, excute the restart with the path and port
ssh -p 9413 -t server@205.233.11.32 "cd ~/$BRANCH/ && ./tar-restart.sh $PORT"
