#!/bin/bash
PORT=$1

rm -rf eagle-tester-saas-*
tar -xvf eagle-tester-saas.tar
cd eagle-tester-saas-*/bin/

##
fuser -k $PORT/tcp
JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64/
nohup ./eagle-tester-saas --spring.config.location=../../application.properties >> nohup.out 2>&1 &
sleep 1
