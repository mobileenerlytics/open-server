package com.mobileenerlytics.eagle.testerpro.entity;

import com.mobileenerlytics.eagle.testerpro.TestBase;
import com.mobileenerlytics.eagle.testerpro.service.*;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;


/**
 * Shows how to persist and retrieve entities to/from NoSQL stores using Hibernate OGM.
 *
 * @author Gunnar Morling
 */
public class CommitTest extends TestBase {
	private Random random = new Random();

	private List<String> componentNames = Arrays.asList(new String[]{"CPU", "GPU", "Screen"});

	@Autowired
	private ProjectService projectService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private ContributorService contributorService;

	@Autowired
	private CommitService commitService;

	@Autowired
	private TestRecordService testRecordService;


	@Test
	@Ignore
	public void testCount() {

	}

	@Test
	@Ignore
	public void canPersistTests() {
		/*// project
		Project project;
		List<Project> projects = projectService.queryProjectByUsername("demo");
		if (projects.isEmpty()) project = projectService.queryOneOrCreate("default", "demo");
		else project = projects.get(0);

		// Create branch
		for (int k = 0; k < 8; k++) {

			Branch branch;
			if (k == 0 ) {
				branch = branchService.queryOneByNameAndProject("origin/master" , project);
			} else {
				branch = branchService.queryOneByNameAndProject("origin/release" + k, project);
			}

			// commit1
			Contributor contributor = contributorService.queryOneOrCreate(k + "1@email.com", "yby");

			Commit commit = commitService.findOneOrCreate("ahashdd" + k, branch, "commit1 for fun", contributor);

			// create 100 data
			Set<ThreadCompEnergy> threadCompEnergies = new HashSet<>();
			for (int i = 0; i < 100; i++) {
				String threadId = "thread" + random.nextInt(100);
				String cname = componentNames.get(random.nextInt(100) % componentNames.size());
				int cost = i;
				threadCompEnergies.add(new ThreadCompEnergy("process", threadId, cname, cost));
			}
			TestRecord testRecord = new TestRecord("test", commit, commit.getBranch(), commit.getProject());
			testRecord.setThreadCompEnergies(threadCompEnergies);
			testRecordService.saveTestRecord(testRecord);
			commitService.saveCommit(commit);

			TestRecord testRecord1 = new TestRecord("test1", commit, commit.getBranch(), commit.getProject());
			testRecord.setThreadCompEnergies(threadCompEnergies);
			testRecordService.saveTestRecord(testRecord1);
			commitService.saveCommit(commit);

			// commit2
			Commit commit1 = commitService.findOneOrCreate("ahashd1d" + k, branch, "commit2 for fun", contributor);

			// create 100 data
			Set<ThreadCompEnergy> threadCompEnergies1 = new HashSet<>();
			for (int i = 0; i < 100; i++) {
				String threadId = "thread" + random.nextInt(100);
				String cname = componentNames.get(random.nextInt(100) % componentNames.size());
				int cost = i;
				threadCompEnergies.add(new ThreadCompEnergy("process", threadId, cname, cost));
			}

			testRecord = new TestRecord("test", commit, commit.getBranch(), commit.getProject());
			testRecord.setThreadCompEnergies(threadCompEnergies);
			testRecordService.saveTestRecord(testRecord);
			commitService.saveCommit(commit1);

			testRecord1 = new TestRecord("test1", commit, commit.getBranch(), commit.getProject());
			testRecord.setThreadCompEnergies(threadCompEnergies);
			testRecordService.saveTestRecord(testRecord1);
			commitService.saveCommit(commit1);
		}*/
	}


	@Test
	@Ignore
    public void canQueryBranchesByBranchName() {
    }
}
