package com.mobileenerlytics.eagle.testerpro;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import com.mobileenerlytics.eagle.testerpro.entity.Contributor;
import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.service.BranchService;
import com.mobileenerlytics.eagle.testerpro.service.CommitService;
import com.mobileenerlytics.eagle.testerpro.service.ContributorService;
import com.mobileenerlytics.eagle.testerpro.service.ProjectService;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InsertionTest extends TestBase {
    private final String RANDOM = RandomStringUtils.randomAlphabetic(32);
    private final String PROJECT_NAME = "InsertionProject-" + RANDOM;
    private final String USER_NAME = "InsertionUser-" + RANDOM ;
    private final String BRANCH_NAME = "InsertionBranch-" + RANDOM;
    private final String COMMIT_HASH = RANDOM;
    private final String AUTHOR_NAME = "InsertAuthor-" + RANDOM;
    private final String AUTHOR_EMAIL = "Insert@b.c-" + RANDOM;
    private final String COMP_NAME = "CPU";

    private final String TEST_NAME_1 = "InsertionTest1-" + RANDOM;
    private final String THREAD_NAME_1 = "InsertionThread1-" + RANDOM;
    private final double ENERGY_1_1 = 23.47;
    private final double ENERGY_1_2 = 3.47;
    private final double ENERGY_AVG_1 = (ENERGY_1_1 + ENERGY_1_2)/2;

    private final String TEST_NAME_2 = "InsertionTest2-" + RANDOM;
    private final String THREAD_NAME_2 = "InsertionThread2-" + RANDOM;
    private final double ENERGY_2 = 91.48;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private BranchService branchService;

    @Autowired
    private ContributorService contributorService;

    @Autowired
    private CommitService commitService;

    public InsertionTest() throws IOException {
    }

    @Test
    @Ignore
    public void test10__insert() throws Exception {
        /*Project project = projectService.queryOneOrCreate(PROJECT_NAME, USER_NAME);
        {
            // Insertion
            // We do inserting in different entity managers since this will also test cascading etc
            final Branch branch = branchService.queryOneByNameAndProject(BRANCH_NAME, project);
            // Verify there is no old leftover data
            Assert.assertEquals(branch.getCommits().size(), 0);

            final Contributor contributor = contributorService.queryOneOrCreate(AUTHOR_EMAIL, AUTHOR_NAME);
            Assert.assertEquals(contributor != null, true);

            final Commit commit = commitService.findOneOrCreate(COMMIT_HASH, branch, "", contributor);
            // Verify there is no old leftover data
            Assert.assertEquals(commit.getTestStats().size(), 0);
        }

        {
            // Verify results
            final Branch branch = branchService.queryOneByNameAndProject(BRANCH_NAME, project);
            Assert.assertEquals(branch.getBranchName(), BRANCH_NAME);
            Assert.assertEquals(branch.getCommits().size(), 1);
//            Assert.assertEquals(branch.getTotalTests(), 2);
//            Assert.assertEquals(branch.getTestCounter().size(), 2);

            final Contributor contributor = contributorService.queryOneOrCreate(AUTHOR_EMAIL, AUTHOR_NAME);

            final Commit commit = commitService.findOneOrCreate(COMMIT_HASH, branch, "", contributor);
            Assert.assertEquals(commit.getHash(), COMMIT_HASH);
//            final TestRecord testRecord = TestRecord.queryTestRecord(em, TEST_NAME_1, commit);
//
//            Assert.assertNotNull(testRecord);
//            Assert.assertEquals(testRecord.getTestName(), TEST_NAME_1);
//            double e = testRecord.getThreadUnit().get(THREAD_NAME_1);
//            Assert.assertEquals(e, ENERGY_1_1, 0.01);
//            e = testRecord.getComponentUnit().get(COMP_NAME);
//            Assert.assertEquals(e, ENERGY_1_1, 0.01);
        }*/

    }


}
