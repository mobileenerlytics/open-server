package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
public class BranchServiceTest {

    @MockBean
    private BranchRepository branchRepository;

    @MockBean
    private CommitService commitService;

    @Autowired
    ApplicationContext applicationContext;

    @SpyBean
    private BranchService branchService;

    private Project project;
    private Date updatedMs;
    private String branchName;

    @Before
    public void setUp() {
        project = new Project("default", "fb_messenger_test");
        updatedMs = new Date();
        branchName = "origin/release";
    }

    @Test
    public void testFindOneByNameAndProject() {
        Mockito.when(branchRepository.findByBranchNameAndProject(branchName, project)).thenReturn(Optional.of(new Branch(branchName, updatedMs, project)));

        Branch branch = branchService.queryOneByNameAndProject(branchName, project).get();

        Assert.assertEquals(project, branch.getProject());
        Assert.assertEquals(branchName, branch.getBranchName());
        Assert.assertEquals(updatedMs, branch.getUpdatedMs());
    }

    @Test
    public void testGetOneOrCreate_Exists() {
        Mockito.when(branchRepository.findByBranchNameAndProject(branchName, project)).thenReturn(Optional.of(new Branch(branchName, updatedMs, project)));

        Branch branch = branchService.queryOneOrCreate(branchName, updatedMs, project);

        Assert.assertEquals(project, branch.getProject());
        Assert.assertEquals(branchName, branch.getBranchName());
        Assert.assertEquals(updatedMs, branch.getUpdatedMs());
    }

    @Test
    public void testGetOneOrCreate_New() {
        Mockito.when(branchRepository.findByBranchNameAndProject(branchName, project)).thenReturn(Optional.empty());
        Mockito.when(branchRepository.save(ArgumentMatchers.any())).then(AdditionalAnswers.returnsFirstArg());

        Branch branch = branchService.queryOneOrCreate(branchName, updatedMs, project);

        Assert.assertEquals(project, branch.getProject());
        Assert.assertEquals(branchName, branch.getBranchName());
        Assert.assertEquals(updatedMs, branch.getUpdatedMs());
    }

    @Test
    public void testFindByBranchNameAndProject() {
        List<Branch> branchList = new ArrayList<>();
        Branch b1 = new Branch(branchName, updatedMs, project);
        Branch b2 = new Branch(branchName, new Date(), project);
        branchList.add(b1);
        branchList.add(b2);

        Mockito.when(branchRepository.queryAllByBranchNameAndProject(branchName, project)).thenReturn(branchList);
        List<Branch> branches = branchService.queryByBranchNameAndProject(branchName, project, 0);
        Assert.assertEquals(2, branches.size());
    }

    @Test
    public void testGetAllBranches() {
        List<Branch> branchList = new ArrayList<>();
        Branch b1 = new Branch(branchName, updatedMs, project);
        Branch b2 = new Branch(branchName + "123", new Date(), project);
        branchList.add(b1);
        branchList.add(b2);

        Mockito.when(branchRepository.queryAllByProject(project)).thenReturn(branchList);

        List<Branch> branches = branchService.queryByProject(project, 0);

        Assert.assertEquals(2, branches.size());
    }

    @Test
    public void testGetAllBranchesByPrefix() {
        List<Branch> branchList = new ArrayList<>();
        Branch b1 = new Branch(branchName, updatedMs, project);
        Branch b2 = new Branch(branchName + "123", new Date(), project);
        branchList.add(b1);
        branchList.add(b2);

        Mockito.when(branchRepository.queryAllByProjectAndBranchNameLike(project, branchName)).thenReturn(branchList);

        List<Branch> branches = branchService.queryByPrefix(project, branchName, 0);

        Assert.assertEquals(2, branches.size());
        Assert.assertEquals(branchName, branches.get(0).getBranchName());
        Assert.assertEquals(branchName + "123", branches.get(1).getBranchName());
    }

    @Test
    public void testAddTest() throws DeletedException {
        Branch branch = new Branch(branchName, updatedMs, project);
        branch.getTestCounter().put("manual-test", 1);
        Mockito.when(branchRepository.save(ArgumentMatchers.any())).then(AdditionalAnswers.returnsFirstArg());
        Mockito.when(branchRepository.findById(branch.getId())).thenReturn(Optional.of(branch));
        branch = branchService.addTest(branch, "manual-test");
        branch = branchService.addTest(branch, "test2");
        Assert.assertThat(branch.getTestCounter().keySet(), Matchers.containsInAnyOrder("test2", "manual-test"));
    }
}
