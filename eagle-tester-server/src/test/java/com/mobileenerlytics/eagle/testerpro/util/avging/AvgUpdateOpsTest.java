package com.mobileenerlytics.eagle.testerpro.util.avging;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertThat;


public class AvgUpdateOpsTest {

    @Test
    public void testAddAvgingBasic() {
        AvgUpdateOps add = new AvgUpdateOps.AvgUpdateAdd(0);
        Map<String, SimpleAvgEntry> oldMap = new HashMap<>();
        List<SimpleAvgEntry> update = new LinkedList<SimpleAvgEntry>() {{
            add(new SimpleAvgEntry("a", 10));
        }};
        add.updateOldMap(oldMap, update);
        assertThat(oldMap.values(), Matchers.contains(new SimpleAvgEntry("a", 10)));
    }

    @Test
    public void testAddAvgingMultiple() {
        AvgUpdateOps add = new AvgUpdateOps.AvgUpdateAdd(4);
        Map<String, SimpleAvgEntry> oldMap = new HashMap<String, SimpleAvgEntry>() {{
            put("a", new SimpleAvgEntry("a", 15));
            put("b", new SimpleAvgEntry("b", 10));
        }};
        List<SimpleAvgEntry> update = new LinkedList<SimpleAvgEntry>() {{
            add(new SimpleAvgEntry("a", 10));
            add(new SimpleAvgEntry("c", 20));
        }};
        add.updateOldMap(oldMap, update);
        assertThat(oldMap.values(), Matchers.contains(
                new SimpleAvgEntry("a", 14),    // (15 * 4 + 10)/(4 + 1)
                new SimpleAvgEntry("b", 8),     // 10 * 4 / (4 + 1)
                new SimpleAvgEntry("c", 4)));   // 20 / (4 + 1)
    }


    @Test
    public void testRmAvgingMultiple() {
        AvgUpdateOps sub = new AvgUpdateOps.AvgUpdateSub(11);
        Map<String, SimpleAvgEntry> oldMap = new HashMap<String, SimpleAvgEntry>() {{
            put("a", new SimpleAvgEntry("a", 15));
            put("b", new SimpleAvgEntry("b", 10));
            put("c", new SimpleAvgEntry("c", 10));
        }};
        List<SimpleAvgEntry> update = new LinkedList<SimpleAvgEntry>() {{
            add(new SimpleAvgEntry("a", 9));
            add(new SimpleAvgEntry("c", 20));
        }};
        sub.updateOldMap(oldMap, update);
        assertThat(oldMap.values(), Matchers.contains(
                new SimpleAvgEntry("a", 15.6),  // (15*11 - 9) / (11 - 1)
                new SimpleAvgEntry("b", 11),    // 10 * 11 / (11 - 1)
                new SimpleAvgEntry("c", 9)));   // (10 * 11 - 20) / (11 - 1)
    }
}
