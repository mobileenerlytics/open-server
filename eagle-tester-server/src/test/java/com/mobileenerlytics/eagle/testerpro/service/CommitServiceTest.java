package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import com.mobileenerlytics.eagle.testerpro.entity.Contributor;
import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class CommitServiceTest {

    @MockBean
    private CommitRepository commitRepository;

    @MockBean
    private BranchService branchService;

    @MockBean @Lazy
    private TestRecordService testRecordService;

    @MockBean @Lazy
    private TestStatsService testStatsService;

    @Autowired
    ApplicationContext applicationContext;

    @SpyBean @Lazy
    private CommitService commitService;

    @MockBean
    private TestStatsRepository testStatsRepository;

    @MockBean
    private ContributorService contributorService;

    private Project project;
    private Contributor contributor;
    private Branch branch;
    private Date updatedMs;
    private String hash;
    private String branchName;
    private String commitId;

    @Before
    public void setUp() {
        commitId = "5c86eff612dbd927eba31d4f";
        updatedMs = new Date();
        project = new Project("default", "fb_messenger_test");
        contributor = new Contributor("reprocess@mobileenerlytics.com", "Reprocess");
        branchName = "origin/release";
        branch = new Branch(branchName, updatedMs, project);
        hash = "T1";
    }

    @Test
    public void testGetByCommitId() {
        Mockito.doReturn(Optional.of(new Commit(hash, branch, contributor, updatedMs))).when(commitRepository).findById(commitId);

        Commit commit = commitService.queryById(commitId);

        Assert.assertEquals(hash, commit.getHash());
        Assert.assertEquals(branchName, commit.getBranchName());
        Assert.assertEquals(branch, commit.getBranch());
        Assert.assertEquals(project, commit.getProject());
        Assert.assertEquals(contributor, commit.getContributor());
        Assert.assertEquals(updatedMs, commit.getUpdatedMs());
    }

    @Test
    public void testGetByHashAndBranch() {
        Mockito.when(commitRepository.findByHashAndBranch(hash, branch)).thenReturn(Optional.of(new Commit(hash, branch, contributor, updatedMs)));

        Commit commit = commitService.findByHashAndBranch(hash, branch).get();

        Assert.assertEquals(hash, commit.getHash());
        Assert.assertEquals(branchName, commit.getBranchName());
        Assert.assertEquals(branch, commit.getBranch());
        Assert.assertEquals(project, commit.getProject());
        Assert.assertEquals(contributor, commit.getContributor());
        Assert.assertEquals(updatedMs, commit.getUpdatedMs());
    }

    @Test
    public void testGetOneOrCreate_Exists() {
        Mockito.when(commitRepository.save(ArgumentMatchers.any())).then(AdditionalAnswers.returnsFirstArg());
        Mockito.when(commitRepository.findByHashAndBranch(hash, branch)).thenReturn(Optional.of(new Commit(hash, branch, contributor, updatedMs)));

        Commit commit = commitService.findOneOrCreate(hash, branch, contributor, updatedMs);

        Assert.assertEquals(hash, commit.getHash());
        Assert.assertEquals(branchName, commit.getBranchName());
        Assert.assertEquals(branch, commit.getBranch());
        Assert.assertEquals(project, commit.getProject());
        Assert.assertEquals(contributor, commit.getContributor());
        Assert.assertEquals(updatedMs, commit.getUpdatedMs());
    }

    @Test
    public void testGetOneOrCreate_New() {
        Mockito.when(commitRepository.findByHashAndBranch(hash, branch)).thenReturn(Optional.empty());
        Mockito.when(commitRepository.save(ArgumentMatchers.any())).then(AdditionalAnswers.returnsFirstArg());

        Commit commit = commitService.findOneOrCreate(hash, branch, contributor, updatedMs);

        Assert.assertEquals(hash, commit.getHash());
        Assert.assertEquals(branchName, commit.getBranchName());
        Assert.assertEquals(branch, commit.getBranch());
        Assert.assertEquals(project, commit.getProject());
        Assert.assertEquals(contributor, commit.getContributor());
    }

    @Test
    public void testGetOneAndDelete() throws DeletedException {
        String randomCommitId = "5c86eff612dbd927eba31d4f";
        Mockito.doReturn(Optional.of(new Commit(hash, branch, contributor, updatedMs))).when(commitRepository).findById(commitId);

        boolean success = commitService.queryOneAndDelete(commitId);

        Assert.assertTrue(success);

        Mockito.doReturn(Optional.ofNullable(null)).when(commitRepository).findById(randomCommitId);

        boolean notFound = commitService.queryOneAndDelete(randomCommitId);

        Assert.assertFalse(notFound);
    }

    @Test
    public void testGetCommits() {
        List<Commit> commitList = new ArrayList<>();
        Commit c1 = new Commit(hash, branch, contributor, updatedMs);
        Commit c2 = new Commit(hash + "123", branch, contributor, updatedMs);
        commitList.add(c1);
        commitList.add(c2);
        Pageable pageable = PageRequest.of(0, 2, Sort.Direction.DESC, "updatedMs");

        Mockito.when(commitRepository.queryAllByProjectAndBranchName(project, branchName, pageable)).thenReturn(commitList);

        List<Commit> commits1 = commitService.queryByProjectAndBranchName(project, branchName, 2);

        Assert.assertEquals(2, commits1.size());

        Mockito.when(commitRepository.queryAllByProject(project, pageable)).thenReturn(commitList);

        List<Commit> commits2 = commitService.queryByProjectAndBranchName(project, null, 2);

        Assert.assertEquals(2, commits2.size());
    }

    @Test
    public void testGetCommitsWithMiddleHash() {
        List<Commit> results = new ArrayList<>();

        List<Commit> beforeCommits = new ArrayList<>();
        Commit firstBefore = new Commit("T1", branch, contributor, updatedMs);
        Commit secondBefore = new Commit("T2", branch, contributor, updatedMs);
        beforeCommits.add(firstBefore);
        beforeCommits.add(secondBefore);

        List<Commit> afterCommits = new ArrayList<>();
        Commit firstAfter = new Commit("T4", branch, contributor, updatedMs);
        Commit secondAfter = new Commit("T5", branch, contributor, updatedMs);
        afterCommits.add(firstAfter);
        afterCommits.add(secondAfter);

        Mockito.when(commitRepository.queryAllByProjectAndBranchNameUpdatedMsBefore(updatedMs, project, branchName, PageRequest.of(0, 2, Sort.Direction.ASC, "updatedMs"))).thenReturn(beforeCommits);
        Mockito.when(commitRepository.queryAllByProjectAndBranchNameUpdatedMsAfter(updatedMs, project, branchName, PageRequest.of(0, 2, Sort.Direction.DESC, "updatedMs"))).thenReturn(afterCommits);

        List<Commit> getCommitsAfter = commitService.queryCommitsWithMiddleHash(project, branchName, 2, Sort.by(Sort.Direction.ASC, "updatedMs"), updatedMs, false);
        results = getCommitsAfter;

        List<Commit> getCommitsBefore = commitService.queryCommitsWithMiddleHash(project, branchName, 2, null, updatedMs, true);
        results.addAll(getCommitsBefore);

    }
}
