package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Contributor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ContributorServiceTest {

    @MockBean
    private ContributorRepository contributorRepository;

    @Autowired
    ApplicationContext applicationContext;

    @SpyBean
    private ContributorService contributorService;

    private String email;
    private String name;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        email = "reprocess@mobileenerlytics.com";
        name = "Reprocess";
    }

    @Test
    public void testGetOneOrCreate_Exists() {
        Mockito.when(contributorRepository.findByEmail(email)).thenReturn(new Contributor(email, name));
        Mockito.when(contributorRepository.save(ArgumentMatchers.any())).then(AdditionalAnswers.returnsFirstArg());

        Contributor noName = contributorService.queryOneOrCreate(email, null);

        Assert.assertEquals(email, noName.getEmail());
        Assert.assertEquals(name, noName.getName());

        Contributor withName = contributorService.queryOneOrCreate(email, name);
        Assert.assertEquals(email, withName.getEmail());
        Assert.assertEquals(name, withName.getName());

        Contributor withUniqueName = contributorService.queryOneOrCreate(email, name + "1234");
        Assert.assertEquals("Reprocess1234", withUniqueName.getName());
    }

    @Test
    public void testGetOneOrCreate_New() {
        Mockito.when(contributorRepository.findByEmail(email)).thenReturn(null);
        Mockito.when(contributorRepository.save(ArgumentMatchers.any())).then(AdditionalAnswers.returnsFirstArg());

        Contributor contributor = contributorService.queryOneOrCreate(email, name);

        Assert.assertEquals(email, contributor.getEmail());
        Assert.assertEquals(name, contributor.getName());
    }
}
