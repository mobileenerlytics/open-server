package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.*;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;

@RunWith(SpringRunner.class)
public class TestStatsServiceTest {

    @MockBean
    private TestStatsRepository testStatsRepository;

    @MockBean
    private CommitService commitService;

    @Autowired
    ApplicationContext applicationContext;

    @SpyBean
    private TestStatsService testStatsService;

    @MockBean
    private TestRecordService testRecordService;

    @MockBean
    private BranchService branchService;

    private Project project;
    private Branch branch;
    private Commit commit;
    private Contributor contributor;
    private TestRecord testRecord;
    private Date updatedMs;
    private long durationMs = 1000;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        doAnswer(returnsFirstArg()).when(testStatsRepository).save(any());
        updatedMs = new Date();
        contributor = new Contributor("reprocess@mobileenerlytics.com", "Reprocess");
        project = new Project("default", "fb_messenger_test");
        branch = new Branch("manual", updatedMs, project);
        commit = new Commit("T1", branch, contributor, updatedMs);
        testRecord = new TestRecord("manual-test", commit, branch, project, durationMs, new HashSet<>(), new HashSet<>());
    }

    @Test
    public void testFindByCommitAndTestName() {
        Mockito.when(testStatsRepository.findByCommitAndTestName(commit, "manual-test")).thenReturn(Optional.of(new TestStats(commit, "manual-test", updatedMs)));

        TestStats testStats = testStatsService.queryByCommitAndTestName(commit, "manual-test").get();

        Assert.assertEquals("manual-test", testStats.getTestName());
        Assert.assertEquals(commit, testStats.getCommit());
    }

    @Test
    public void testAddTestRecord() throws DeletedException {
        Mockito.when(testStatsRepository.findByCommitAndTestName(commit, "manual-test")).thenReturn(Optional.of(new TestStats(commit, "manual-test", updatedMs)));

        TestStats testStatsExists = testStatsService.addTestRecord(testRecord);

        Assert.assertEquals("manual-test", testStatsExists.getTestName());
        Assert.assertEquals(commit, testStatsExists.getCommit());
        Assert.assertEquals(1, testStatsExists.getTestRecordSet().size());

        Mockito.when(testStatsRepository.findByCommitAndTestName(commit, "test2")).thenReturn(Optional.empty());
        Mockito.when(testStatsRepository.save(any())).then(returnsFirstArg());

        TestRecord newTest = new TestRecord("test2", commit, branch, project, durationMs, new HashSet<>(), new HashSet<>());
        TestStats testStatsNotExists = testStatsService.addTestRecord(newTest);

        Assert.assertEquals("test2", testStatsNotExists.getTestName());
        Assert.assertEquals(commit, testStatsNotExists.getCommit());
        Assert.assertEquals(1, testStatsNotExists.getTestRecordSet().size());
    }

    @Test
    public void testDeleteTestRecord() throws DeletedException {
        TestStats original = new TestStats(commit, testRecord.getTestName(), testRecord.getUpdatedMs());
        original.getTestRecordSet().add("HELLO");
        original.getTestRecordSet().add(testRecord.getId());
        Mockito.when(testStatsRepository.findByCommitAndTestName(commit, "manual-test")).thenReturn(Optional.of(original));

        testStatsService.deleteTestRecord(testRecord);

        Assert.assertEquals(1, original.getTestRecordSet().size());
    }
}
