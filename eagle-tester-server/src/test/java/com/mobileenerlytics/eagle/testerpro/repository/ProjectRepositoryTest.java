package com.mobileenerlytics.eagle.testerpro.repository;


import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.service.BranchService;
import com.mobileenerlytics.eagle.testerpro.service.ProjectService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@DataMongoTest
public class ProjectRepositoryTest {
    private String prjId;

    @Autowired
    private ProjectService projectService;

    @MockBean
    private BranchService branchService;

    @Before
    public void setUp() {
//        MockitoAnnotations.initMocks(this);
        projectService.queryOneOrCreate("test1", "harsh");
        projectService.queryOneOrCreate("test2", "harsh");
        prjId = projectService.queryByProjectNameAndUsername("test2", "harsh").getId();
    }

    @Test
    @Ignore
    public void whenGetProject_returnCorrectProject() {
        Project foundProj = projectService.queryByProjectNameAndUsername("test1", "harsh");
        Assert.assertEquals("test1", foundProj.getName());
        Assert.assertEquals("harsh", foundProj.getUsername());

    }

    @Test
    @Ignore
    public void whenGetProject_createNewProject() {
        Project foundProj = projectService.queryByProjectNameAndUsername("test3", "harsh");
        Assert.assertEquals("test3", foundProj.getName());
        Assert.assertEquals("harsh", foundProj.getUsername());
    }

    @Test
    @Ignore
    public void testQueryByUserId() {
        List<Project> allProjs = projectService.queryProjectByUsername("harsh");
        Assert.assertEquals(2, allProjs.size());
    }

    @Test
    @Ignore
    public void testQueryByProjectId() {
        Project foundProj = projectService.queryProjectById(prjId);
        Assert.assertEquals("test2", foundProj.getName());
        Assert.assertEquals("harsh", foundProj.getUsername());
        Assert.assertEquals(prjId, foundProj.getId());
    }

}
