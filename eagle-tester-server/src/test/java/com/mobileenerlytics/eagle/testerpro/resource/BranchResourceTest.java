package com.mobileenerlytics.eagle.testerpro.resource;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.service.BranchService;
import com.mobileenerlytics.eagle.testerpro.service.ProjectService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
public class BranchResourceTest {

    @MockBean
    private BranchService branchService;

    @MockBean
    private ProjectService projectService;

    @Context
    HttpHeaders httpHeaders;

    @Autowired
    ApplicationContext applicationContext;

    @SpyBean
    private BranchResource branchResource;

    private Project project;

    @Before
    public void setUp() {
        project = new Project("default", "fb_messenger_test");
        project.setId("5c9169433f80434072874c98");
    }

    @Test
    public void queryAllBranches() {
        List<Branch> originalBranches = new ArrayList<>();
        Branch b1 = new Branch("manual-test-branch", new Date(), project);
        Branch b2 = new Branch("login-logout", new Date(), project);
        originalBranches.add(b1);
        originalBranches.add(b2);
        Mockito.when(branchService.queryByProject(project, 2)).thenReturn(originalBranches);
        Mockito.when(projectService.queryProjectById(project.getId())).thenReturn(project);

        List<Branch> branches = branchResource.getBranches(null, null, 2, project.getId());

        Assert.assertEquals(2, branches.size());
        Assert.assertEquals("manual-test-branch", branches.get(0).getBranchName());
        Assert.assertEquals("login-logout", branches.get(1).getBranchName());
    }

    @Test
    public void queryByBranchNameAndProject() {
        List<Branch> originalBranches = new ArrayList<>();
        Branch b1 = new Branch("manual-test-branch", new Date(), project);
        Branch b2 = new Branch("manual-test-branch", new Date(), project);
        originalBranches.add(b1);
        originalBranches.add(b2);
        Mockito.when(projectService.queryProjectById(project.getId())).thenReturn(project);
        Mockito.when(branchService.queryByBranchNameAndProject("manual-test-branch", project, 2)).thenReturn(originalBranches);

        List<Branch> branches = branchResource.getBranches(null, "manual-test-branch", 2, project.getId());

        Assert.assertEquals(2, branches.size());
        Assert.assertEquals("manual-test-branch", branches.get(0).getBranchName());
        Assert.assertEquals("manual-test-branch", branches.get(1).getBranchName());
    }

    @Test
    public void queryByPrefix() {
        List<Branch> originalBranches = new ArrayList<>();
        Branch b1 = new Branch("manual-test-branch", new Date(), project);
        Branch b2 = new Branch("manual-test-branch", new Date(), project);
        originalBranches.add(b1);
        originalBranches.add(b2);
        Mockito.when(projectService.queryProjectById(project.getId())).thenReturn(project);
        Mockito.when(branchService.queryByPrefix(project, "manual*", 2)).thenReturn(originalBranches);

        List<Branch> branches = branchResource.getBranches("manual", null, 2, project.getId());

        Assert.assertEquals(2, branches.size());
        Assert.assertEquals("manual-test-branch", branches.get(0).getBranchName());
    }

    @Test(expected = ForbiddenException.class)
    public void queryWithoutProjectId() {
        List<Branch> branches = branchResource.getBranches(null, "manual-test-branch", 2, null);
    }

    @Test(expected = BadRequestException.class)
    public void queryProjectNotFound() {
        Mockito.when(projectService.queryProjectById(project.getId())).thenReturn(null);

        List<Branch> branches = branchResource.getBranches(null, "manual-test-branch", 2, project.getId());
    }
}
