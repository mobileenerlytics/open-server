package com.mobileenerlytics.eagle.testerpro.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import static org.junit.Assert.assertNotNull;

public class ApiTest {

    private WebTarget target;

    @Before
    public void setUp() {
        // create the client
        Client c = ClientBuilder.newClient();
        target = c.target("http://localhost:7357");
    }

    @After
    public void tearDown() {
//        server.shutdown();
    }

    /**
     * Test to see that the message "Got it!" is sent in the response.
     */
    @Test
    @Ignore
    public void testGetIt() {
        String responseMsg = target.path("eagle/").request().get(String.class);
        assertNotNull(responseMsg);
    }
}
