package com.mobileenerlytics.eagle.testerpro;

import com.google.common.collect.Sets;
import com.mobileenerlytics.eagle.testerpro.entity.AggregateEvent;
import com.mobileenerlytics.eagle.testerpro.entity.ThreadCompEnergy;
import com.mobileenerlytics.eagle.testerpro.util.db.UploadRunnable;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertTrue;


public class JsonToDbTest extends TestBase {

    @Autowired
    private UploadRunnable uploadRunnable;

    @Test
    @Ignore
    public void testAppJson() throws IOException {
        String fileName = this.getClass().getClassLoader().getResource("app.json").getFile();
        File jsonFile = new File(fileName);
        Set<ThreadCompEnergy> threadCompEnergySet = uploadRunnable.parseThreadCompEnergy(jsonFile, new HashMap<Integer, String>(){{
            put(-2, "phone");
        }}, 1000);
        assertTrue(!threadCompEnergySet.isEmpty());
    }

    @Test
    @Ignore
    public void testEvents () {
        String fileName = this.getClass().getClassLoader().getResource("events.json").getFile();
        File jsonFile = new File(fileName);
        Set<AggregateEvent> test = uploadRunnable.parseEvent(jsonFile, new HashMap<Integer, String>() {{
            put(19969, "app");
        }});
        assertTrue(!test.isEmpty());
    }
}