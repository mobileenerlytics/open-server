package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.*;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
public class TestRecordServiceTest {

    @MockBean
    private TestRecordRepository testRecordRepository;

    @MockBean
    private BranchService branchService;

    @MockBean
    private TestStatsService testStatsService;

    @Autowired
    ApplicationContext applicationContext;

    @SpyBean
    private TestRecordService testRecordService;

    @MockBean
    private CommitService commitService;

    @MockBean
    private ContributorService contributorService;

    @MockBean
    private UploadService uploadService;

    private Project project;
    private Branch branch;
    private Commit commit;
    private Contributor contributor;
    private long durationMs = 1000;
    private String id;

    @Before
    public void dependencySetup() {
        MockitoAnnotations.initMocks(this);
        id = "1311";
        Date updatedMs = new Date();
        contributor = new Contributor("reprocess@mobileenerlytics.com", "Reprocess");
        project = new Project("default", "fb_messenger_test");
        branch = new Branch("manual", updatedMs, project);
        commit = new Commit("T1", branch, contributor, updatedMs);
    }

    @Test
    public void testGetById() {
        Mockito.when(testRecordRepository.findById(id)).thenReturn(Optional.of(
                new TestRecord("manual-test", commit, branch, project, durationMs,
                        new HashSet<>(), new HashSet<>())));

        TestRecord testRecord = testRecordService.queryById(id).get();

        Assert.assertEquals("manual-test", testRecord.getTestName());
        Assert.assertEquals(project, testRecord.getProject());
        Assert.assertEquals(branch, testRecord.getBranch());
        Assert.assertEquals(commit, testRecord.getCommit());
    }

    @Test
    public void testGetOneAndDelete() throws DeletedException {
        Mockito.when(testRecordRepository.findById(id)).thenReturn(Optional.of(
                new TestRecord("manual-test", commit, branch, project, durationMs,
                        new HashSet<>(), new HashSet<>())));
        Mockito.when(testRecordRepository.findById("1")).thenReturn(Optional.empty());

        boolean success = testRecordService.queryOneAndDelete(id);
        boolean notFound = testRecordService.queryOneAndDelete("1");

        Assert.assertTrue(success);
        Assert.assertFalse(notFound);
    }

    @Test
    public void testGetTestRecords() {
        TestRecord t1 = new TestRecord("manual-test", commit, branch, project, durationMs,
                new HashSet<>(), new HashSet<>());
        TestRecord t2 = new TestRecord("manual-test", commit, branch, project, durationMs,
                new HashSet<>(), new HashSet<>());
        List<TestRecord> testRecordList = new ArrayList<>();
        testRecordList.add(t1);
        testRecordList.add(t2);
        int count = 2;
        Pageable pageable = PageRequest.of(0, count, Sort.Direction.DESC, "updatedMs");

        Mockito.when(testRecordRepository.queryAllByTestNameAndCommit("manual-test", commit, pageable)).thenReturn(testRecordList);

        List<TestRecord> testRecords = testRecordService.queryByCommitAndTestName(commit, "manual-test", 2);

        Assert.assertEquals(2, testRecords.size());
    }
}
