package com.mobileenerlytics.eagle.testerpro.resource;

import com.mobileenerlytics.eagle.testerpro.entity.*;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import com.mobileenerlytics.eagle.testerpro.service.CommitService;
import com.mobileenerlytics.eagle.testerpro.service.TestRecordService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.*;

@RunWith(SpringRunner.class)
public class TestRecordResourceTest {

    @MockBean
    private TestRecordService testRecordService;

    @MockBean
    private CommitService commitService;

    @Autowired
    ApplicationContext applicationContext;

    @SpyBean
    private TestRecordResource testRecordResource;

    private Project project;
    private Branch branch;
    private Contributor contributor;
    private Commit commit;
    private TestRecord testRecord;

    private final String PROJECT_ID = "5c9169433f80434072874c98";
    private final String COMMIT_ID = "5c9169433f80434072874c77";
    private final String TESTRECORD_ID = "14";
    private final long DURATION_MS = 1000;

    @Before
    public void setUp() {
        project = new Project("default", "fb_messenger_test");
        project.setId(PROJECT_ID);
        contributor = new Contributor("reprocess@mobileenerlytics.com", "Reprocess");
        branch = new Branch("manual-test-branch", new Date(), project);
        commit = new Commit("T1", branch, contributor, new Date());
        commit.setId(COMMIT_ID);
        testRecord = new TestRecord("testName", commit, branch, project, DURATION_MS, new HashSet<>(), new HashSet<>());
        testRecord.setId(TESTRECORD_ID);
        Mockito.when(testRecordService.queryById(TESTRECORD_ID)).thenReturn(Optional.of(testRecord));
    }

    @Test
    public void queryTestRecords() {
        List<TestRecord> originalRecords = new ArrayList<>();
        TestRecord t1 = new TestRecord("Manual", commit, branch, project, DURATION_MS, new HashSet<>(), new HashSet<>());
        TestRecord t2 = new TestRecord("Manual", commit, branch, project, DURATION_MS, new HashSet<>(), new HashSet<>());
        originalRecords.add(t1);
        originalRecords.add(t2);
        Mockito.when(commitService.queryById(commit.getId())).thenReturn(commit);
        Mockito.when(testRecordService.queryByCommitAndTestName(commit, "Manual", 2)).thenReturn(originalRecords);

        List<TestRecord> testRecords = testRecordResource.getTestRecords(commit.getId(), "Manual", 2);

        Assert.assertEquals(2, testRecords.size());
        Assert.assertEquals("Manual", testRecords.get(0).getTestName());
    }

    @Test
    public void deleteTestRecord() throws DeletedException {
        Mockito.when(testRecordService.queryOneAndDelete("14")).thenReturn(true);

        Response response = testRecordResource.deleteTestRecords("14", project.getId());

        Assert.assertEquals(200, response.getStatus());

        Assert.assertEquals("Test Record successfully deleted", response.getEntity());
    }

    @Test(expected = ForbiddenException.class)
    public void getTestRecordsWithoutCommitId() {
        testRecordResource.getTestRecords(null, "Manual", 2);
    }

    @Test(expected = ForbiddenException.class)
    public void getTestRecordsWithoutTestname() {
        testRecordResource.getTestRecords(commit.getId(), null, 2);
    }

    @Test(expected = NotFoundException.class)
    public void getTestRecordsCommitNotFound() {
        Mockito.when(commitService.queryById(commit.getId())).thenReturn(null);
        testRecordResource.getTestRecords(commit.getId(), "Manual", 2);
    }

    @Test(expected = NotFoundException.class)
    public void deleteTestRecordNotFound() throws DeletedException {
        Mockito.when(testRecordService.queryOneAndDelete(TESTRECORD_ID)).thenReturn(false);
        testRecordResource.deleteTestRecords(TESTRECORD_ID, project.getId());
    }

    @Test(expected = ForbiddenException.class)
    public void deleteTestRecordNoProjectId() {
        testRecordResource.deleteTestRecords(TESTRECORD_ID, null);
    }

    @Test(expected = NotFoundException.class)
    public void deleteTestRecordInvalidId() {
        testRecordResource.deleteTestRecords("-1", project.getId());
    }
}
