package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Project;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class ProjectServiceTest {

    @MockBean
    private ProjectRepository projectRepository;

    @Autowired
    ApplicationContext applicationContext;

    @SpyBean
    private ProjectService projectService;

    @MockBean
    private BranchService branchService;

    @Test
    public void whenGetOrCreate_returnExisting() {
        Mockito.when(projectRepository.findByNameAndUserId("default", "fb_messenger_test")).thenReturn(new Project("default", "fb_messenger_test"));

        Project project = projectService.queryOneOrCreate("default", "fb_messenger_test");

        Assert.assertEquals("default", project.getName());
        Assert.assertEquals("fb_messenger_test", project.getUsername());
    }

    @Test
    public void whenGetOrCreate_returnNew() {
        Mockito.when(projectRepository.findByNameAndUserId("default", "fb_messenger_test")).thenReturn(null);
        Mockito.when(projectRepository.save(ArgumentMatchers.any())).then(AdditionalAnswers.returnsFirstArg());

        Project project = projectService.queryOneOrCreate("default", "fb_messenger_test");

        Assert.assertEquals("default", project.getName());
        Assert.assertEquals("fb_messenger_test", project.getUsername());
    }

    @Test
    public void testUpdateProjectAttribute() {
        String projectId = "5c86eff612dbd927eba31d4f";
        Project original = new Project("default", "fb_messenger_test");

        Mockito.doReturn(Optional.of(original)).when(projectRepository).findById(projectId);
        //Mockito.when(projectRepository.queryById(ArgumentMatchers.any()).orElse(null)).thenReturn(original);

        boolean isNotUpdated = projectService.updateProjectAttribute(projectId, "id", "000000000000");
        Assert.assertFalse(isNotUpdated);

        boolean nameUpdated = projectService.updateProjectAttribute(projectId, "name", "manualTest");
        Assert.assertTrue(nameUpdated);
        Assert.assertEquals("manualTest", original.getName());

        boolean prefixUpdated = projectService.updateProjectAttribute(projectId, "prefix", "def");
        Assert.assertTrue(prefixUpdated);
        Assert.assertEquals("def", original.getPrefix());

        boolean tourStatusUpdated = projectService.updateProjectAttribute(projectId, "tourStatus", "false");
        Assert.assertTrue(tourStatusUpdated);
        Assert.assertEquals("false", original.getTourStatus());

        boolean commitUrlPrefixUpdated = projectService.updateProjectAttribute(projectId, "commitUrlPrefix", "origin");
        Assert.assertTrue(commitUrlPrefixUpdated);
        Assert.assertEquals("origin", original.getCommitUrlPrefix());
    }

    @Test
    public void testGetByProjectNameAndUsername() {
        Mockito.when(projectRepository.findByNameAndUserId("default", "fb_messenger_test")).thenReturn(new Project("default", "fb_messenger_test"));

        Project project = projectService.queryByProjectNameAndUsername("default", "fb_messenger_test");

        Assert.assertEquals("default", project.getName());
        Assert.assertEquals("fb_messenger_test", project.getUsername());
    }

    @Test
    public void testFindProjectById() {
        String projectId = "5c86eff612dbd927eba31d4f";

        Mockito.doReturn(Optional.of(new Project("default", "fb_messenger_test"))).when(projectRepository).findById(projectId);
        //Mockito.when(projectRepository.queryById(ArgumentMatchers.any()).orElse(null)).thenReturn(new Project("default", "fb_messenger_test"));

        Project project = projectService.queryProjectById(projectId);

        Assert.assertEquals("default", project.getName());
        Assert.assertEquals("fb_messenger_test", project.getUsername());
    }

    @Test
    public void testGetProjectByUserName() {
        Project p1 = new Project("project_one", "fb_messenger_test");
        Project p2 = new Project("project_two", "fb_messenger_test");
        List<Project> projects = new ArrayList<>();
        projects.add(p1);
        projects.add(p2);

        Mockito.when(projectRepository.findAllByUserId("fb_messenger_test")).thenReturn(projects);

        List<Project> getProjects = projectService.queryProjectByUsername("fb_messenger_test");

        Assert.assertEquals(2, getProjects.size());
    }
}
