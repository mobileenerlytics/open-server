package com.mobileenerlytics.eagle.testerpro.resource;


import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.service.BranchService;
import com.mobileenerlytics.eagle.testerpro.service.ProjectService;
import com.mobileenerlytics.eagle.testerpro.util.web.HeaderUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class ProjectResourceTest {

    @MockBean
    private ProjectService projectService;

    @MockBean
    private HeaderUtils headerUtils;

    @MockBean
    private BranchService branchService;

    @Context
    HttpHeaders httpHeaders;

    @Autowired
    ApplicationContext applicationContext;

    @SpyBean
    private ProjectResource projectResource;

    @Test
    public void returnProjectByUsername() {
        List<Project> projectList = new ArrayList<>();
        Project originalProject = new Project("default", "fb_messenger_test");
        projectList.add(originalProject);
        Mockito.when(headerUtils.getUsernameFromHeader(httpHeaders)).thenReturn("fb_messenger_test");
        Mockito.when(projectService.queryProjectByUsername("fb_messenger_test")).thenReturn(projectList);

        Response response = projectResource.getProjects();
        List<Project> projects = (List<Project>) response.getEntity();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(1, projects.size());
        Assert.assertEquals("default", projects.get(0).getName());
        Assert.assertEquals("fb_messenger_test", projects.get(0).getUsername());
    }

    @Test
    public void returnProjectById() {
        Project originalProject = new Project("default", "fb_messenger_test");
        Mockito.when(projectService.queryProjectById("5c9169433f80434072874c98")).thenReturn(originalProject);

        Project project = projectResource.getProject("5c9169433f80434072874c98");

        Assert.assertEquals("default", project.getName());
        Assert.assertEquals("fb_messenger_test", project.getUsername());
    }

    @Test
    public void updateAttribute() {
        Mockito.when(projectService.updateProjectAttribute("5c9169433f80434072874c98", "name", "main")).thenReturn(true);

        Response response = projectResource.updateProject("5c9169433f80434072874c98", "name", "main");

        Assert.assertEquals(200, response.getStatus());
    }

    @Test(expected = NotFoundException.class)
    public void getProjectReturnNull() {
        Mockito.when(headerUtils.getUsernameFromHeader(httpHeaders)).thenReturn("fb_messenger_test");
        Mockito.when(projectService.queryProjectByUsername("fb_messenger_test")).thenReturn(null);

        Response response = projectResource.getProjects();
    }

    @Test(expected = BadRequestException.class)
    public void updateAttributeFail() {
        Mockito.when(projectService.updateProjectAttribute("5c9169433f80434072874c98", "name", "main")).thenReturn(false);

        Response response = projectResource.updateProject("5c9169433f80434072874c98", "name", "main");
    }
}
