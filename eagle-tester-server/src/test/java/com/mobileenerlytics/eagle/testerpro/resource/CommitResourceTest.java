package com.mobileenerlytics.eagle.testerpro.resource;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import com.mobileenerlytics.eagle.testerpro.entity.Contributor;
import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import com.mobileenerlytics.eagle.testerpro.service.BranchService;
import com.mobileenerlytics.eagle.testerpro.service.CommitService;
import com.mobileenerlytics.eagle.testerpro.service.ProjectService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class CommitResourceTest {

    @MockBean
    private CommitService commitService;

    @MockBean
    private ProjectService projectService;

    @MockBean
    private BranchService branchService;

    @Context
    SecurityContext context;

    @Autowired
    ApplicationContext applicationContext;

    @SpyBean
    private CommitResource commitResource;

    private Project project;
    private Branch branch;
    private Contributor contributor;
    private String commitId;

    @Before
    public void setUp() {
        project = new Project("default", "fb_messenger_test");
        project.setId("5c9169433f80434072874c98");
        contributor = new Contributor("reprocess@mobileenerlytics.com", "Reprocess");
        branch = new Branch("manual-test-branch", new Date(), project);
        commitId = "5c9169433f80434072874c77";
    }

    @Test
    public void queryCommits() {
        List<Commit> originalCommits = new ArrayList<>();
        Commit c1 = new Commit("T1", branch, contributor, new Date());
        Commit c2 = new Commit("T2", branch, contributor, new Date());
        originalCommits.add(c1);
        originalCommits.add(c2);
        Mockito.when(projectService.queryProjectById(project.getId())).thenReturn(project);
        Mockito.when(commitService.queryByProjectAndBranchName(project, branch.getBranchName(), 2)).thenReturn(originalCommits);

        List<Commit> commits = commitResource.getCommits(2, branch.getBranchName(), null, null, project.getId());

        Assert.assertEquals(2, commits.size());
        Assert.assertEquals("T1", commits.get(0).getHash());
    }

    @Test
    public void queryCommitsMiddleHash() {
        List<Commit> originalCommits = new ArrayList<>();
        Commit c1 = new Commit("T1", branch, contributor, new Date());
        Commit c2 = new Commit("T2", branch, contributor, new Date());
        originalCommits.add(c1);
        originalCommits.add(c2);
        Mockito.when(projectService.queryProjectById(project.getId())).thenReturn(project);
        Mockito.when(branchService.queryOneByNameAndProject(branch.getBranchName(), project)).thenReturn(Optional.of(branch));
        Mockito.when(commitService.findByHashAndBranch("T1", branch)).thenReturn(Optional.of(c1));
        Mockito.when(commitService.queryCommitsWithMiddleHash(project, branch.getBranchName(), 2, Sort.by(Sort.Direction.ASC, "updatedMs"), c1.getUpdatedMs(), false)).thenReturn(originalCommits);
        Mockito.when(commitService.queryCommitsWithMiddleHash(project, branch.getBranchName(), 2, null, c1.getUpdatedMs(), true)).thenReturn(originalCommits);

        List<Commit> commits = commitResource.getCommits(4, branch.getBranchName(), "T1", null, project.getId());

        Assert.assertEquals(4, commits.size());
    }

    @Test
    public void deleteCommit() throws DeletedException {
        Mockito.when(commitService.queryOneAndDelete(commitId)).thenReturn(true);

        Response response = commitResource.deleteCommitById(commitId, project.getId());

        Assert.assertEquals(200, response.getStatus());

        Assert.assertEquals("Commit deleted successfully", response.getEntity());
    }

    @Test(expected = NotFoundException.class)
    public void deleteCommitNotFound() throws DeletedException {
        Mockito.when(commitService.queryOneAndDelete(commitId)).thenReturn(false);

        Response response = commitResource.deleteCommitById(commitId, project.getId());
    }

    @Test(expected = BadRequestException.class)
    public void deleteCommitNoProject() throws DeletedException {
        Response response = commitResource.deleteCommitById(commitId, null);

    }

    @Test(expected = BadRequestException.class)
    public void getCommitsNoProject() {
        List<Commit> commits = commitResource.getCommits(2, branch.getBranchName(), null, null, null);
    }

    @Test(expected = BadRequestException.class)
    public void getCommitsMiddleHashNoBranch() {
        List<Commit> commits = commitResource.getCommits(2, null, "T1", null, project.getId());
    }
}
