#### API Docs

**Summary**
This folder contains a text file, a json file annd an adoc file. All these files contain the API documentation for all the APIs in this project.
They are generated using JAXRS-ANALYZER which is available as a maven plugin. It generates the documenation in three formats: PlainText, Swagger and AsciiDoc.

How to genrate rich API documentation from the swagger.json file?
* Upload the swagger.json file in the below link to generate a more user friendly document.
  *http://editor.swagger.io/
