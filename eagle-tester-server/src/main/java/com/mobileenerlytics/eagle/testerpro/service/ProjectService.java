package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.entity.Widget;
import com.mobileenerlytics.eagle.testerpro.exceptions.EagleException;
import com.mobileenerlytics.eagle.testerpro.exceptions.InsufficientDataException;
import com.mobileenerlytics.eagle.testerpro.exceptions.InvalidWidgetException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.BadRequestException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository repo;

    @Autowired
    private BranchService branchService;

    public Project queryOneOrCreate(String projectName, String userId) {
        Project proj = repo.findByNameAndUserId(projectName, userId);
        if(proj != null)
            return proj;
        proj = new Project(projectName, userId);
        return repo.save(proj);
    }

    public boolean updateProjectAttribute(String projectId, String attribute, String value) {
        Optional<Project> opt = repo.findById(projectId);

        if(!opt.isPresent()) {
            return false;
        }

        Project project = opt.get();

        switch (attribute) {
            case "name":
                project.setName(value);
                break;
            case "prefix":
                project.setPrefix(value);
                break;
            case "tourStatus":
                project.setTourStatus(value);
                break;
            case "commitUrlPrefix":
                project.setCommitUrlPrefix(value);
                break;
            default:
                return false;
        }

        repo.save(project);
        return true;
    }

    public Project queryByProjectNameAndUsername(String projectName, String username) { return repo.findByNameAndUserId(projectName, username); }

    public Project queryProjectById(String id) {
        Optional<Project> project = repo.findById(id);
        if(!project.isPresent()) {
            throw new BadRequestException("No project found for specified projectId");
        }
        return project.get();
    }

    public List<Project> queryProjectByUsername(String username) { return repo.findAllByUserId(username); }

    public Optional<Project> findById(String projectId) {
        return repo.findById(projectId);
    }

    public Project setWidgets(Project project, List<Widget> widgets) {
        project.setWidgets(widgets);
        return repo.save(project);
    }

    public List<Widget> getDefaultWidgets(Project project) throws InvalidWidgetException, InsufficientDataException {
        List<Branch> branches = branchService.queryByProject(project, 2);
        if(branches == null || branches.size() < 2)
            throw new InsufficientDataException("Can't create default widgets, need at least 2 branches uploaded");

        return new LinkedList<Widget>() {{
            add(new Widget("branch", "energy", branches.get(0).getBranchName(), null, branches.get(1).getBranchName(), null));
            add(new Widget("branch", "energy", branches.get(0).getBranchName(), null, branches.get(1).getBranchName(), null));
            add(new Widget("test", "energy", branches.get(0).getBranchName(), null, branches.get(1).getBranchName(), null));
            add(new Widget("component", "energy", branches.get(0).getBranchName(), null, branches.get(1).getBranchName(), null));
            add(new Widget("thread", "energy", branches.get(0).getBranchName(), null, branches.get(1).getBranchName(), null));
        }};
    }
}
