package com.mobileenerlytics.eagle.testerpro.util.web;

import org.springframework.beans.factory.annotation.Value;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.HttpHeaders;

public class HeaderUtils {
    @Value("${proUsername:#{null}}")
    String proUsername;

    public String getUsernameFromHeader(HttpHeaders httpHeaders) {
        if(proUsername == null)
            throw new ForbiddenException("Please sign up at https://tester.mobileenerlytics.com/ to obtain a username. " +
                    "Then rerun the server after putting in the obtained username in proUserName property in the properties file. " +
                    "More at https://tester.mobileenerlytics.com/instructions.html?subscription=server");
        return proUsername;
    }
}
