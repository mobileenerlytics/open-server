package com.mobileenerlytics.eagle.testerpro.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;
import java.util.Objects;

@Document(collection = "Project")
@ApiModel(value="Projects Model", description = "Model for the projects API output")
@CompoundIndexes({
        @CompoundIndex(name = "userId_name", def = "{'userId' : 1, 'name': 1}", unique = true)
})
@Getter @Setter @NoArgsConstructor
public class Project {

    @Id
    @JsonProperty("_id")
    private String id;

    @Field("name")
    private String name;

    @Field("userId")
    private String username;

    @Field("prefix")
    private String prefix;

    @Field("commitUrlPrefix")
    private String commitUrlPrefix;

    @Field("tourStatus")
    private String tourStatus;

    @Field("widgets")
    List<Widget> widgets;

    @PersistenceConstructor
    public Project(String name, String username) {
        this.name = name;
        this.username = username;
        this.prefix = "origin/release";// default
        this.tourStatus = "true";
        this.commitUrlPrefix = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return id.equals(project.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
