package com.mobileenerlytics.eagle.testerpro.util.avging;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mobileenerlytics.eagle.testerpro.config.SpringMongoConfig;

public interface IAvgEntry<T> {
    /**
     * Be careful when overriding this. Don't add "dot replacement" in the key. Since this key is also put in db,
     * it will be turned to . when reading from db.
     *
     * @See {@link SpringMongoConfig#mongoTemplate()} {@link org.springframework.data.mongodb.core.convert.MappingMongoConverter#setMapKeyDotReplacement(String)}
     */
     @JsonIgnore
    String getKey();
    @JsonIgnore
    double[] getValues();
    void setValues(double... values);
    T copyConstruct(double... values);
}
