package com.mobileenerlytics.eagle.testerpro.service.files;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.Headers;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import lombok.extern.log4j.Log4j;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;

@Log4j
public class S3FileService extends FileService {
    final AmazonS3 s3;
    private final String bucketName;
    private static Tika tika = new Tika();

    public S3FileService(String accessKey, String secretKey, String bucketName) {
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
        s3 = new AmazonS3Client(awsCreds);
        this.bucketName = bucketName;
    }

    @Override
    public void uploadFolder(String testrecordId, File folder) throws IOException {
        log.info("uploadingFolder to s3 path:" + testrecordId + "/ " + folder);
        File[] files = folder.listFiles();
        uploadFiles(files, testrecordId);
        log.info("uploadingFolder to s3 path:" + testrecordId + "/ " + folder + "is done!");
    }

    @Override
    public Response.ResponseBuilder getFiles(String uri, String range) {
        GetObjectRequest objectRequest = new GetObjectRequest(bucketName, uri);
        String contentType = tika.detect(uri);
        //Partial Content
        if (range != null) {
            String[] ranges = range.split("=")[1].split("-");
            long from = Long.parseLong(ranges[0]);
            long to = from + chunkSize;
            if (ranges.length == 2) {
                to = Long.parseLong(ranges[1]);
            }
            objectRequest.setRange(from, to);
            S3Object object = s3.getObject(objectRequest);
            ObjectMetadata meta = object.getObjectMetadata();
            String contentRange = (String) meta.getRawMetadata().get(Headers.CONTENT_RANGE);
            return Response.ok(object.getObjectContent())
                    .header("Accept-Ranges", "bytes")
                    .header("Content-Range", contentRange)
                    .header("Content-Length", meta.getContentLength())
                    .header("Last-Modified", meta.getLastModified())
                    .status(Response.Status.PARTIAL_CONTENT);

        } else {
            S3Object object = s3.getObject(objectRequest);
            return Response
                    .ok(object.getObjectContent());
        }
    }

    private void uploadFiles(File[] files, String pathFile) throws IOException {
        for (File file : files) {
            if (file.isDirectory()) {
                String folderName = file.getName();
                log.info("Directory: " + folderName);
                uploadFiles(file.listFiles(), pathFile + "/" + folderName);

            } else {
                log.info("uploading name: " + file.getName());
                String path = pathFile + "/" + file.getName();
                s3.putObject(new PutObjectRequest(bucketName, path, file));
            }
        }
    }
}
