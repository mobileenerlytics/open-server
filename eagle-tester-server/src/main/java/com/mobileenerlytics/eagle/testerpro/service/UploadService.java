package com.mobileenerlytics.eagle.testerpro.service;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.mobileenerlytics.eagle.testerpro.entity.*;
import com.mobileenerlytics.eagle.testerpro.util.db.UploadRunnable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class UploadService {
    // upload status
    public static final String UPLOAD_STATUS_QUEUED = "Queued";
    public static final String UPLOAD_STATUS_PROCESSING = "Processing";
    public static final String UPLOAD_STATUS_DONE = "Done";
    public static final String UPLOAD_STATUS_FAILED = "Failed";

    private static Logger logger = LoggerFactory.getLogger(UploadService.class);
    private ExecutorService executorService;

    @Autowired
    private ApplicationContext context;
    private String classPath;

    @Autowired
    private UploadRepository uploadRepository;


    @PreDestroy
    public void shutdown() {
        logger.info("Shutting down DB service");
        try {
            executorService.shutdownNow();
            executorService.awaitTermination(5, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @PostConstruct
    public void init() {
        // Start database service that handles incoming files
        ThreadFactory namedThreadFactory =
                new ThreadFactoryBuilder().setNameFormat("upload-thread-%d").build();
        int numCores = Runtime.getRuntime().availableProcessors();

        // Eprof itself runs many threads, we don't want to cause contention in eprof by starting too many instances.
        // This allows each Eprof instance to easily run roughly 4 threads.
        int numThreads = 1 + (numCores / 4);
        executorService = Executors.newFixedThreadPool(numThreads, namedThreadFactory);

        URLClassLoader classLoader = (URLClassLoader) Thread.currentThread().getContextClassLoader();
        Set<URL> urls = new HashSet<>();
        while(true) {
            urls.addAll(Arrays.asList(classLoader.getURLs()));

            ClassLoader parent = classLoader.getParent();
            if(!(parent instanceof URLClassLoader))
                break;
            classLoader = (URLClassLoader) parent;
        }

        classPath = urls.stream().map(URL::getPath)
                // Don't put eagle-tester-server and eagle-tester-saas in this classpath that runs eprof,
                // spring gets confused otherwise
                .map(p -> FileSystems.getDefault().getPath(p))
                // In prod, server file names could be eagle-tester-server*jar and eagle-tester-saas*jar
                // In dev, eagle-tester-saas file names include /resources/main, /classes/java/main
                .filter(p -> !p.getFileName().toString().contains("eagle-tester-") && !p.getFileName().toString().equals("main"))
                .map(Path::toString)
                .collect(Collectors.joining(":"));
    }

    public Upload createUpload(final File file, final String commitHash, final String pkgName, final Project project,
                               final String branchName, final String authorEmail, final String authorName) {
        Upload upload = new Upload(file.getAbsolutePath(), commitHash, pkgName, project, branchName, authorEmail,
                authorName, new Date());
        return uploadRepository.save(upload);
    }

    public void submit(final File uploadedFile, final String commitHash, final Project project, final String branchName,
                       final String authorEmail, final String authorName, final String pkgName, final boolean isSymbolic) {
        final Upload upload = createUpload(uploadedFile, commitHash, pkgName, project, branchName, authorEmail, authorName);
        Runnable runnable = context.getBean(UploadRunnable.class, classPath, uploadedFile, upload, isSymbolic);
        executorService.execute(runnable);
    }

    public void submit(final Upload upload, final boolean isSymbolic) {
        File uploadedFile = new File(upload.getZipLocation());
        if(!uploadedFile.exists())
            throw new ServerErrorException("Failed to reprocess. Upload's zip file does not exist anymore", Response.Status.INTERNAL_SERVER_ERROR);
        Runnable runnable = context.getBean(UploadRunnable.class, classPath, uploadedFile, upload, isSymbolic);
        executorService.execute(runnable);
    }

    public List<Upload> queryUploads(Project project, int count) {
        Pageable pageable = PageRequest.of(0, count);
        return uploadRepository.queryByProject(project, pageable);
    }

    @Transactional
    Upload addTestRecord(Upload upload, TestRecord testRecord) {
        upload.getTestRecords().add(testRecord);
        return uploadRepository.save(upload);
    }

    public Optional<Upload> findById(String uploadId) {
        return uploadRepository.findById(uploadId);
    }

    @Transactional
    public Upload setJobStatus(Upload upload, String status) {
        upload.setJobStatus(status);
        return uploadRepository.save(upload);
    }
}
