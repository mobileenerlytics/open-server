package com.mobileenerlytics.eagle.testerpro.exceptions;

public class InvalidWidgetException extends EagleException {
    public InvalidWidgetException(String param, String value) {
        super("Invalid widget " + param + " " + value);
    }
}
