package com.mobileenerlytics.eagle.testerpro.resource;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.service.BranchService;
import com.mobileenerlytics.eagle.testerpro.service.ProjectService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Controller
@Path("api/branches")
@Api(value = "Branches API", position = 2, produces = MediaType.APPLICATION_JSON, description = "API to get the branch details corresponding to a project for a particular user")
public class BranchResource {
    private static Logger logger = LoggerFactory.getLogger(BranchResource.class);
    private static final String MSG_FORBIDDEN = "projectId is required";
    private static final String MSG_BRANCH_NOT_FOUND = "Branches not found";
    @Context HttpHeaders httpHeaders;

    @Autowired
    private BranchService branchService;

    @Autowired
    private ProjectService projectService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value="Find the branch details by projectId, count, prefix and branchName", nickname = "getBranches", response = Branch.class, responseContainer = "List",
            notes="curl -u demo:password 'https://tester.mobileenerlytics.com/api/branches?count=1&prefix=origin/master&projectId=abd3d359-d2b8-4454-bdde-b08fcd21b834'"
    )
    @ApiResponses(value = {
            @ApiResponse(code=403, message=MSG_FORBIDDEN),
            @ApiResponse(code = 404, message = MSG_BRANCH_NOT_FOUND)
    } )
    public List<Branch> getBranches(
            @ApiParam(value="Prefix of the branch",required=false)@QueryParam("prefix") String prefix,
            @ApiParam(value="Name of the branch for which the details are required",required=false)@QueryParam("branchName") String branchName,
            @ApiParam(value="Number of commits to be returned",required=false)@QueryParam("count") int count,
            @ApiParam(value="Id of the project corresponding to the user",required=true)@QueryParam("projectId") String projectId) {
        if (projectId == null) throw new ForbiddenException(MSG_FORBIDDEN);

        Project project = projectService.queryProjectById(projectId);
        if(project == null) {
            logger.error("An error occurred at GET /api/branches: No project found for specified projectId");
            throw new BadRequestException("No project found for specified projectId");
        }

        if (branchName == null && (prefix == null || prefix.length() == 0))
            return branchService.queryByProject(project, count);
        if (prefix == null || prefix.length() == 0)
            return branchService.queryByBranchNameAndProject(branchName, project, count);

        String branchPrefix = prefix + "*";
        return branchService.queryByPrefix(project, branchPrefix, count);
    }

}
