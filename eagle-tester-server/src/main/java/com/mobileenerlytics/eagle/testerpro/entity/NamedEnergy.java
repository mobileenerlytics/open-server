package com.mobileenerlytics.eagle.testerpro.entity;

import com.mobileenerlytics.eagle.testerpro.util.avging.IAvgEntry;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.Nullable;

import java.util.Objects;

public class NamedEnergy implements IAvgEntry<NamedEnergy> {
    public final String processName;
    public final String name;
    public double energy;
    public double energySquared;
    public double power;
    public double powerSquared;

    @PersistenceConstructor
    public NamedEnergy(String processName, String name) {
        this.processName = processName;
        this.name = name;
    }


    public NamedEnergy(String processName, String name, double energyUAh, long durationMs) {
        this(processName, name, energyUAh, durationMs > 0 ? 3600 * energyUAh / durationMs : 0);
    }

    private NamedEnergy(String processName, String name, double energyuAh, double powermA) {
        this(processName, name, energyuAh, energyuAh * energyuAh, powermA, powermA * powermA);
    }

    private NamedEnergy(@Nullable String processName, String name, double energy, double energySquared, double power, double powerSquared) {
        this.processName = processName;
        this.name = name;
        this.energy = energy;
        this.energySquared = energySquared;
        this.power = power;
        this.powerSquared = powerSquared;
    }

    @Override
    public String getKey() {
        return processName + "--" + name;
    }

    @Override
    public double[] getValues() {
        return new double[]{energy, energySquared, power, powerSquared};
    }

    @Override
    public void setValues(double... values) {
        energy = values[0];
        energySquared = values[1];
        power = values[2];
        powerSquared = values[3];
    }

    @Override
    public NamedEnergy copyConstruct(double... values) {
        return new NamedEnergy(processName, name, values[0], values[1], values[2], values[3]);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NamedEnergy that = (NamedEnergy) o;
        return Objects.equals(processName, that.processName) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(processName, name);
    }
}
