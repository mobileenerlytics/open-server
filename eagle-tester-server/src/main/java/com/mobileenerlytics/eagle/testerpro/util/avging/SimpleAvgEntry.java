package com.mobileenerlytics.eagle.testerpro.util.avging;

import java.util.Objects;

public class SimpleAvgEntry implements IAvgEntry<SimpleAvgEntry> {
    final String key;
    double value;

    public SimpleAvgEntry(String key, double value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public double[] getValues() {
        return new double[]{value};
    }

    @Override
    public void setValues(double... values) {
        this.value = values[0];
    }

    @Override
    public SimpleAvgEntry copyConstruct(double... values) {
        return new SimpleAvgEntry(key, values[0]);
    }

    @Override
    public String toString() {
        return key + " -> " + value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleAvgEntry that = (SimpleAvgEntry) o;
        return Double.compare(that.value, value) == 0 &&
                Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }
}
