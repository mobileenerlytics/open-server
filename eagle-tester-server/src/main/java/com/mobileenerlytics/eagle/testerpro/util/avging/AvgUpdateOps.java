package com.mobileenerlytics.eagle.testerpro.util.avging;

import java.util.*;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toSet;

public abstract class AvgUpdateOps<T extends IAvgEntry<T>> {
    final int count;
    final double LOWER_BOUND = 0.01;

    AvgUpdateOps(int count) {
        this.count = count;
    }

    public abstract long updateAvg(long old, long update);

    public abstract double updateAvg(double old, double update);

    public void updateOldMap(Map<String, T> oldMap, Collection<T> update) {
        update.stream().forEach(t -> {
            String key = t.getKey();
            if(!oldMap.containsKey(key)) {
                // For subtraction it should never come here!
                double[] values = Arrays.stream(t.getValues()).map(v -> v/(count + 1)).toArray();
                if(values[0] >= LOWER_BOUND) {
                    T avg = t.copyConstruct(values);
                    oldMap.put(key, avg);
                }
            } else {
                T old = oldMap.get(key);
                double[] newAvg = IntStream.range(0, old.getValues().length)
                        .mapToDouble(i -> updateAvg(old.getValues()[i], t.getValues()[i]))
                        .toArray();

                if(newAvg[0] >= LOWER_BOUND)
                    old.setValues(newAvg);
                else
                    oldMap.remove(key);
            }
        });

        Set<String> allKeys = new HashSet<>();
        allKeys.addAll(oldMap.keySet());
        allKeys.removeAll(update.stream().map(IAvgEntry::getKey).collect(toSet()));

        for(String key: allKeys) {
            T old = oldMap.get(key);
            double[] newAvg = IntStream.range(0, old.getValues().length)
                    .mapToDouble(i -> updateAvg(old.getValues()[i], 0))
                    .toArray();
            if(newAvg[0] >= LOWER_BOUND)
                old.setValues(newAvg);
            else
                oldMap.remove(key);
        }
    }

    public static final class AvgUpdateAdd extends AvgUpdateOps {
        public AvgUpdateAdd(int count) {
            super(count);
        }

        public double updateAvg(double old, double update) {
            return (count * old + update) / (count + 1);
        }

        public long updateAvg(long old, long update) {
            return (count * old + update) / (count + 1);
        }
    }

    public static final class AvgUpdateSub extends AvgUpdateOps {
        public AvgUpdateSub(int count) {
            super(count);
        }

        public double updateAvg(double old, double update) {
            return (count * old - update) / (count - 1);
        }

        public long updateAvg(long old, long update) {
            return (count * old - update) / (count - 1);
        }
    }
}
