package com.mobileenerlytics.eagle.testerpro.resource;

import com.mobileenerlytics.eagle.testerpro.entity.*;
import com.mobileenerlytics.eagle.testerpro.service.BranchService;
import com.mobileenerlytics.eagle.testerpro.service.ContributorService;
import com.mobileenerlytics.eagle.testerpro.service.ProjectService;
import com.mobileenerlytics.eagle.testerpro.service.UploadService;
import com.mobileenerlytics.eagle.testerpro.util.Constants;
import com.mobileenerlytics.eagle.testerpro.util.web.HeaderUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
@Path("api/upload")
public class UploadResource {
    private static Logger logger = LoggerFactory.getLogger(UploadResource.class);

    @Autowired
    private ProjectService projectService;

    @Autowired
    private BranchService branchService;

    @Autowired
    private ContributorService contributorService;

    @Autowired
    private UploadService uploadService;

    @Autowired
    private HeaderUtils headerUtils;

    @Context
    SecurityContext context;

    @Context
    HttpHeaders httpHeaders;

    @POST
    @Path("version_energy")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @RolesAllowed({"default", "manual"})
    @Deprecated public Response uploadTraces(
            @FormDataParam("file") InputStream fileInputStream,
            @FormDataParam("file") FormDataContentDisposition contentDispositionHeader,
            @FormDataParam("device") String deviceName,
            @FormDataParam("author_name") String authorName,
            @FormDataParam("author_email") String authorEmail,
            @FormDataParam("commit") String commitHash,
            @FormDataParam("branch") String branchName,
            @FormDataParam("project_name") String projectName,
            @FormDataParam("symbolic") boolean isSymbolic,  // false by default when missing, so should be backward compatible
            @FormDataParam("pkg") String pkgName) throws IOException {
        logger.debug("upload/version_energy");
        boolean isManualUser = context.isUserInRole(Constants.USER_ROLE_MANUAL);
        if (isManualUser && !branchName.equals(Constants.BRANCH_MANUAL))
            throw new BadRequestException("branch name should be: " + Constants.BRANCH_MANUAL);
        String fileName = contentDispositionHeader.getFileName();
        String userName = headerUtils.getUsernameFromHeader(httpHeaders);
        if (projectName == null || projectName.equals("")) projectName = "default";

        Project project = projectService.queryOneOrCreate(projectName, userName);

        File file = File.createTempFile(project.getId() + "-hash-" + commitHash + "-", fileName);
        Files.copy(fileInputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        logger.info(String.format("Upload %s %s %s %s %s %s %s", branchName, commitHash, projectName,
                authorEmail, authorName, pkgName, file.getAbsolutePath()));

        uploadService.submit(file, commitHash, project, branchName, authorEmail, authorName, pkgName, isSymbolic);
        return Response.ok().build();
    }

    @GET
    @Path("reprocess")
    @RolesAllowed({"default", "manual"})
    @ApiOperation(value="Reprocess an upload using upload id")
    public Response reprocess(
            @ApiParam(value="Upload id that need to be reprocessed.", required=true)
            @QueryParam("upload_id") String uploadId,
            @QueryParam("symbolic") boolean isSymbolic  // false by default when missing, so should be backward compatible
    ) {
        Optional<Upload> optUpload = uploadService.findById(uploadId);
        if(!optUpload.isPresent())
            throw new NotFoundException();
        uploadService.submit(optUpload.get(), isSymbolic);
        return Response.ok().build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value="Find the uploads by projectId, count", nickname = "getUploads", response = Upload.class, responseContainer = "List",
            notes="curl -u demo:password 'https://tester.mobileenerlytics.com/api/upload?projectId=abd3d359-d2b8-4454-bdde-b08fcd21b834&count=1"
    )
    public List<Upload> getUploads(
            @ApiParam(value="Number of uploads to be returned.", required=true)
            @QueryParam("count") int count,
            @ApiParam(value="Id of the project corresponding to the user", required=true)
            @QueryParam("projectId") String projectId) {

        if (projectId == null) throw new BadRequestException("projectId is required");
        if(count < 1) {
            count = 10;
        }
        Project project = projectService.queryProjectById(projectId);
        return uploadService.queryUploads(project, count);
    }
}
