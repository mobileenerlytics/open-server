package com.mobileenerlytics.eagle.testerpro.exceptions;

public class DeletedException extends TransactionException {
    public DeletedException(Class entity) {
        super("We were trying to modify" + entity.getSimpleName() + " but turns out this entity has been deleted from the database possibly" +
                "in a different transaction");
    }
}
