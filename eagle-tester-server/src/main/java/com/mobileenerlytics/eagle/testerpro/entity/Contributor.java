package com.mobileenerlytics.eagle.testerpro.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Objects;

@Document(collection = "Contributor")
@Getter @Setter
public class Contributor {
    @Id
    private String email;

    @Field("name")
    private String name;

    /**
     * Counts number of commits made by this contributor
     */
    @Field("count")
    private int count;

    @PersistenceConstructor
    public Contributor() {

    }

    public Contributor(String email, String name) {
        this.email = email;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contributor that = (Contributor) o;
        return email.equals(that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}
