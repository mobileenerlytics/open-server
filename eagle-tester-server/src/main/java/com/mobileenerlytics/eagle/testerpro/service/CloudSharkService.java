package com.mobileenerlytics.eagle.testerpro.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

@Service
public class CloudSharkService {
    static String CS_API = "1f80de90a7839732ae254d03c0bdcc25";

    public String generateSharkCloudLink(File file, String displayName) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost("https://www.cloudshark.org/api/v1/" + CS_API + "/upload/");
        MultipartEntityBuilder builder = MultipartEntityBuilder.create()
                .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                .addTextBody("filename", displayName)
                .addBinaryBody("file", file);
        HttpEntity entity = builder.build();
        post.setEntity(entity);
        HttpResponse response = client.execute(post);

        if (response.getStatusLine().getStatusCode() == 200) {
            Gson gson = new Gson();
            JsonObject jsonObj = gson.fromJson(new InputStreamReader(response.getEntity().getContent()), JsonObject.class);
            String fileId = jsonObj.getAsJsonPrimitive("id").getAsString();
            String resultURL = "https://www.cloudshark.org/captures/" + fileId;
            return resultURL;
        } else {
            return null;
        }
    }
}