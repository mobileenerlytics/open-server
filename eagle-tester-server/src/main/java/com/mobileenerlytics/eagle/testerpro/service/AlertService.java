package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.*;
import com.mobileenerlytics.eagle.testerpro.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.*;

@Service
public class AlertService {
    private static AlertService instance;

    @Autowired
    private AlertRepository alertRepository;

    @Autowired
    private CommitService commitService;

    @Autowired
    private EmailService emailService;

    public static AlertService getInstance() {
        if (instance != null) return instance;
        return instance = new AlertService();
    }

    public void invokeAlert(Commit commit) throws MessagingException {
        Branch branch = commit.getBranch();
        String branchName = branch.getBranchName();
        Project project = branch.getProject();
        List<Commit> commits = commitService.queryByProjectAndBranchName(project, branchName, Constants.ALERT_MINIMUM_COMMITS_NUMBER);
        if (commits.size() < Constants.ALERT_MINIMUM_COMMITS_NUMBER) return;

        // find alert tests
        Set<String> testNames = getTestNames(commits);
        List<String> alertTestNames = new ArrayList<>();
        for (String testName : testNames) {
            boolean isAlerted  = isAlertedBefore(project, branchName, testName, commits);
            if (!isAlerted  && shouldAlert(commits, testName))
                alertTestNames.add(testName);
        }
        if (alertTestNames.size() == 0)
            return;

        // send email
//        Document middleCommit = commits.get(commits.size() / 2);
//        long middleUpdatedMS = ((Date) middleCommit.get("updatedMs")).getTime();
//        InetAddress inetAddress = InetAddress.getLocalHost();
//        System.out.println("IP Address:- " + inetAddress.getHostAddress());
//        String baseUrl = inetAddress.getLocalHost().getHostName();

//        String url = baseUrl + "/commit?count=10&branchName=origin/master&middleUpdatedMS=" + middleUpdatedMS;

        String contributor = commit.getContributor().getEmail();
        String content =  "Hi " + contributor + ", \n\n " + "The commits with test names + " + alertTestNames + "  in branch " + branchName + " are going higher. Go to "
                + "the dashboard "
                + " for full report.\n\nMobile Enerlytics";
        emailService.sendEmail(contributor + ",", content, "Eager Tester commits alert");
    }


    /*
    check the first commit's updateMs, compare to lastest alert (same pid, bName, testName,  sorted by updatedMs),
    if the commit's updateMs is between startMs and endMs, return false
    else return true
     */
    private boolean isAlertedBefore(Project project, String branchName, String testName, List<Commit> commits) {
        if (commits.size() < Constants.ALERT_MINIMUM_COMMITS_NUMBER)
            throw new RuntimeException("invalid commits");
        Date curUpdatedMs = new Date();
//        Date startMs = (Date) commits.get(Constants.AlertStartCommitsIndex).get("updatedMs");
        Date endMs = commits.get(Constants.ALERT_END_COMMITS_INDEX).getUpdatedMs();

        // find latest matching alert
        Pageable pageable = PageRequest.of(0, 1, Sort.Direction.ASC,"updatedMs");
        Alert latestAlert = alertRepository.findByBranchNameAndProjectAndTestName(branchName, project, testName, pageable).get(0);
        // should alert, and insert
        if (latestAlert == null || curUpdatedMs.after(latestAlert.getEndMs())) {
            Alert alert = new Alert();
            alert.setEndMs(endMs);
            alert.setUpdatedMs(curUpdatedMs);
            alertRepository.save(alert);
            return true;
        }

        return false;
    }

    private boolean shouldAlert(List<Commit> commits, String testName) {
        double medianOld, medianNew;
        int minimumTests = Constants.ALERT_MINIMUM_TESTS_NUMBER;
        int testCount = 0;
        List<Double> values = new ArrayList<>();

        // old commits
        for (int i = 0; i <= commits.size() / 2; i++) {
            Commit c = commits.get(i);
            Set<TestStats> testStatsSet = c.getTestStats();
            double value = 0;
            for (TestStats testStats: testStatsSet) {
                if (testName.equals(testStats.getTestName())) value = testStats.getEnergy();
            }
            if (value != 0) testCount++;
            values.add(value);
        }
        if (testCount < minimumTests / 2) return false;
        Collections.sort(values);
        medianOld = values.get(values.size() / 2);

        // new commits
        values.clear();
        testCount = 0;
        for (int i = commits.size() / 2; i < commits.size(); i++) {
            Commit c = commits.get(i);
            Set<TestStats> testStatsSet = c.getTestStats();
            double value = 0;
            for (TestStats testStats: testStatsSet)
                if (testName.equals(testStats.getTestName()))
                    value = testStats.getEnergy();
            if (value != 0) testCount++;
            values.add(value);
        }
        Collections.sort(values);
        if (testCount < minimumTests / 2) return false;
        medianNew = values.get(values.size() / 2);
        return medianNew > (medianOld * (1 + Constants.ALRRT_OFFSET_PERCENTAGE));
    }

    private Set<String> getTestNames(List<Commit> commits) {
        Set<String> names = new HashSet<>();
        for (Commit c : commits) {
            Set<TestStats> testStatsSet = c.getTestStats();
            for (TestStats testStats: testStatsSet) {
                names.add(testStats.getTestName());
            }
        }
        return names;
    }
}
