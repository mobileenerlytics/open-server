package com.mobileenerlytics.eagle.testerpro.util.web;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DebugMapper implements ExceptionMapper<Throwable> {
    @Override
    public Response toResponse(Throwable t) {
        t.printStackTrace();
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(t)
                .build();
    }
}
