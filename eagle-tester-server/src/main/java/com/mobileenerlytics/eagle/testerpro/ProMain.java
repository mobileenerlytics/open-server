package com.mobileenerlytics.eagle.testerpro;

import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@SpringBootApplication
public class ProMain implements CommandLineRunner {
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(ProMain.class);

    @Value("${verbose:false}")
    private boolean isVerbose;

    @Value("${log:eagle.log}")
    private String logFile;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(ProMain.class, args);
    }

    protected void initLogger() {
        // Prepare log4j logger
        InputStream log4jStream = ProMain.class.getResourceAsStream("/server.log4j.properties");
        Properties props = new Properties();
        try {
            props.load(log4jStream);
            log4jStream.close();
        } catch (IOException e) {
            logger.info("Error loading server.log4j.properties");
        }
        props.setProperty("log4j.appender.A1.File", logFile);
        LogManager.resetConfiguration();
        props.getProperty("log4j.appender.A1.File");
        if(isVerbose)
            props.setProperty("log4j.rootLogger", "VERBOSE, A1, console");
        PropertyConfigurator.configure(props);
    }


    public void run(String... args) throws Exception {
        initLogger();
    }
}
