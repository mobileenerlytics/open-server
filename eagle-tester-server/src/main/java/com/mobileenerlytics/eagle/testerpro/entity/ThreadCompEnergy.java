package com.mobileenerlytics.eagle.testerpro.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mobileenerlytics.eagle.testerpro.util.avging.IAvgEntry;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.Nullable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ThreadCompEnergy implements IAvgEntry<ThreadCompEnergy> {
    public final String processName;
    public final String threadId;
    public final String componentId;
    public double power;
    public double powerSquared;
    public double energy;
    public double energySquared;

    @PersistenceConstructor
    public ThreadCompEnergy(String processName, String threadId, String componentId) {
        this.processName = processName;
        this.threadId = threadId;
        this.componentId = componentId;
    }


    public ThreadCompEnergy(String processName, String threadId, String componentId, double energyUAh, long durationMs) {
        this(processName, threadId, componentId, energyUAh, durationMs > 0 ? 3600 * energyUAh / durationMs : 0);
    }

    private ThreadCompEnergy(String processName, String threadId, String componentId, double energyUAh, double powermA) {
        this(processName, threadId, componentId, energyUAh, energyUAh * energyUAh, powermA, powermA * powermA);
    }

    private ThreadCompEnergy(@Nullable String processName, String threadId, String componentId,
                            double energy, double energySquared, double power, double powerSquared) {
        this.processName = processName;
        this.threadId = threadId;
        this.componentId = componentId;

        this.energy = energy;
        this.energySquared = energySquared;
        this.power = power;
        this.powerSquared = powerSquared;
    }

    @Override
    public String getKey() {
        return processName + "-" + threadId + "--" + componentId;
    }

    @Override
    public double[] getValues() {
        return new double[]{energy, energySquared, power, powerSquared};
    }

    @Override
    public void setValues(double... values) {
        energy = values[0];
        energySquared = values[1];
        power = values[2];
        powerSquared = values[3];
    }

    @Override
    public ThreadCompEnergy copyConstruct(double... values) {
        return new ThreadCompEnergy(processName, threadId, componentId, values[0], values[1], values[2], values[3]);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ThreadCompEnergy that = (ThreadCompEnergy) o;

        if (Double.compare(that.energy, energy) != 0) return false;
        if (threadId != null ? !threadId.equals(that.threadId) : that.threadId != null) return false;
        return componentId != null ? componentId.equals(that.componentId) : that.componentId == null;
    }

    @Override
    public int hashCode() {
        int result = 1;
        long temp;
        result = 31 * result + (threadId != null ? threadId.hashCode() : 0);
        temp = Double.doubleToLongBits(energy);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (componentId != null ? componentId.hashCode() : 0);
        return result;
    }
}
