package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import com.mobileenerlytics.eagle.testerpro.entity.TestStats;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface TestStatsRepository extends MongoRepository<TestStats, String> {

    Optional<TestStats> findByCommitAndTestName(Commit commit, String testName);

}
