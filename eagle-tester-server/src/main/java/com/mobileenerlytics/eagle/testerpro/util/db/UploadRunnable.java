package com.mobileenerlytics.eagle.testerpro.util.db;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.mobileenerlytics.eagle.testerpro.entity.*;
import com.mobileenerlytics.eagle.testerpro.exceptions.AppNotFoundException;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import com.mobileenerlytics.eagle.testerpro.exceptions.EagleException;
import com.mobileenerlytics.eagle.testerpro.service.AlertService;
import com.mobileenerlytics.eagle.testerpro.service.CloudSharkService;
import com.mobileenerlytics.eagle.testerpro.service.TestRecordService;
import com.mobileenerlytics.eagle.testerpro.service.UploadService;
import com.mobileenerlytics.eagle.testerpro.service.files.FileService;
import com.mobileenerlytics.eagle.testerpro.util.RunEprof;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.zeroturnaround.zip.ZipUtil;

import javax.mail.MessagingException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

import static java.util.stream.Collectors.toMap;

@Component
@Scope(value = "prototype")
public class UploadRunnable implements Runnable {
    @Autowired
    private TestRecordService testRecordService;

    @Autowired
    private FileService fileService;

    @Autowired
    private CloudSharkService cloudSharkService;

    @Autowired
    private AlertService alertService;

    private static final String TESTNAMEKEY = "tester_logname";
    private static final String DURATION_MS = "durationInMs";

    private final String classPath;
    private final File uploadedFile;
    private Commit commit;
    private Upload upload;
    private int failedTests = 0;

    final boolean isSymbolic;

    private static Logger logger = LoggerFactory.getLogger(UploadRunnable.class);

    @Autowired
    private UploadService uploadService;

    public UploadRunnable(String classPath, File uploadedFile, Upload upload, boolean isSymbolic) {
        this.classPath = classPath;
        this.upload = upload;
        this.uploadedFile = uploadedFile;
        this.isSymbolic = isSymbolic;
    }

    @NonNull
    public Set<AggregateEvent> parseEvent(File eventJson, Map<Integer, String> pids) {
        logger.debug("parseEvent:{}", eventJson.getAbsolutePath());
        Set<AggregateEvent> ret = new HashSet<>();
        for(Map.Entry<Integer, String> e : pids.entrySet()) {
            try {
                FileReader jsonReader = new FileReader(eventJson);
                Type mapType = new TypeToken<Map<Integer, Map<String, List<AggregateEvent.JsonEvent>>>>() {
                }.getType();
                Map<Integer, Map<String, List<AggregateEvent.JsonEvent>>> jsonEventMap = new Gson().fromJson(jsonReader, mapType);
                int pid = e.getKey();
                if(jsonEventMap.containsKey(pid))
                    ret.addAll(AggregateEvent.aggregate(e.getValue(), jsonEventMap.get(pid)));
            } catch (FileNotFoundException fnfe) {
                fnfe.printStackTrace();
            }
        }
        return ret;
    }

    @NonNull
    private Map<Integer, String> findPids(File jsonFile, String pkgName) throws IOException, AppNotFoundException {
        logger.debug("findPids:{} pkg:{}", jsonFile.getAbsolutePath(), pkgName);
        try (FileReader jsonReader = new FileReader(jsonFile)) {
            Type listType = new TypeToken<List<JsonOutput>>() {
            }.getType();
            List<JsonOutput> jsonOutputs = new Gson().fromJson(jsonReader, listType);
            Map<Integer, String> pids = jsonOutputs.parallelStream()
                    .filter(j -> j.taskName.startsWith(pkgName))
                    .collect(toMap(j -> j.taskId, j -> j.taskName));
            if(pids.isEmpty())
                throw new AppNotFoundException();
            return pids;
        }
    }

    @NonNull
    public Set<ThreadCompEnergy> parseThreadCompEnergy(File jsonFile, Map<Integer, String> pids, long durationMs) throws IOException {
        logger.debug("parseThreadCompEnergy:{}", jsonFile.getAbsolutePath());
        Set<ThreadCompEnergy> threadEnergies = new HashSet<>();
        try (FileReader jsonReader = new FileReader(jsonFile)) {
            logger.info("Inserting data for pid {}", pids);
            Type listType = new TypeToken<List<JsonOutput>>() {
            }.getType();
            List<JsonOutput> jsonOutputs = new Gson().fromJson(jsonReader, listType);
            jsonOutputs.stream().filter(j -> pids.containsKey(j.taskId) || pids.containsKey(j.parentTaskId))
                    .forEach( j -> {
                        String processName = null;
                        if(pids.containsKey(j.parentTaskId))
                            processName = pids.get(j.parentTaskId);
                        if(pids.containsKey(j.taskId))
                            processName = pids.get(j.taskId);

                        threadEnergies.add(new ThreadCompEnergy(processName, j.taskName, "CPU", j.perComponentEnergy.CPU, durationMs));
                        threadEnergies.add(new ThreadCompEnergy(processName, j.taskName, "GPU", j.perComponentEnergy.GPU, durationMs));
                        threadEnergies.add(new ThreadCompEnergy(processName, j.taskName, "GPS", j.perComponentEnergy.GPS, durationMs));
                        threadEnergies.add(new ThreadCompEnergy(processName, j.taskName, "NETWORK", j.perComponentEnergy.Network, durationMs));
                        threadEnergies.add(new ThreadCompEnergy(processName, j.taskName, "SCREEN", j.perComponentEnergy.SCREEN, durationMs));
                        threadEnergies.add(new ThreadCompEnergy(processName, j.taskName, "HwDecoder", j.perComponentEnergy.HwDecoder, durationMs));
                        threadEnergies.add(new ThreadCompEnergy(processName, j.taskName, "SDCard", j.perComponentEnergy.SDCard, durationMs));
                        threadEnergies.add(new ThreadCompEnergy(processName, j.taskName, "DRM", j.perComponentEnergy.DRM, durationMs));
                    });
        }
        return threadEnergies;
    }

    @Override
    public void run() {
        try {
            Date proccessed = startProcessing();
            unzipAndProcessEnergy();
            endProcessing(proccessed);
        } catch(MessagingException me) {
            logger.error("Error while processing upload: ", me);
        }
    }

    @Transactional
    private Date startProcessing() {
        Date proccessed = new Date();

        // Delete all past test records if any
        for(TestRecord testRecord : upload.getTestRecords())
            if(testRecord != null)
                try {
                    testRecordService.delete(testRecord);
                } catch (DeletedException e) {
                    e.printStackTrace();
                }
        upload.getTestRecords().clear();

        upload = uploadService.setJobStatus(upload, UploadService.UPLOAD_STATUS_PROCESSING);
        return proccessed;
    }

    private void unzipAndProcessEnergy() {
        String parentDir = uploadedFile.getParent();
        String zipFileName = uploadedFile.getName();
        int dotIndex = zipFileName.lastIndexOf(".zip");
        String dirName = zipFileName.substring(0, dotIndex);
        File unzipDir = new File(parentDir + File.separator + dirName);
        unzipDir.mkdirs();
        ZipUtil.unpack(uploadedFile, unzipDir);

        // Find and process all trace folders
        File[] traceDirs = unzipDir.listFiles(pathname -> pathname.isDirectory());
        for (File traceDir : traceDirs) {
            // check test exist or not
            try {
                File buildPropFile = traceDir.toPath().resolve("build.prop").toFile();
                Properties property = new Properties();
                property.load(new FileReader(buildPropFile));
                String testName = property.getProperty(TESTNAMEKEY);
                long durationMs = Long.parseLong(property.getProperty(DURATION_MS));

                // build eprofOutDir
                logger.info("Processing " + traceDir.getAbsolutePath());
                RunEprof runEprof = new RunEprof(traceDir.getAbsolutePath(), classPath);
                runEprof.processTraces(isSymbolic);

                File eprofOutDir = traceDir.toPath().resolve("eprof.files").toFile().getAbsoluteFile();
                logger.info("Updating database from processed traces at " + traceDir.getAbsolutePath());

                // parse from eprofOutDir
                File energyJson = eprofOutDir.toPath().resolve("app.json").toFile();
                Map<Integer, String> pids = findPids(energyJson, upload.getPkgName());
                Set<ThreadCompEnergy> threadCompEnergies = parseThreadCompEnergy(energyJson, pids, durationMs);
                File eventJson = eprofOutDir.toPath().resolve("events.json").toFile();
                Set<AggregateEvent> events = parseEvent(eventJson, pids);

                //save testrecord with eprofOutDir
                TestRecord testRecord = testRecordService.create(testName, upload, threadCompEnergies, events,
                        property);

                // Upload has changed, refresh
                upload = uploadService.findById(upload.getId()).get();

                // update testrecord and upload files
                upload(testRecord, eprofOutDir);
            } catch (IOException | InterruptedException | EagleException e) {
                failedTests++;
                logger.error("Failed in processing ", e);
                e.printStackTrace();
            }
        }
    }

    private void upload(TestRecord testRecord, File eprofOutDir) throws IOException {
        logger.info("Uploading test record files to S3 " + eprofOutDir);
        fileService.uploadFolder("" + testRecord.getId(), eprofOutDir);
//        String uploadCloudShark = uploadCloudShark(testRecord, eprofOutDir);
//        if (uploadCloudShark != null)
//            testRecord.setCloudsharkLink(uploadCloudShark);
//        return testRecordService.saveTestRecord(testRecord);
    }


    /** TODO: Removing cloud shark since we have stopped paying for it. It always returns null now */
//    private String uploadCloudShark(TestRecord testRecord, File eprofOutDir) {
//        try {
//            File pcap = eprofOutDir.toPath().getParent().resolve("capture.pcap").toFile();
//            if (!pcap.exists())
//                return null;
//            logger.info("Generating sharkLink " + pcap);
//            String link = cloudSharkService.generateSharkCloudLink(pcap, testRecord.getTestName());
//            logger.info("Returned sharkLink: " + link);
//            return link;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    @Transactional
    private void endProcessing(Date proccessed) throws MessagingException {
        long duration = new Date().getTime() - proccessed.getTime();
        upload.setJobDurationMs(duration);
        if(failedTests > 0)
            upload = uploadService.setJobStatus(upload, UploadService.UPLOAD_STATUS_FAILED);
        else
            upload = uploadService.setJobStatus(upload, UploadService.UPLOAD_STATUS_DONE);

        //check alert
        alertService.invokeAlert(commit);
        logger.info("Upload finished with {} failed tests", failedTests);
    }

    class JsonOutput {
        String taskName;
        int taskId;
        int parentTaskId;
        PerComponentEnergy perComponentEnergy = new PerComponentEnergy();
        class PerComponentEnergy {
            double CPU;
            double GPU;
            double GPS;
            double Network;
            double SCREEN;
            double HwDecoder;
            double SDCard;
            double DRM;
        }
    }
}
