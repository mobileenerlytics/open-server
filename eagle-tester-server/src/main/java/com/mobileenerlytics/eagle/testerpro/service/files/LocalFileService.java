package com.mobileenerlytics.eagle.testerpro.service.files;

import lombok.extern.log4j.Log4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.zeroturnaround.zip.commons.FileUtils;

import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;

@Log4j
public class LocalFileService extends FileService {
    final Path outputDirPath;

    LocalFileService(Path outputDirPath) {
        this.outputDirPath = outputDirPath;
    }

    @Override
    public void uploadFolder(String testrecordId, File folder) throws IOException {
        log.info("uploadingFolder to local path:" + testrecordId + "/ " + folder);
        FileUtils.copyDirectory(folder, outputDirPath.resolve(testrecordId).resolve(folder.getName()).toFile());
        return;
    }

    @Override
    public Response.ResponseBuilder getFiles(String uri, String range) throws IOException {
        File file = outputDirPath.resolve(uri).toFile();
        //Partial ContentF
        if (range != null) {
            String[] ranges = range.split("=")[1].split("-");
            long from = Long.parseLong(ranges[0]);
            long to = from + chunkSize;
            if (ranges.length == 2) {
                to = Long.parseLong(ranges[1]);
            }
            if ( to >= file.length() ) {
                to = (int) (file.length() - 1);
            }
            FileInputStream fileInputStream = new FileInputStream(file);
            fileInputStream.skip(from);
            long length = to - from + 1;
            BoundedInputStream boundedInputStream = new BoundedInputStream(fileInputStream, length);
            final String contentRange = String.format( "bytes %d-%d/%d", from, to, file.length());
            return Response.ok(boundedInputStream)
                    .header("Accept-Ranges", "bytes")
                    .header("Content-Range", contentRange)
                    .header("Content-Length", length)
                    .header("Last-Modified", new Date(file.lastModified()))
                    .status(Response.Status.PARTIAL_CONTENT);
        } else {
            log.info("Content " + file);
            return Response.ok(new FileInputStream(file));
        }
    }
}
