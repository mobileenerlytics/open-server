package com.mobileenerlytics.eagle.testerpro.util.db;

import com.fasterxml.jackson.databind.util.StdConverter;

import java.util.Collection;
import java.util.Map;

public class MapToCollectionConverter<V> extends StdConverter<Map<?,V>, Collection<V>> {
    @Override
    public Collection<V> convert(Map<?, V> map) {
        return map.values();
    }
}
