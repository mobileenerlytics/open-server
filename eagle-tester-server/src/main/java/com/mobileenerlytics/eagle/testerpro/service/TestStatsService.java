package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import com.mobileenerlytics.eagle.testerpro.entity.TestRecord;
import com.mobileenerlytics.eagle.testerpro.entity.TestStats;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
public class TestStatsService {
    @Autowired
    private TestStatsRepository repo;

    @Autowired
    private CommitService commitService;

    @Autowired
    private BranchService branchService;

    Optional<TestStats> queryByCommitAndTestName(Commit commit, String testName) {
        return repo.findByCommitAndTestName(commit, testName);
    }

    @Transactional
    @NonNull
    public TestStats addTestRecord(@NonNull TestRecord testRecord) throws DeletedException {
        Commit commit = testRecord.getCommit();
        Optional<TestStats> optTestStats = queryByCommitAndTestName(commit, testRecord.getTestName());
        if (optTestStats.isPresent()) {
            TestStats testStats = optTestStats.get();
            testStats.addTestRecord(testRecord);
            return repo.save(testStats);
        }

        log.info("Creating new TestStats {} on commit {}-{}", testRecord.getTestName(), commit.getId(), commit.getHash());
        TestStats testStats = new TestStats(commit, testRecord.getTestName(), testRecord.getUpdatedMs());
        testStats.addTestRecord(testRecord);
        testStats = repo.save(testStats);

        commitService.addTestStats(testStats);
        branchService.addTest(commit.getBranch(), testRecord.getTestName());

        log.info("Added test record {} to testStats {}-{}", testRecord.getId(), testStats.getId(), testStats.getTestName());
        return testStats;
    }

    @Transactional
    void deleteTestRecord(TestRecord testRecord) throws DeletedException {
        Commit commit = testRecord.getCommit();
        // Find this TestStats
        Optional<TestStats> optTestStats = queryByCommitAndTestName(commit, testRecord.getTestName());
        if(!optTestStats.isPresent())
            // No such TestStats found, probably got deleted in another transaction
            throw new DeletedException(TestStats.class);

        TestStats testStats = optTestStats.get();
        testStats.deleteTestRecord(testRecord);
        repo.save(testStats);

        if(testStats.getTestRecordSet().isEmpty()) {
            commitService.deleteTestStats(testStats);
            branchService.removeTest(commit.getBranch(), testRecord.getTestName());
            repo.delete(testStats);
        }
    }
}
