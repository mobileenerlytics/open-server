package com.mobileenerlytics.eagle.testerpro.resource;

import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

@Controller
@Path("api/alert")
public class AlertResource {
    @Context
    HttpHeaders httpHeaders;

    @GET
    public Response Get() {
        return Response.ok().build();
    }
}
