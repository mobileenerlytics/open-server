package com.mobileenerlytics.eagle.testerpro.resource;

import com.mobileenerlytics.eagle.testerpro.entity.TestRecord;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import com.mobileenerlytics.eagle.testerpro.service.TestRecordRepository;
import com.mobileenerlytics.eagle.testerpro.service.TestStatsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Slf4j
@Controller
@Path("api/redo")
public class RedoTestStatsResource {
    @Autowired
    TestStatsService testStatsService;

    @Autowired
    TestRecordRepository testRecordRepository;

    @GET
    public void redo() {
        for(TestRecord testRecord: testRecordRepository.findAll()) {
            // Add TestRecord to TestStats
            try {
                testStatsService.addTestRecord(testRecord);
            } catch (DeletedException e) {
                log.error("Failed to add " + testRecord.getId());
                e.printStackTrace();
            }
        }
    }
}
