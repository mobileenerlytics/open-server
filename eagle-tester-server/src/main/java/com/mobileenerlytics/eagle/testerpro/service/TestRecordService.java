package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.*;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Slf4j
@Service
public class TestRecordService {

    @Autowired
    private TestRecordRepository repo;

    @Autowired
    private TestStatsService testStatsService;

    @Autowired
    private BranchService branchService;

    @Autowired
    private CommitService commitService;

    @Autowired
    private ContributorService contributorService;

    @Autowired
    private UploadService uploadService;

    public Optional<TestRecord> queryById(String id) {
        return repo.findById(id);
    }

    public List<TestRecord> queryByCommitAndTestName(Commit commit, String testName, int numOfRecords) {
        Pageable pageable = PageRequest.of(0, numOfRecords, Sort.Direction.DESC, "updatedMs");
        return repo.queryAllByTestNameAndCommit(testName, commit, pageable);
    }

    List<TestRecord> findAllByCommit(Commit commit) {
        return repo.findAllByCommit(commit);
    }

    /**
     * Ideally this should be {@link Transactional} but unfortunately {@link org.springframework.data.mongodb.core.mapping.DBRef}
     * don't work with transactions. Specifically, when we create an entry in a document, and add that to a
     * different document via a DBRef, it doesn't work.
     * @param testName
     * @param upload
     * @param threadCompEnergies
     * @param events
     * @param properties
     * @return
     * @throws DeletedException
     */
    public TestRecord create(String testName, Upload upload, Set<ThreadCompEnergy> threadCompEnergies,
                             Set<AggregateEvent> events, Properties properties) throws DeletedException {
        log.info("Creating new TestRecords {}", testName);
        Date date = new Date();
        Project project = upload.getProject();

        // Create a branch
        Branch branch = branchService.queryOneOrCreate(upload.getBranchName(), date, project);

        // Create contributor
        Contributor contributor = contributorService.queryOneOrCreate(upload.getAuthorEmail(), upload.getAuthorName());

        // Create commit
        Commit commit = commitService.findOneOrCreate(upload.getHash(), branch, contributor, date);

        TestRecord testRecord = createTestRecord(testName, commit, threadCompEnergies, events, properties);

        // Add TestRecord to TestStats
        testStatsService.addTestRecord(testRecord);

        // Add TestRecord to Upload
        uploadService.addTestRecord(upload, testRecord);

        return testRecord;
    }

    @Transactional
    TestRecord createTestRecord(String testName, Commit commit, Set<ThreadCompEnergy> threadCompEnergies, Set<AggregateEvent> events, Properties properties) {
        // Create testrecord
        String testDurationStartTime = properties.getProperty("startTime");
        String testDurationEndTime = properties.getProperty("endTime");
        long durationMS = Long.parseLong(testDurationEndTime) - Long.parseLong(testDurationStartTime);
        TestRecord testRecord = new TestRecord(testName, commit, commit.getBranch(), commit.getProject(), durationMS, threadCompEnergies, events);
        return repo.save(testRecord);
    }

    @Transactional
    public void delete(TestRecord testRecord) throws DeletedException {
        testStatsService.deleteTestRecord(testRecord);
        repo.delete(testRecord);
    }

    @Transactional
    public boolean queryOneAndDelete(String testRecordId) throws DeletedException {
        Optional<TestRecord> optTestRecord = repo.findById(testRecordId);
        if(!optTestRecord.isPresent())
            return false;
        TestRecord testRecord = optTestRecord.get();
        delete(testRecord);
        return true;
    }
}
