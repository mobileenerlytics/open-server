package com.mobileenerlytics.eagle.testerpro.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class EmailService {

    private static Logger logger = LoggerFactory.getLogger(EmailService.class);

    private Session session;

    @Value("${server.baseUrl:'http://localhost:38673'}")
    private String baseUrl;

    @Value("${mail.smtp.username:#{null}}")
    String username;

    @Value("${mail.smtp.password:#{null}}")
    String password;

    @Autowired
    Environment environment;

    @PostConstruct
    private void init() {
        Properties emailProperties = new Properties();
        String[] properties = new String[]{"mail.smtp.host", "mail.smtp.port", "mail.smtp.auth",
                "mail.smtp.starttls.enable", "mail.smtp.username", "mail.smtp.password"};
        for(String s : properties) {
            if(environment.containsProperty(s))
                emailProperties.setProperty(s, environment.getProperty(s));
        }

        if(username == null || password == null) {
            logger.error("Failed to create email service");
            return;
        }
        session = Session.getInstance(emailProperties,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
    }

    public String generateResetURL(String tokenStr) {
        String url = baseUrl + "/new_pass.html#" + tokenStr;
        return url;
    }

    public boolean sendEmail(String recipients, String content, String subject) throws MessagingException {
        if(session == null) {
            logger.error("Failed to send email. Email session couldn't be instantiated");
            return false;
        }
        Message message = new MimeMessage(this.session);
        Address[] addresses =  InternetAddress.parse(recipients);
        message.setRecipients(Message.RecipientType.TO,
                addresses);
        message.setSubject(subject);
        message.setText(content);
        Transport.send(message);
        logger.info("Message sent to " + recipients);
        return true;
    }
}