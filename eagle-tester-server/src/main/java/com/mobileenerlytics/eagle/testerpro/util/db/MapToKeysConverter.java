package com.mobileenerlytics.eagle.testerpro.util.db;

import com.fasterxml.jackson.databind.util.StdConverter;

import java.util.Collection;
import java.util.Map;

public class MapToKeysConverter<K> extends StdConverter<Map<K,?>, Collection<K>> {
    @Override
    public Collection<K> convert(Map<K, ?> map) {
        return map.keySet();
    }
}
