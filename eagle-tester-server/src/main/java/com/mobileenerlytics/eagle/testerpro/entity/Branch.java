package com.mobileenerlytics.eagle.testerpro.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mobileenerlytics.eagle.testerpro.util.db.MapToKeysConverter;
import io.swagger.annotations.ApiModel;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.*;

/*
json
  {
    "branch": "release1",
    "totalCommits": 1000,
    "updatedMs": 1543448761000,
    "tests" : ["test1", "test2", "test3", "test4"],
    "commits":["commitiId1", "commitId2"]
  }


 */
@ApiModel(value="Branches Model", description = "Model for the branches API output")
@Document(collection = "Branch")
@CompoundIndexes({
    @CompoundIndex(name = "project_branchName", def = "{'project.$id' : 1, 'branchName': 1}", unique = true)
})
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter @Setter @NoArgsConstructor
public class Branch implements Comparable<Branch> {
    @Id
    @JsonProperty("_id")
    private String id;

    @Field("branchName")
    private String branchName;

    @Field("updatedMs")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date updatedMs;

    @JsonIgnore
    @Field("project")
    @DBRef
    private Project project;

    /**
     * testCounter is a map from testname to a counter counting number of {@link TestStats} in this branch with that testname
     */
    @JsonSerialize(converter = MapToKeysConverter.class)
    @Field("testCounter")
    @JsonProperty("tests")
    private Map<String, Integer> testCounter = new HashMap<>();

    /**
     * Commits is just a transient field, i.e, not in the db. This design decision was made since we anyways were doing
     * explicit joins to find a particular number of commits and were never using this set. This set however was
     * creating consistency problems.
     *
     * It is still kept here to keep the json returned through APIs
     * backward compatible. It is thus only set for the use of APIs and should never be "get". To get the commits, use
     * {@link com.mobileenerlytics.eagle.testerpro.service.CommitService#findAllByBranch(Branch)}
     */
    @Transient
    @Getter(AccessLevel.NONE)
    private SortedSet<Commit> commits = new TreeSet<>();

    @PersistenceConstructor
    public Branch(String branchName, Date updatedMs, Project project) {
        this.branchName = branchName;
        this.updatedMs = updatedMs;
        this.project = project;
    }

    @Override
    public int compareTo(Branch o) {
        // Higher updateMs should appear first in the list
        return o.updatedMs.compareTo(updatedMs);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Branch branch = (Branch) o;
        return id.equals(branch.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
