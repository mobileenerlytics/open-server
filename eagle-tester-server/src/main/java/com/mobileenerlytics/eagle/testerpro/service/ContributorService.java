package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import com.mobileenerlytics.eagle.testerpro.entity.Contributor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;


@Service
public class ContributorService {
    @Autowired
    private ContributorRepository repo;

    @NonNull
    Contributor queryOneOrCreate(String email, @Nullable String name) {
        Contributor contributor = repo.findByEmail(email);
        if(contributor == null) {
            contributor = new Contributor(email, name);
            return repo.save(contributor);
        }
        if(name == null)
            return contributor;
        if(contributor.getName().equals(name))
            return contributor;
        contributor.setName(name);
        return repo.save(contributor);
    }

    Contributor addCommit(Commit commit) {
        Contributor contributor = commit.getContributor();
        contributor.setCount(contributor.getCount() + 1);
        return repo.save(contributor);
    }

    void rmCommit(Commit commit) {
        Contributor contributor = commit.getContributor();
        if(contributor.getCount() == 1) {
            repo.delete(contributor);
            return;
        }
        contributor.setCount(contributor.getCount() - 1);
        repo.save(contributor);
    }
}
