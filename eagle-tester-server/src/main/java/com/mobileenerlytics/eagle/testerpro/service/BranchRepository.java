package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Project;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BranchRepository extends MongoRepository<Branch, String> {
    Optional<Branch> findByBranchNameAndProject(String branchName, Project project);

    @Query(fields = "{'project' : 0, 'commits' : 0}", sort="{'updatedMs': -1}")
    List<Branch> queryAllByProject(Project project);

    @Query(fields = "{'project' : 0, 'commits' : 0}", sort="{'updatedMs': -1}")
    List<Branch> queryAllByBranchNameAndProject(String branchName, Project project);

    @Query(fields = "{'project' : 0, 'commits' : 0}", sort="{'updatedMs': -1}")
    List<Branch> queryAllByProjectAndBranchNameLike(Project project, String branchName);
}
