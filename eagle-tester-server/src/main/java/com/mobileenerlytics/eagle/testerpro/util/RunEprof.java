package com.mobileenerlytics.eagle.testerpro.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

public class RunEprof {
    private final String traceDir;
    private static final Logger logger = LoggerFactory.getLogger("RUNEPROF");
    private final String classPath;

    public RunEprof(String traceDir, String classPath) {
        this.traceDir = traceDir;
        this.classPath = classPath;
    }

    // public void run() {
    //     /**
    //      * Eprof post processing was initially built on the assumption that
    //      * it processes just one trace at a time. However, in this laptop
    //      * client that assumption doesn't hold true.. since several traces
    //      * can be collected back to back and are needed to be processed
    //      * without restarting the client.
    //      *
    //      * One approach was to run the eprof post processing in a different
    //      * JVM (a different process). But, that approach may not be very
    //      * good in the long run since Eprof post processing cannot
    //      * communicate much with the plugin, for example, advancing progress
    //      * bar, showing status in GUI etc..
    //      *
    //      * This commit picked better but trickier approach. We load Eprof
    //      * post-processing and related libraries in a different ClassLoader.
    //      * Every trace instance can simultaneously get processed in the same
    //      * process and they (ideally) won't interfere with each other since
    //      * these are all different classloaders and classes even with same
    //      * name will have different _namespaces_.
    //      */
    //     URLClassLoader classLoader = null;
    //     try {
    //         logger.info("Processing trace: {}", traceDir);
    //         URLClassLoader thisClassLoader = (URLClassLoader) Thread.currentThread().getContextClassLoader();
    //         URL[] urls = thisClassLoader.getURLs();
    //         classLoader = new URLClassLoader(urls);
    //         //classLoader = new URLClassLoader(urls, thisClassLoader);
    //         String eprofMain = "com.android.traceview.MainWindow";
    //         java.lang.reflect.Method m = ClassLoader.class.getDeclaredMethod("findLoadedClass", new Class[]{String.class});
    //         m.setAccessible(true);
    //         Object test1 = m.invoke(classLoader, eprofMain);
    //         // Eprof main class shouldn't be loaded in this classloader yet!!
    //         Assert.isTrue(test1 == null, "Ouch!! Eprof already loaded in this class loader! Every " +
    //                 "trace must be processed by fresh eprof classes.");

    //         Class<?> clazz = classLoader.loadClass(eprofMain);
    //         Method mainMethod = clazz.getMethod("main", String[].class);
    //         String[] params = new String[4];
    //         params[0] = "-tracedir";
    //         params[1] = traceDir;
    //         params[2] = "-noplugin";
    //         params[3] = "-json";
    //         mainMethod.invoke(null, new Object[]{params});
    //         success = true;
    //     } catch (Exception e) {
    //         logger.error("Failed to process trace. Detailed logs in " + traceDir
    //                 + File.separator + "run.out");
    //         e.printStackTrace();
    //     } finally {
    //         if (classLoader != null) {
    //             try {
    //                 classLoader.close();
    //             } catch (IOException e) {
    //                 e.printStackTrace();
    //             }
    //         }
    //     }
    // }

    public void processTraces(boolean isSymbolic) throws IOException, InterruptedException {
        String javaExecutable = new File(System.getProperty("java.home"), "bin/java").getAbsolutePath();
        String vmOptions = "-Xmx8g";

        String extDirs = "-Djava.ext.dirs=" + System.getProperty("java.ext.dirs");

        String profile = "-Dspring.profiles.active=" + (isSymbolic ? "symbolic" : "");

        String[] cmdArray = new String[]{javaExecutable, vmOptions, "-classpath", classPath, extDirs,
                profile, "-Dparent.dir=" + traceDir, "com.mobileenerlytics.eprof.stream.Main"};

        // For debugging only
        String cmd = Arrays.stream(cmdArray).collect(Collectors.joining(" "));
        logger.info("Running {}", cmd);

        ProcessBuilder processBuilder = new ProcessBuilder(cmdArray);
        processBuilder.redirectErrorStream(true);
        Process pr = processBuilder.start();
        BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        String line;
        while ((line = buf.readLine()) != null) {
            logger.info(line);
        }
        pr.waitFor();
    }

}
