package com.mobileenerlytics.eagle.testerpro.util;

import com.mobileenerlytics.eagle.testerpro.util.web.ProSecurityContext;
import org.springframework.stereotype.Component;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

/** This class is necessary for applying {@link ProSecurityContext} which basically
 * just makes all APIs with default role available for pro user. */
@Component
@Provider
@Priority(Priorities.AUTHENTICATION)
@PreMatching
public class ContextFilter implements ContainerRequestFilter {
    @Override
    public void filter(ContainerRequestContext requestContext) {
        final SecurityContext oldContext = requestContext.getSecurityContext();
        requestContext.setSecurityContext(new ProSecurityContext(oldContext));
    }
}
