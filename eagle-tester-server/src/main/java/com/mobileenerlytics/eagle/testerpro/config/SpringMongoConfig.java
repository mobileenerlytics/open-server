package com.mobileenerlytics.eagle.testerpro.config;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration(exclude = {MongoAutoConfiguration.class})
@ComponentScan("com.mobileenerlytics.eagle")
@EnableMongoRepositories(basePackages = {"com.mobileenerlytics.eagle.testertrial.service", "com.mobileenerlytics.eagle.testerpro.service"})
@EnableTransactionManagement
public class SpringMongoConfig extends AbstractMongoClientConfiguration {

    @Value("${uri}")
    private String uri;

    @Value("${database}")
    private String database;

    @Bean
    public MongoClient mongoClient() {
        return MongoClients.create(this.uri);
    }

    @Override
    protected String getDatabaseName() {
        return this.database;
    }
}
