package com.mobileenerlytics.eagle.testerpro.service.files;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
public class FileServiceFactory extends AbstractFactoryBean<FileService> {
    @Value("${aws.access_key_id:#{null}}")
    String accessKey;

    @Value("${aws.secret_access_key:#{null}}")
    String secretKey;

    @Value("${aws.s3.bucket_name:eagle}")
    String bucketName;

    @Value("${outputDir:eagle-files}")
    String outputDir;

    protected FileService createInstance() throws Exception {
        setSingleton(true);
        if(accessKey != null && secretKey != null)
            return new S3FileService(accessKey, secretKey, bucketName);

        logger.warn("AWS access key/secret key not found. Will use local storage instead");
        File dirFile = new File(outputDir);
        if(!dirFile.exists())
            dirFile.mkdirs();
        if(!dirFile.isDirectory())
            throw new RuntimeException("Unable to initialize FileService. outputDir invalid " + outputDir);
        if(!dirFile.exists())
            throw new RuntimeException("Unable to initialize FileService. Couldn't create outputDir " + outputDir);
        return new LocalFileService(dirFile.toPath());
    }

    @Override
    public Class<?> getObjectType() {
        return FileService.class;
    }
}
