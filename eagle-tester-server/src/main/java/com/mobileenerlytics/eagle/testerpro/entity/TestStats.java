package com.mobileenerlytics.eagle.testerpro.entity;


import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mobileenerlytics.eagle.testerpro.util.avging.AvgUpdateOps;
import com.mobileenerlytics.eagle.testerpro.util.db.MapToCollectionConverter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.*;

@Document(collection = "TestStats")
@CompoundIndexes({
    @CompoundIndex(name = "commit_testName", def = "{'commit.$id' : 1, 'testName': 1}", unique = true)
})
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter @Setter @NoArgsConstructor
public class TestStats {

    @Id
    @JsonProperty("_id")
    private String id;

    @JsonIgnore
    @Field("commit")
    @DBRef
    private Commit commit;

    @Field("energy")
    private double energy;

    @Field("energySquared")
    private double energySquared;

    @Field("power")
    private double power;

    @Field("powerSquared")
    private double powerSquared;

    @Field("testDurationMS")
    private long testDurationMS;

    @Field("testName")
    private String testName;

    @Field("updatedMs")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date updatedMs;

    @JsonIgnore
    @Field("testRecordSet")
    private Set<String> testRecordSet = new HashSet<>();

    @JsonSerialize(converter = MapToCollectionConverter.class)
    @Field("threadCompEnergies")
    private Map<String, ThreadCompEnergy> threadCompEnergies = new HashMap<>();

    @JsonSerialize(converter = MapToCollectionConverter.class)
    @Field("events")
    private Map<String, AggregateEvent> events = new HashMap<>();

    @JsonSerialize(converter = MapToCollectionConverter.class)
    @Field("componentUnit")
    private Map<String, NamedEnergy> componentUnit = new HashMap<>();

    @JsonSerialize(converter = MapToCollectionConverter.class)
    @Field("threadUnit")
    private Map<String, NamedEnergy> threadUnit = new HashMap<>();

    @Transient
    private static Logger logger = LoggerFactory.getLogger(TestStats.class.getSimpleName());

    @JsonAnyGetter
    Map<String, Object> get() {
        return new HashMap<String, Object>() {{
            put("count", testRecordSet.size());
        }};
    }

    @PersistenceConstructor
    public TestStats(Commit commit, String testName, Date updatedMs) {
        this.commit = commit;
        this.testName = testName;
        this.updatedMs = updatedMs;
    }

    private void updateFields(TestRecord testRecord, AvgUpdateOps ops) {
        long recordDurationMs = testRecord.getTestDurationMS();
        double recordEnergyuAh = testRecord.getEnergy();
        double recordPowermA = (3600 * recordEnergyuAh) / recordDurationMs;

        testDurationMS = ops.updateAvg(testDurationMS, recordDurationMs);

        energy = ops.updateAvg(energy, recordEnergyuAh);
        energySquared = ops.updateAvg(energySquared, recordEnergyuAh * recordEnergyuAh);

        power = ops.updateAvg(power, recordPowermA);
        powerSquared = ops.updateAvg(powerSquared, recordPowermA * recordPowermA);
    }

    private void updateTestRecord(TestRecord testRecord, AvgUpdateOps ops) {
        updateFields(testRecord, ops);

        if(updatedMs == null)
            // Only during first migration
            updatedMs = testRecord.getUpdatedMs();
        else
            updatedMs = new Date();

        ops.updateOldMap(threadCompEnergies, testRecord.getThreadCompEnergies());
        ops.updateOldMap(events, testRecord.getEvents());
        ops.updateOldMap(threadUnit, testRecord.getThreadUnit());
        ops.updateOldMap(componentUnit, testRecord.getComponentUnit());
    }

    public void addTestRecord(TestRecord testRecord) {
        if(testRecordSet.contains(testRecord.getId())) {
            logger.error("Skipping. Tried to add testRecord {} which is already in stats {}", testRecord, this);
            return;
        }

        updateTestRecord(testRecord, new AvgUpdateOps.AvgUpdateAdd(testRecordSet.size()));
        testRecordSet.add(testRecord.getId());
    }

    public void deleteTestRecord(TestRecord testRecord) {
        if(!testRecordSet.contains(testRecord.getId())) {
            logger.error("Skipping. Tried to remove testRecord {} which doesn't belong to me {}", testRecord, this);
            return;
        }

        int count = testRecordSet.size();
        if(count != 1)
            updateTestRecord(testRecord, new AvgUpdateOps.AvgUpdateSub(count));
        else {
            energy = 0;
            energySquared = 0;

            threadCompEnergies.clear();
            events.clear();

            threadUnit.clear();
            componentUnit.clear();
        }
        testRecordSet.remove(testRecord.getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestStats testStats = (TestStats) o;
        return id.equals(testStats.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
