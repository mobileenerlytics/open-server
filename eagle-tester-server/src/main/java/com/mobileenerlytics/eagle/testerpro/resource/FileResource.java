package com.mobileenerlytics.eagle.testerpro.resource;

import com.mobileenerlytics.eagle.testerpro.entity.TestRecord;
import com.mobileenerlytics.eagle.testerpro.service.TestRecordService;
import com.mobileenerlytics.eagle.testerpro.service.files.FileService;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;


// TODO changes to api/files/
@Controller
@Path("api/testrecords")
public class FileResource {
    private static Logger logger = LoggerFactory.getLogger(FileResource.class);

    @Context
    HttpHeaders httpHeaders;

    @Autowired
    private TestRecordService testRecordService;

    @Autowired
    FileService fileService;
    private static Tika tika = new Tika();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}/")
    public Response getTestRecord(
            @PathParam("id") String id,
            @QueryParam("projectId") String projectId
            ) {
        // auth by projectId
        Optional<TestRecord> optTestRecord = testRecordService.queryById(id);
        if(!optTestRecord.isPresent())
            throw new NotFoundException();
        TestRecord testRecord = optTestRecord.get();
        String projectIdTestrecord = testRecord.getProject().getId();
        if (projectId == null || projectIdTestrecord != null && !projectId.equals(projectIdTestrecord))
            throw new ForbiddenException();
        return Response.ok().entity(testRecord).build();
    }

    @GET
    @Path("/{id}/{path: .+}/")
    public Response getFiles(
            @PathParam("id") String id,
            @PathParam("path") String path,
            @QueryParam("projectId") String projectId,
            @HeaderParam("Range") String range) {
        // auth by projectId
        Optional<TestRecord> optTestRecord = testRecordService.queryById(id);
        if(!optTestRecord.isPresent())
            throw new NotFoundException();

        TestRecord testRecord = optTestRecord.get();
        if (projectId == null || testRecord.getProject() != null && !projectId.equals(testRecord.getProject().getId()))
            throw new ForbiddenException();

        // query from file system
        String uri = id + "/" + path;
        logger.info("Downloading an object, with the path:" + uri);

        try {
            String contentType = tika.detect(path);

            CacheControl control = new CacheControl();
            control.setMaxAge(60*10);   // 10 minutes

            return fileService.getFiles(uri, range)
                    .cacheControl(control)
                    .header(HttpHeaders.CONTENT_TYPE, contentType).build();
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
            throw new NotFoundException();
        } catch (IOException e) {
            throw new ServerErrorException(INTERNAL_SERVER_ERROR);
        }
    }
}
