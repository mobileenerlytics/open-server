package com.mobileenerlytics.eagle.testerpro.exceptions;

public class EagleException extends Exception {
    public EagleException(String name) {
        super(name);
    }
}
