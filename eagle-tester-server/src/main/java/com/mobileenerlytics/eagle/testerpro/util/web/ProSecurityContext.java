package com.mobileenerlytics.eagle.testerpro.util.web;

import org.springframework.beans.factory.annotation.Value;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

public class ProSecurityContext implements SecurityContext {
    @Value("${proUsername:#{null}}")
    String proUsername;

    SecurityContext oldContext;
    public ProSecurityContext(SecurityContext oldContext) {
        this.oldContext = oldContext;
    }

    @Override
    public Principal getUserPrincipal() {
        return () -> proUsername;
    }

    @Override
    public boolean isUserInRole(String s) {
        // pro user has default role
        if(s.equals("default"))
            return true;
        return oldContext.isUserInRole(s);
    }

    @Override
    public boolean isSecure() {
        return oldContext.isSecure();
    }

    @Override
    public String getAuthenticationScheme() {
        return oldContext.getAuthenticationScheme();
    }

}
