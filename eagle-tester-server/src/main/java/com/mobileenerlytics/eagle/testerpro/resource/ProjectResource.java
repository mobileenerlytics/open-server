package com.mobileenerlytics.eagle.testerpro.resource;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.entity.Widget;
import com.mobileenerlytics.eagle.testerpro.exceptions.EagleException;
import com.mobileenerlytics.eagle.testerpro.service.BranchService;
import com.mobileenerlytics.eagle.testerpro.service.ProjectService;
import com.mobileenerlytics.eagle.testerpro.util.web.HeaderUtils;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Controller
@Path("api/projects")
@Api(value = "Projects API", position = 0, produces = MediaType.APPLICATION_JSON, description = "API to get the projects list or the details of a particular project or update a project corresponding to a user")
public class ProjectResource {
    private static Logger logger = LoggerFactory.getLogger(ProjectResource.class);
    private static final String MSG_PROJECT_ID_NOT_FOUND = "Project of id:projectId is not found";
    private static final String MSG_PROJECT_USER_NOT_FOUND = "Projects of user:userName is not found";
    private static final String MSG_SUCCESSFUL = "successful operation";
    private static final String MSG_BAD_REQUEST = "Bad Request";
    private static final String MSG_NO_CONTENT = "No Content";

    @Autowired
    private ProjectService projectService;

    @Autowired
    private HeaderUtils headerUtils;

    @Context
    HttpHeaders httpHeaders;

    @Autowired
    private BranchService branchService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value="Find the list of projects corresponding to a user", nickname = "getProjects", response = Project.class, responseContainer = "List",
            notes="curl -u demo:password https://tester.mobileenerlytics.com/api/projects",
            position = 0
    )
    @ApiResponses(value = {
            @ApiResponse(code=404, message=MSG_PROJECT_USER_NOT_FOUND),
            @ApiResponse(code=200, message=MSG_SUCCESSFUL),
    } )
    public Response getProjects() {
        String userName = headerUtils.getUsernameFromHeader(httpHeaders);
        List<Project> documents = projectService.queryProjectByUsername(userName);

        if (documents == null)
            throw new NotFoundException("Projects of user: " + userName + "is not found");

        return Response.status(Response.Status.OK).entity(documents).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}/")
    @ApiOperation(value="Get the details of a particular project", nickname = "getProject", response = Project.class, responseContainer = "List",
            notes="curl -u demo:password https://tester.mobileenerlytics.com/api/projects/abd3d359-d2b8-4454-bdde-b08fcd21b834"
    )
    @ApiResponses(value = {
            @ApiResponse(code=404, message=MSG_PROJECT_ID_NOT_FOUND),
            @ApiResponse(code=200, message=MSG_SUCCESSFUL),
    } )
    public Project getProject(@ApiParam(value="Id of the project", required=true)@PathParam("id") String id) {

        Project project = projectService.queryProjectById(id);
        return project;
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{projectId}/{attribute}")
    @RolesAllowed("default")
    @ApiOperation(value="Update a project based on its Id", nickname = "updateProject",
            notes="curl -u demo:password https://tester.mobileenerlytics.com/api/projects/abd3d359-d2b8-4454-bdde-b08fcd21b834/name/P1"
    )
    @ApiResponses(value = {
            @ApiResponse(code=400, message=MSG_BAD_REQUEST),
            @ApiResponse(code=204, message=MSG_NO_CONTENT),
            @ApiResponse(code=200, message =MSG_SUCCESSFUL)
    } )
    public Response updateProject(
            @ApiParam(value="Id of the project", required=true)@PathParam("projectId") String projectId,
            @ApiParam(value="Attribute name that is required to be updated", required=true)@PathParam("attribute") String attribute,
            @ApiParam(value="Value of the  of the attribute", required=true)@QueryParam("value") String value)  {
        boolean oneAndUpdate = projectService.updateProjectAttribute(projectId, attribute, value);
        if (!oneAndUpdate)
            throw new BadRequestException("project update failed");

        return Response.ok().build();
    }

    @PUT
    @Path("{projectId}/widget/{index}")
    @RolesAllowed("default")
    @ApiOperation(value="Update a project's widget preferences based on its id", nickname = "updateWidget")
    @ApiResponses(value = {
            @ApiResponse(code=400, message=MSG_BAD_REQUEST),
            @ApiResponse(code=204, message=MSG_NO_CONTENT),
            @ApiResponse(code=200, message =MSG_SUCCESSFUL)
    } )
    public Response updateWidget(
            @ApiParam(value="Id of the project", required=true)
            @PathParam("projectId") String projectId,

            @ApiParam(value="Zero indexed index of widget preference that needs update", required=true)
            @PathParam("index") int index,

            @ApiParam(value="Comparison type", allowableValues= "branch, test, component, thread",required=true)
            @QueryParam("type") String comparisonType,

            @ApiParam(value="Comparison metric", allowableValues= "energy, power",required=true)
            @QueryParam("metric") String metric,

            @ApiParam(value="First branch for comparison", required=true)
            @QueryParam("firstBranch") String firstBranchName,

            @ApiParam(value="First commit hash for comparison. null means just compare the latest commit of the first branch")
            @QueryParam("firstCommit") String firstCommitHash,

            @ApiParam(value="Second branch for comparison", required=true)
            @QueryParam("secondBranch") String secondBranchName,

            @ApiParam(value="Second commit hash for comparison. null means just compare the latest commit of the second branch")
            @QueryParam("secondCommit") String secondCommitHash) throws EagleException {

        Optional<Project> optionalProject = projectService.findById(projectId);
        if(!optionalProject.isPresent())
            throw new NotFoundException();

        Project project = optionalProject.get();
        Optional<Branch> optionalFirstBranch = branchService.queryOneByNameAndProject(firstBranchName, project);
        if(!optionalFirstBranch.isPresent())
            throw new NotFoundException();

        Optional<Branch> optionalSecondBranch = branchService.queryOneByNameAndProject(secondBranchName, project);
        if(!optionalSecondBranch.isPresent())
            throw new NotFoundException();

        Widget widget = new Widget(comparisonType, metric, firstBranchName, firstCommitHash, secondBranchName, secondCommitHash);
        List<Widget> widgets = project.getWidgets();
        if(widgets == null)
            widgets = projectService.getDefaultWidgets(project);
        widgets.set(index, widget);
        project = projectService.setWidgets(project, widgets);
        return Response.ok().build();
    }
}
