package com.mobileenerlytics.eagle.testerpro.resource;

import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import com.mobileenerlytics.eagle.testerpro.entity.TestRecord;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import com.mobileenerlytics.eagle.testerpro.service.CommitService;
import com.mobileenerlytics.eagle.testerpro.service.TestRecordService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Controller
@Path("/api/records")
public class TestRecordResource {

    @Autowired
    private TestRecordService testRecordService;

    @Autowired
    private CommitService commitService;

    private static final String MSG_INCOMPLETE = "Commit_Id and Test Name are both required";
    private static final String MSG_RECORDS_NOT_FOUND = "Test Records not found";
    private static final String MSG_TESTRECORD_DELETE_FAILED = "Test Record deletion failed";
    private static final String MSG_TESTRECORD_DELETE_SUCCESS = "Test Record successfully deleted";

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Find test record details by commitId and testName", nickname = "queryTestRecords", response = TestRecord.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code=403, message=MSG_INCOMPLETE),
            @ApiResponse(code = 404, message = MSG_RECORDS_NOT_FOUND)
    } )
    public List<TestRecord> getTestRecords(
            @ApiParam(value="Commit_id of test record",required=true)@QueryParam("commitId") String commitId,
            @ApiParam(value="Name of the test whos records to return",required=true)@QueryParam("testName") String testName,
            @ApiParam(value="Number of records to be returned",required=true)@QueryParam("count") int count) {

        if(commitId == null || testName == null || count <= 0)
            throw new ForbiddenException(MSG_INCOMPLETE);


        Commit commit = commitService.queryById(commitId);

        if(commit == null)
            throw new NotFoundException(MSG_RECORDS_NOT_FOUND);

        return testRecordService.queryByCommitAndTestName(commit, testName, count);
    }

    @DELETE
    @Path("/{projectId}/")
    @RolesAllowed({"default", "manual"})
    @ApiOperation(value = "Delete test records by passing the test record id", nickname = "deleteTestRecords")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiResponses(value = {
            @ApiResponse(code=500, message=MSG_TESTRECORD_DELETE_FAILED),
            @ApiResponse(code=404, message=MSG_RECORDS_NOT_FOUND),
            @ApiResponse(code=200, message=MSG_TESTRECORD_DELETE_SUCCESS)
    } )
    public Response deleteTestRecords(
            @ApiParam(value = "id of the test record", required = true)@QueryParam("testRecordId") String testRecordId,
            @ApiParam(value = "Id of the project corresponding to the user", required = true) @PathParam("projectId") String projectId) {

        if ( testRecordId == null || testRecordId.isEmpty())
            throw new ForbiddenException();

        Optional<TestRecord> optTestRecord = testRecordService.queryById(testRecordId);
        if(!optTestRecord.isPresent())
            throw new NotFoundException();
        TestRecord testRecord = optTestRecord.get();
        if (projectId == null || testRecord.getProject() != null && !projectId.equals(testRecord.getProject().getId()))
            throw new ForbiddenException();

        try {
            boolean oneAndDelete = testRecordService.queryOneAndDelete(testRecordId);

            if (!oneAndDelete)
                throw new NotFoundException(MSG_RECORDS_NOT_FOUND);

            return Response.ok().entity(MSG_TESTRECORD_DELETE_SUCCESS).build();
        } catch(DeletedException de) {
            // Test record got deleted in another API
            return Response.ok().entity(MSG_TESTRECORD_DELETE_SUCCESS).build();
        }

    }
}
