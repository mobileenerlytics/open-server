package com.mobileenerlytics.eagle.testerpro.exceptions;

public class TransactionException extends EagleException {
    TransactionException(String name) {
        super(name);
    }
}
