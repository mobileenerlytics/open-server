package com.mobileenerlytics.eagle.testerpro.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Service
public class CRMService {
    @Value("${crm.apiKey}")
    String apiKey;

    private static Logger logger = LoggerFactory.getLogger(CRMService.class);
    public enum CRMLifecyleEvent {
        SIGNUP ("Signed up"),
        FREE_TRIAL ("Free Trial"),
        PAID ("Paid"),
        OTHER ("Other");

        final String crmInternalVal;
        CRMLifecyleEvent(String value) {
            crmInternalVal = value;
        }
    }

    private static final String VAL_CUSTOMER = "customer";
    private static final String VAL_OPPORTUNITY = "opportunity";

    public static final String PROP_LIFECYCLE = "lifecyclestage";
    public static final String PROP_TESTER_LIFECYCLE = "tester_lifecycle";
    public static final String PROP_FIRSTNAME = "firstname";
    public static final String PROP_LASTNAME = "lastname";
    public static final String PROP_EMAIL = "email";
    public static final String PROP_COMPANY = "company";
    public static final String PROP_ROLE = "default";

    public void logLifecycleEvent(String userEmail, CRMLifecyleEvent event) {
        Map<String, String> map = new HashMap<String, String>();
        if(event == CRMLifecyleEvent.PAID)
            map.put(PROP_LIFECYCLE, VAL_CUSTOMER);
        if(event == CRMLifecyleEvent.FREE_TRIAL)
            map.put(PROP_LIFECYCLE, VAL_OPPORTUNITY);
        map.put(PROP_TESTER_LIFECYCLE, event.crmInternalVal);
        logPropertyUpdate(userEmail, map);
    }

    public void logPropertyUpdate(String userEmail, Map<String, String> properties) {
        if(apiKey == null || apiKey.length() == 0) {
            logger.warn("CRM is not initialized. Skipping");
            return;
        }
        String uri = String.format("https://api.hubapi.com/contacts/v1/contact/createOrUpdate/email/%s/?hapikey=%s",
                userEmail, apiKey);
        Client client = ClientBuilder.newClient();

        StringBuilder input = new StringBuilder("{\n" +
                "  \"properties\": [");
        for(Map.Entry<String, String> property: properties.entrySet()) {
            input.append("{");
            input.append("  \"property\": \"" + property.getKey() + "\",");
            input.append("  \"value\": \"" + property.getValue() + "\"");
            input.append("},");
        }
        input.append("]\n}");
        String postInput = input.toString();
        postInput = postInput.replace("},]", "}]");

        // POST method
        Response response = client.target(uri).request("application/json").post(Entity.json(postInput));

        // check response status code
        if (response.getStatus() != 200) {
            logger.error("Failed : HTTP error code : " + response.getStatus());
        }
    }

}
