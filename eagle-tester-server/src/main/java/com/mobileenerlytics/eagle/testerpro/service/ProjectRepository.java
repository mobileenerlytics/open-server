package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Project;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends MongoRepository<Project, String> {

    @Query("{'name' : ?0, 'userId' : ?1}")
    Project findByNameAndUserId(String name, String userId);

    @Query("{'userId' : ?0}")
    List<Project> findAllByUserId(String userId);
}
