package com.mobileenerlytics.eagle.testerpro.resource;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import com.mobileenerlytics.eagle.testerpro.service.BranchService;
import com.mobileenerlytics.eagle.testerpro.service.CommitService;
import com.mobileenerlytics.eagle.testerpro.service.ProjectService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
@Path("api/commits")
@Api(value = "Commits API", position = 1, produces = MediaType.APPLICATION_JSON, description = "API to get the commits list or delete a commit corresponding to a branch for a particular user")
public class CommitResource {
    private static Logger logger = LoggerFactory.getLogger(CommitResource.class);
    private static final String MSG_FORBIDDEN = "projectId is required";
    private static final String MSG_COMMIT_DELETE_FAILED = "Commit delete failed!!!";
    private static final String MSG_COMMIT_ID_NOT_FOUND = "Commit Id not found";
    private static final String MSG_COMMIT_DELETE_SUCCESS = "Commit deleted successfully";
    @Context SecurityContext context;

    @Autowired
    private CommitService commitService;

    @Autowired
    private BranchService branchService;

    @Autowired
    private ProjectService projectService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value="Find the commits by projectId, count, middleUpdateMS and branchName", nickname = "getCommits", response = Commit.class, responseContainer = "List",
            notes="curl -u demo:password 'https://tester.mobileenerlytics.com/api/commits?projectId=abd3d359-d2b8-4454-bdde-b08fcd21b834&count=1&branchName=origin/master&count=1'"
    )
    @ApiResponses(value = {
            @ApiResponse(code=403, message=MSG_FORBIDDEN),
    } )
    public List<Commit> getCommits(
            @ApiParam(value="Number of commits to be returned.", required=true)
            @QueryParam("count") int count,
            @ApiParam(value="Name of the branch for which the commits list is required", required=false)
            @QueryParam("branchName") String branchName,
            @ApiParam(value="Commit hash of middle commit (Commits before and after this commit are returned)",required=false)
            @QueryParam("middleCommit") String middleHash,
            @ApiParam(value="Prefix of commit hashes")
            @QueryParam("hashPrefix") String hashPrefix,
            @ApiParam(value="Id of the project corresponding to the user", required=true)
            @QueryParam("projectId") String projectId) {
        if (projectId == null) throw new BadRequestException("projectId is required");

        if(count < 1) count = 1;

        Project project = projectService.queryProjectById(projectId);

        if(hashPrefix != null) {
            if (middleHash != null)
                throw new BadRequestException("middleHash and hashPrefix can not be sent together");

            Branch branch = getBranch(branchName, project);
            return commitService.queryByPrefix(project, branch, hashPrefix, count);
        }

        if(middleHash != null) {
            List<Commit> commits = new ArrayList<>();
            Branch branch = getBranch(branchName, project);

            Optional<Commit> optMiddleCommit = commitService.findByHashAndBranch(middleHash, branch);
            if(!optMiddleCommit.isPresent())
                throw new NotFoundException("No such commit " + middleHash);

            Commit middleCommit = optMiddleCommit.get();
            Date middleUpdatedMs = middleCommit.getUpdatedMs();
            Sort sort = Sort.by(Sort.Direction.ASC, "updatedMs");

            // greater than middleMs
            commits.addAll(commitService.queryCommitsWithMiddleHash(project, branchName, count/2, sort, middleUpdatedMs,false));

            // less than or equal to middleMs (default sort in getCommits is same as sort operation for this half)
            commits.addAll(commitService.queryCommitsWithMiddleHash(project, branchName, count/2, null, middleUpdatedMs, true));

            return commits;
        }

        return commitService.queryByProjectAndBranchName(project, branchName, count);
    }

    private Branch getBranch(String branchName, Project project) {
        if(branchName == null || branchName.length() == 0)
            throw new BadRequestException("branchName is required when supplying a middleHash");
        Optional<Branch> optBranch = branchService.queryOneByNameAndProject(branchName, project);
        if(!optBranch.isPresent())
            throw new NotFoundException("No such branch " + branchName);
        return optBranch.get();
    }

    @DELETE
    @Path("/{id}/")
    @RolesAllowed("default")
    @ApiOperation(value="Delete the commits by passing the commitId and the projectId", nickname = "deleteCommitById",
            notes="curl -u demo:password 'https://tester.mobileenerlytics.com/api/commits/8aea66ce-5f2c-4f9a-a282-e59a05660c53?projectId=abd3d359-d2b8-4454-bdde-b08fcd21b834'"
    )
    @ApiResponses(value = {
            @ApiResponse(code=500, message=MSG_COMMIT_DELETE_FAILED),
            @ApiResponse(code=404, message=MSG_COMMIT_ID_NOT_FOUND),
            @ApiResponse(code=200, message=MSG_COMMIT_DELETE_SUCCESS)
    } )
    @Produces(MediaType.APPLICATION_JSON)
    /*
    url : localhost:38673/api/commits/b671ea46-7977-4060-be8c-2c6f789ec8ff/?projectId=473f9089-dc33-40b4-9ad5-6a5a8f9ef833
     */
    public Response deleteCommitById(@PathParam("id") String id
            , @QueryParam("projectId") String projectId) throws DeletedException {
        if (projectId == null) throw new BadRequestException("projectId is required");

        boolean oneAndDelete = commitService.queryOneAndDelete(id);

        if(!oneAndDelete)
            throw new NotFoundException("Commit of id: " + id + " was not found");

        return Response.ok().entity(MSG_COMMIT_DELETE_SUCCESS).build();

    }
}
