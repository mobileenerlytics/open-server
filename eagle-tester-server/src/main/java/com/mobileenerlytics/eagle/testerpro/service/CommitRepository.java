package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import com.mobileenerlytics.eagle.testerpro.entity.Project;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface CommitRepository extends MongoRepository<Commit, String> {
    Optional<Commit> findByHashAndBranch(String hash, Branch branch);

    List<Commit> findAllByBranch(Branch branch);

    @Query(fields = "{'project': 0, 'branch' : 0, 'testRecords' : 0}")
    List<Commit> queryAllByProjectAndBranchName(Project project, String branchName, Pageable pageable);

    @Query(fields = "{'project': 0, 'branch' : 0, 'testRecords' : 0}")
    List<Commit> queryAllByProject(Project project, Pageable pageable);

    @Query(value = "{'updatedMs': {$lte:?0 }}", fields = "{'project': 0, 'branch' : 0, 'testRecords' : 0}")
    List<Commit> queryAllByProjectAndBranchNameUpdatedMsBefore(Date middleMs, Project project, String branchName, Pageable pageable);

    @Query(value = "{'updatedMs': {$gt:?0 }}", fields = "{'project': 0, 'branch' : 0, 'testRecords' : 0}")
    List<Commit> queryAllByProjectAndBranchNameUpdatedMsAfter(Date middleMs, Project project, String branchName, Pageable pageable);

    @Query(sort = "{'updatedMs' : -1}", fields = "{'project': 0, 'branch' : 0, 'testRecords' : 0}")
    List<Commit> queryAllByProjectAndBranchAndHashLike(Project project, Branch branch, String hash, Pageable pageable);
}
