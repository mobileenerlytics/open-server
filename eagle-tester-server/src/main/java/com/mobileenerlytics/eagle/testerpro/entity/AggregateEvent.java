package com.mobileenerlytics.eagle.testerpro.entity;

import com.google.common.base.Verify;
import com.mobileenerlytics.eagle.testerpro.util.avging.IAvgEntry;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.Nullable;

import java.util.*;

import static java.util.stream.Collectors.groupingBy;

public class AggregateEvent implements IAvgEntry<AggregateEvent> {
    public String processName;
    public String eventName;
    public String name;
    public double durationSec;
    public double durationSecSquared;

    static Optional<AggregateEvent> aggregateEventJsons(String processName, String eventName, List<JsonEvent> jsonEvents) {
        return jsonEvents.stream()
                .filter(e -> e.endTimeMs - e.startTimeMs >= 1000)    // At least 1 second of event
                .map(e -> {
                    long durationSec = (e.endTimeMs - e.startTimeMs)/1000;
                    return new AggregateEvent(processName, eventName, e.name, durationSec, durationSec*durationSec);
                })
                .reduce((e1, e2) -> {
                    Verify.verify(e1.name.equals(e2.name));
                    e1.durationSec += e2.durationSec;
                    return e1;
                });
    }


    @PersistenceConstructor
    public AggregateEvent(@Nullable String processName, String eventName, String name,
                          double durationSec, double durationSecSquared) {
        this.processName = processName;
        this.eventName = eventName;
        this.name = name;
        this.durationSec = durationSec;
        this.durationSecSquared = durationSecSquared;
    }

    @Override
    public String getKey() {
        return processName + "--" + eventName + "--" + name;
    }

    @Override
    public double[] getValues() {
        return new double[] {durationSec, durationSecSquared};
    }

    @Override
    public void setValues(double... value) {
        this.durationSec = value[0];
        this.durationSecSquared = value[1];
    }

    @Override
    public AggregateEvent copyConstruct(double... value) {
        return new AggregateEvent(processName, eventName, name, value[0], value[1]);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AggregateEvent event = (AggregateEvent) o;
        return Objects.equals(processName, event.processName) &&
                Objects.equals(eventName, event.eventName) &&
                Objects.equals(name, event.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eventName, name, processName);
    }

    public class JsonEvent {
        String name;
        long startTimeMs, endTimeMs;
    }

    public static List<AggregateEvent> aggregate(String processName, Map<String, List<JsonEvent>> jsonMap) {
        List<AggregateEvent> aggregateEvents = new LinkedList<>();
        for(String eventName : jsonMap.keySet()) {
            Map<String, List<JsonEvent>> eventsByName = jsonMap.get(eventName).stream().collect(groupingBy(ej -> ej.name));
            for(List<JsonEvent> jsonEvents: eventsByName.values()) {
                Optional <AggregateEvent> optionalAggregateEvent = aggregateEventJsons(processName, eventName, jsonEvents);
                if(optionalAggregateEvent.isPresent())
                    aggregateEvents.add(optionalAggregateEvent.get());
            }
        }
        return aggregateEvents;
    }
}
