package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.entity.Upload;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UploadRepository extends MongoRepository<Upload, String> {

    @Query(fields = "{'project' : 0, 'testRecords' : 0}", sort="{'uploadMs': -1}")
    List<Upload> queryByProject(Project project, Pageable pageable);
}
