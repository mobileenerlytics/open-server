package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.*;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.BadRequestException;
import java.util.*;

@Slf4j
@Service
public class CommitService {

    @Autowired
    private CommitRepository repo;

    @Autowired
    private ContributorService contributorService;

    @Autowired
    private TestRecordService testRecordService;

    public List<Commit> findAllByBranch(Branch branch) {
        return repo.findAllByBranch(branch);
    }

    public List<Commit> queryByProjectAndBranchName(@NonNull Project project, String branchName, int count) {
        Pageable pageable = PageRequest.of(0, count, Sort.Direction.DESC, "updatedMs");

        if(branchName == null || branchName.length() == 0)
            return repo.queryAllByProject(project, pageable);
        return repo.queryAllByProjectAndBranchName(project, branchName, pageable);
    }

    public List<Commit> queryCommitsWithMiddleHash(@NonNull Project project, String branchName, int count, Sort sort, Date middleMs, Boolean isBefore) {
        List<Commit> commits = new ArrayList<>();
        if(sort == null) {
            sort = Sort.by(Sort.Direction.DESC, "updatedMs");
        }
        Pageable pageable = PageRequest.of(0, count, sort);

        if(middleMs != null) {
            if(isBefore) {
                commits = repo.queryAllByProjectAndBranchNameUpdatedMsBefore(middleMs, project, branchName, pageable);
            } else {
                commits = repo.queryAllByProjectAndBranchNameUpdatedMsAfter(middleMs, project, branchName, pageable);
            }
        }

        return commits;
    }

    @Transactional
    public boolean queryOneAndDelete(String id) throws DeletedException {
        Optional<Commit> opt = repo.findById(id);

        if(!opt.isPresent()) {
            return false;
        }
        delete(opt.get());
        return true;
    }

    @Transactional
    void delete(Commit commit) throws DeletedException {
        for(TestRecord testRecord: testRecordService.findAllByCommit(commit))
            testRecordService.delete(testRecord);
    }

    public Commit queryById(String id) {
        Optional<Commit> commit = repo.findById(id);
        if(!commit.isPresent()) {
            throw new BadRequestException("No commit found for specified commitId");
        }
        return commit.get();
    }

    public Optional<Commit> findByHashAndBranch(String hash, Branch branch) {
        return repo.findByHashAndBranch(hash, branch);
    }

    public List<Commit> queryByPrefix(Project project, Branch branch, String hashPrefix, int count) {
        Pageable pageable = PageRequest.of(0, count);
        return repo.queryAllByProjectAndBranchAndHashLike(project, branch, hashPrefix + "*", pageable);
    }

    @Transactional
    Commit findOneOrCreate(String hash, Branch branch, Contributor contributor, Date updatedMs) {
        Optional<Commit> optCommit = findByHashAndBranch(hash, branch);
        if(optCommit.isPresent()) {
            Commit commit = optCommit.get();
            commit.setUpdatedMs(updatedMs);
            return repo.save(commit);
        }

        log.info("Creating new commit with hash {} on branch {}-{}", hash, branch.getId(), branch.getBranchName());
        Commit commit = new Commit(hash, branch, contributor, updatedMs);
        commit = repo.save(commit);
        contributorService.addCommit(commit);
        return commit;
    }

    @Transactional
    Commit addTestStats(TestStats testStats) throws DeletedException {
        Commit commit = testStats.getCommit();
        // refresh commit, as this commit might be stale
        Optional<Commit> optCommit = repo.findById(commit.getId());
        if(!optCommit.isPresent())
            throw new DeletedException(Commit.class);
        commit = optCommit.get();
        commit.getTestStats().add(testStats);
        return repo.save(commit);
    }

    @Transactional
    void deleteTestStats(TestStats testStats) throws DeletedException {
        Commit commit = testStats.getCommit();
        Optional<Commit> optCommit = repo.findById(commit.getId());
        if(!optCommit.isPresent())
            throw new DeletedException(Commit.class);
        commit.getTestStats().remove(testStats);
        if(commit.getTestStats().isEmpty()) {
            contributorService.rmCommit(commit);
            repo.delete(commit);
            return;
        }
        repo.save(commit);
    }
}
