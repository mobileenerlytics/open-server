package com.mobileenerlytics.eagle.testerpro.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.text.DecimalFormat;
import java.util.*;

import static java.util.stream.Collectors.*;

/*
      {
        "testId": "test1",
        "energyCost": 11,
        "components": {
          "gpu": 1,
          "screen": 10
        }
      },

 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "TestRecord")
@Getter @Setter
public class TestRecord {
    @JsonProperty(value = "_id")
    @Id
    private String id;

    @JsonIgnore
    @Field("commit")
    @DBRef
    private Commit commit;

    @JsonIgnore
    @Field("branch")
    @DBRef
    private Branch branch;

    @Field("testName")
    String testName;

    @Field("energy")
    private double energy;

    @JsonIgnore
    @Field("project")
    @DBRef
    private Project project;

    @ApiModelProperty(hidden = true)
    @Field("updatedMs")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date updatedMs;

    @Field("threadCompEnergies")
    private Set<ThreadCompEnergy> threadCompEnergies = new HashSet<>();

    @Field("componentUnit")
    private Set<NamedEnergy> componentUnit = new HashSet<>();

    @Field("threadUnit")
    private Set<NamedEnergy> threadUnit = new HashSet<>();

//    @OneToMany
// {wake_lock_in -> (LocationManagerService, 448), ...}, ...
    @Field("events")
    private Set<AggregateEvent> events = new HashSet<>();

    private String cloudsharkLink;

    @Field("testDurationMS")
    private long testDurationMS;

    @PersistenceConstructor
    public TestRecord() {
    }

    public TestRecord(String testName, Commit commit, Branch branch, Project project,
                      long durationMS, Set<ThreadCompEnergy> threadCompEnergies, Set<AggregateEvent> events) {
        this.testName = testName;
        this.updatedMs = new Date();
        this.commit = commit;
        this.branch = branch;
        this.project = project;
        this.testDurationMS = durationMS;

        double e = threadCompEnergies.parallelStream().mapToDouble(t -> t.energy).sum();
        DecimalFormat df2 = new DecimalFormat(".##");
        this.energy = Double.parseDouble(df2.format(e));

        Map<String, List<ThreadCompEnergy>> processes = threadCompEnergies.parallelStream().collect(groupingBy(t -> t.processName));
        for(Map.Entry<String, List<ThreadCompEnergy>> entry : processes.entrySet()) {
            // com.pandora.android -> {CPU : 100}
            final String process = entry.getKey();
            Map<String, Double> cu = entry.getValue().parallelStream()
                            .collect(groupingBy(t -> t.componentId, summingDouble(t -> t.energy)));
            componentUnit.addAll(cu.entrySet().parallelStream()
                    .map(u -> new NamedEnergy(process, u.getKey(), u.getValue(), testDurationMS))
                    .collect(toSet()));

            // com.pandora.android -> {RenderThread : 100}
            Map<String, Double> tu = entry.getValue().parallelStream()
                            .collect(groupingBy(t -> t.threadId, summingDouble(t -> t.energy)));
            threadUnit.addAll(tu.entrySet().parallelStream()
                    .map(u -> new NamedEnergy(process, u.getKey(), u.getValue(), testDurationMS))
                    .collect(toSet()));
        }

        this.threadCompEnergies.addAll(threadCompEnergies);
        this.events.addAll(events);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestRecord that = (TestRecord) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
