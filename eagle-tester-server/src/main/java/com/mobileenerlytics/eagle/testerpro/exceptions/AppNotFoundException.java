package com.mobileenerlytics.eagle.testerpro.exceptions;

public class AppNotFoundException extends EagleException {
    public AppNotFoundException() {
        super("AppNotFoundException");
    }
}
