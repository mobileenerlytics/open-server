package com.mobileenerlytics.eagle.testerpro.util.web;

import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URL;

@Path("")
public class StaticResource {
    private static Logger logger = LoggerFactory.getLogger(StaticResource.class);
    public static Class otherResolvers;
    private static Tika tika = new Tika();
    @GET
    @Path("{path:.*}")
    public Response Get(@PathParam("path") String path) throws IOException {
        if(path == null || path.length() == 0) {
            path = "index.html";
        }
        path = "/" + path;
        logger.debug("Resolving file {}", path);

        if(path.startsWith("/private/") || path.startsWith("/api/") || path.startsWith("/error"))
            return Response.status(Response.Status.NOT_FOUND).build();
        URL resUrl = getResourceURL(path);

        if(resUrl == null) {
            logger.warn("Couldn't resolve {}", path);
            resUrl = getResourceURL("/index.html");
//            return Response.status(Response.Status.NOT_FOUND).build();
        }
        String contentType = tika.detect(resUrl);
        return Response.ok(resUrl.openStream()).header(HttpHeaders.CONTENT_TYPE, contentType).build();
    }

    URL getResourceURL(String path) {
        // Warning! DO not use getClass().getClassLoader().getResourceURL(path)
        // since when we package as a jar file, the getClassLoader() returns
        // system class loader which is apparently unaware of resources in
        // the jar file and hence can't resolve the resource. Using
        // Classname.class.getResourceURL is able to resolve to the
        // resources!
        URL resUrl = StaticResource.class.getResource(path);
        if(resUrl == null && otherResolvers != null) {
            resUrl = otherResolvers.getResource(path);
        }
        return resUrl;
    }
}
