package com.mobileenerlytics.eagle.testerpro.config;

import com.mobileenerlytics.eagle.testerpro.util.web.HeaderUtils;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!saas")
public class ServerConfig {
    @Bean
    public ResourceConfig getResourceConfig() {
        return new ServerJerseyConfig();
    }

    @Bean
    public HeaderUtils getUtil() {
        return new HeaderUtils();
    }
}
