package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import com.mobileenerlytics.eagle.testerpro.entity.TestRecord;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestRecordRepository extends MongoRepository<TestRecord, String> {
    List<TestRecord> findAllByCommit(Commit commit);

    @Query(fields = "{'project' : 0, 'branch' : 0, 'commit' : 0}")
    List<TestRecord> queryAllByTestNameAndCommit(String testName, Commit commit, Pageable pageable);
}
