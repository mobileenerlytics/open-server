package com.mobileenerlytics.eagle.testerpro.config;

import com.mobileenerlytics.eagle.testerpro.util.ContextFilter;
import com.mobileenerlytics.eagle.testerpro.util.web.CORSResponseFilter;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

public class ServerJerseyConfig extends ResourceConfig {

    public ServerJerseyConfig() {
        register(MultiPartFeature.class);
//        register(DebugMapper.class);
        register(CORSResponseFilter.class);
        register(LoggingFeature.class);
        register(ContextFilter.class);
        register(RolesAllowedDynamicFeature.class);
        property(ServerProperties.RESPONSE_SET_STATUS_OVER_SEND_ERROR, true);
        packages("com.mobileenerlytics.eagle.testerpro");
    }
}
