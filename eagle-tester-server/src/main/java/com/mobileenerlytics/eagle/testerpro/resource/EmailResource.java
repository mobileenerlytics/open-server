package com.mobileenerlytics.eagle.testerpro.resource;
//

import com.mobileenerlytics.eagle.testerpro.service.EmailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.security.RolesAllowed;
import javax.mail.MessagingException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Controller
@Path("api/email")
@Api(value = "Email API", position = 0, produces = MediaType.APPLICATION_JSON, description = "API to send email")
public class EmailResource {
    private static final String MSG_SUCCESSFUL = "Email sent";

    @Autowired
    private EmailService emailService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("default")
    @ApiOperation(httpMethod = "POST", value="", nickname = "postEmail")
    @ApiResponses(value = { @ApiResponse(code=200, message =MSG_SUCCESSFUL) } )
    public Response sendEmail(
//            @FormDataParam("sender") String sender,
            @FormDataParam("recipients") String recipients,
            @FormDataParam("content") String content,
            @FormDataParam("subject") String subject) throws MessagingException {
        emailService.sendEmail(recipients, content, subject);
        return Response.status(200).entity(MSG_SUCCESSFUL).build();
    }

}