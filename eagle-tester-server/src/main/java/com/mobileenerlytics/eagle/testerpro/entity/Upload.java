package com.mobileenerlytics.eagle.testerpro.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mobileenerlytics.eagle.testerpro.service.UploadService;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * This describes an upload API call made by the user. This is used to show the recent activity
 * portion in frontend.
 *
 * This entity shall thus be kept pretty close to what the user sent instead of keeping a bunch of {@link DBRef}.
 * This is because when we're reprocessing an {@link Upload}, {@link Branch}, {@link Commit} may no longer exist
 * in the db. Branch, Commit etc shall get created using the information available in the upload.
 */
@Document(collection = "Upload")
@ApiModel(value="Upload Model", description = "Model for the uploads API")
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter @Setter
@CompoundIndex(name = "project_uploadMs", def = "{'project.$id' : 1, 'uploadMs': -1}")
public class Upload {

    @Id
    @JsonProperty("_id")
    private String id;

    @JsonIgnore
    @DBRef
    private Project project;

    private String branchName;

    private String hash;

    private Date uploadMs;

    private String pkgName;

    private String authorEmail;

    private String authorName;

    private String jobStatus;

    private Long jobDurationMs;

    @JsonIgnore
    private String zipLocation;

    /**
     * We make an exception here to directly hold {@link DBRef} to {@link TestRecord} since they need to be deleted when
     * an Upload is reprocessed.
     */
    @Field("testrecords")
    @DBRef
    private List<TestRecord> testRecords = new LinkedList<>();

    @PersistenceConstructor
    public Upload() { }

    public Upload(String zipLocation, String hash, String pkgName, Project project, String branchName,
                  String authorEmail, String authorName, Date uploadMs) {
        this.zipLocation = zipLocation;
        this.hash = hash;
        this.pkgName = pkgName;
        this.branchName = branchName;
        this.authorEmail = authorEmail;
        this.authorName = authorName;
        this.project = project;
        this.uploadMs = uploadMs;
        this.jobStatus = UploadService.UPLOAD_STATUS_QUEUED;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Upload upload = (Upload) o;
        return id.equals(upload.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
