package com.mobileenerlytics.eagle.testerpro.resource;

import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Controller
@Path("api/about")
public class AboutResource {
    @Context
    HttpHeaders httpHeaders;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value="Information about the server",
            notes="curl -u demo:password \"https://tester.mobileenerlytics.com/api/about\""
    )
    public Response Get() {
        Map<String, Object> map = new HashMap<>();
        String version = getClass().getPackage().getImplementationVersion();
        map.put("version", version);
        return Response.ok().entity(map).build();
    }
}
