package com.mobileenerlytics.eagle.testerpro.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "Alert")
@Getter @Setter @NoArgsConstructor
public class Alert {
    @Id
    private String id;

    @DBRef
    @Field("project")
    private Project project;

    @Field("testName")
    String testName;

    @Field("branchName")
    private String branchName;

    @Field("endMs")
    private Date endMs;

    @Field("updatedMs")
    private Date updatedMs;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Alert alert = (Alert) o;
        return id.equals(alert.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
