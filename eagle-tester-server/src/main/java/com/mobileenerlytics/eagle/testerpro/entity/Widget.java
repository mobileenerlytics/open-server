package com.mobileenerlytics.eagle.testerpro.entity;

import com.mobileenerlytics.eagle.testerpro.exceptions.InvalidWidgetException;
import com.mobileenerlytics.eagle.testerpro.util.Constants;
import org.springframework.lang.Nullable;

public class Widget {
    public Widget(String type, String metric, String firstBranchName, @Nullable String firstCommitHash, String secondBranchName,
                  @Nullable String secondCommitHash) throws InvalidWidgetException {
        if(metric == null)
            metric = "energy";
        if(!Constants.validWidgetTypes.contains(type))
            throw new InvalidWidgetException("type", type);

        if(!Constants.validMetricTypes.contains(metric))
            throw new InvalidWidgetException("metric", metric);

        this.type = type;
        this.metric = metric;
        this.firstBranchName = firstBranchName;
        this.firstCommitHash = firstCommitHash;
        this.secondBranchName = secondBranchName;
        this.secondCommitHash = secondCommitHash;
    }

    public final String type;

    public final String metric;

    public final String firstBranchName;

    @Nullable
    public final String firstCommitHash;

    public final String secondBranchName;

    @Nullable
    public final String secondCommitHash;
}
