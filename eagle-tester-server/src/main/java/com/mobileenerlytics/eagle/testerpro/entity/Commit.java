package com.mobileenerlytics.eagle.testerpro.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Document(collection = "Commit")
@ApiModel(value="Commits Model", description = "Model for the commits API output")
@CompoundIndexes({
    @CompoundIndex(name = "branch_hash", def = "{'branch.$id' : 1, 'hash': 1}", unique = true),
    @CompoundIndex(name = "project_updatedMs", def = "{'project.$id' : 1, 'updatedMs': -1}")
})
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter @Setter
public class Commit implements Comparable<Commit> {

    @Id
    @JsonProperty("_id")
    private String id;

    @Field("hash")
    private String hash;

    @DBRef
    @Field("contributor")
    private Contributor contributor;

    @JsonIgnore
    @Field("branch")
    @DBRef
    private Branch branch;

    @Field("branchName")
    private String branchName;

    @JsonIgnore
    @Field("project")
    @DBRef
    private Project project;

    @Field("updatedMs")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date updatedMs;

    @JsonProperty(value = "testRecordSet")
    @Field("testStats")
    @DBRef
    private Set<TestStats> testStats = new HashSet<>();

    @PersistenceConstructor
    public Commit() {

    }

    public Commit(String hash, Branch branch, Contributor contributor, Date updatedMs) {
        this.hash = hash;
        this.branch = branch;
        this.branchName = branch.getBranchName();
        this.contributor = contributor;
        this.project = branch.getProject();
        this.updatedMs = updatedMs;
    }

    @Override
    public int compareTo(Commit o) {
        // Higher updateMs should appear first in the list
        return o.updatedMs.compareTo(updatedMs);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Commit commit = (Commit) o;
        return id.equals(commit.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
