package com.mobileenerlytics.eagle.testerpro.exceptions;

public class InsufficientDataException extends EagleException {
    public InsufficientDataException(String name) {
        super(name);
    }
}
