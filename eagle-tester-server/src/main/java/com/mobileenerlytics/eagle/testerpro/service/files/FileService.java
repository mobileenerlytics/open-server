package com.mobileenerlytics.eagle.testerpro.service.files;

import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Service
public abstract class FileService {
    protected static long chunkSize = 1024 * 1024;
    public abstract void uploadFolder(String testrecordId, File folder) throws IOException;
    public abstract Response.ResponseBuilder getFiles(String uri, String range) throws IOException;

    public class BoundedInputStream extends InputStream {
        private final InputStream stream;
        private long length;

        public BoundedInputStream(InputStream stream, long length) {
            this.stream = stream;
            this.length = length;
        }

        @Override public int read() throws IOException {
            return (length-- <= 0) ? -1 : stream.read();
        }
    }
}
