package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import com.mobileenerlytics.eagle.testerpro.entity.Project;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Slf4j
@Service
public class BranchService {

    @Autowired
    private BranchRepository repo;

    @Autowired
    private CommitService commitService;


    public Optional<Branch> queryOneByNameAndProject(String branchName, Project project) {
        return repo.findByBranchNameAndProject(branchName, project);
    }

    Branch queryOneOrCreate(String branchName, Date updatedMs, Project project) {
        Optional<Branch> optBranch = queryOneByNameAndProject(branchName, project);
        if(optBranch.isPresent())
            return optBranch.get();
        Branch branch = new Branch(branchName, updatedMs, project);
        return repo.save(branch);
    }

    @Transactional
    Branch addTest(Branch branch, String testName) throws DeletedException {
        // refresh branch, as the passed in branch might be stale
        Optional<Branch> optBranch = repo.findById(branch.getId());
        if(!optBranch.isPresent())
            throw new DeletedException(Branch.class);
        branch = optBranch.get();
        Map<String, Integer> testCounter = branch.getTestCounter();
        if(!testCounter.containsKey(testName)) {
            testCounter.put(testName, 0);
        }
        testCounter.put(testName, 1 + testCounter.get(testName));
        branch.setTestCounter(testCounter);
        return repo.save(branch);
    }

    @Transactional
    void removeTest(Branch branch, String testName) throws DeletedException {
        // refresh branch, as the passed in branch might be stale
        Optional<Branch> optBranch = repo.findById(branch.getId());
        if(!optBranch.isPresent())
            throw new DeletedException(Branch.class);
        branch = optBranch.get();
        Map<String, Integer> testCounter = branch.getTestCounter();
        if(!testCounter.containsKey(testName))
            log.warn("Tried to remove, but Branch {}-{} didn't have test {}", branch.getId(), branch.getBranchName(), testName);
        else {
            testCounter.put(testName, testCounter.get(testName) - 1);
            if(testCounter.get(testName) <= 0)
                testCounter.remove(testName);
        }

        if(testCounter.isEmpty()) {
            repo.delete(branch);
            return;
        }

        branch.setTestCounter(testCounter);
        repo.save(branch);
    }

    public List<Branch> queryByBranchNameAndProject(String branchName, Project project, int count) {
        List<Branch> branches = repo.queryAllByBranchNameAndProject(branchName, project);
        fillCommits(project, branches, count);
        return branches;
    }

    public List<Branch> queryByProject(Project project, int count) {
        List<Branch> branches = repo.queryAllByProject(project);
        fillCommits(project, branches, count);
        return branches;
    }

    public List<Branch> queryByPrefix(Project project, String branchNamePrefix, int count) {
        List<Branch> branches = repo.queryAllByProjectAndBranchNameLike(project,branchNamePrefix);
        fillCommits(project, branches, count);
        return branches;
    }

    /**
     * Fills specified number of commits sorted by updatedMs in each branch
     * @param project Project to which all branches belong. This is necessary to pass here since
     *                branch.getProject is null in the branch projection
     * @param branches List of branches to fill in
     * @param count Number of commits to fill in each branch
     */
    private void fillCommits(Project project, List<Branch> branches, int count) {
        if(count <= 0)
            return;
        branches.parallelStream().forEach(b -> {
            SortedSet<Commit> commits = new TreeSet<>();
            commits.addAll(commitService.queryByProjectAndBranchName(project, b.getBranchName(), count));
            b.setCommits(commits);
        });
    }

}
