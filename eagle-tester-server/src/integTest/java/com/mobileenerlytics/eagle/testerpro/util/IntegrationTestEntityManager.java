package com.mobileenerlytics.eagle.testerpro.util;

import com.google.common.base.Verify;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

public class IntegrationTestEntityManager implements EntityResolver {
    public IntegrationTestEntityManager() {
    }

    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
        if (systemId.equals("http://mobileenerlytics.com/server/dtd/testdata.dtd")) {
            InputStream stream = IntegrationTestEntityManager.class.getClassLoader().getResourceAsStream("testdata.dtd");
            return new InputSource(stream);
        } else {
            Verify.verify(false, "Entity not found: public {} system {}", new Object[]{publicId, systemId});
            return null;
        }
    }
}
