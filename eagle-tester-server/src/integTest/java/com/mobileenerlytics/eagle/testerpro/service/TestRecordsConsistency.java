package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.TestRecord;
import org.junit.Assert;
import org.springframework.test.context.TestContext;

import java.util.List;

/**
 * This class runs after each test execution and verifies that {@link TestRecord} is consistent with other collections
 */
public class TestRecordsConsistency extends AbstractDataConsistency {
    @Override
    public void afterTestExecution(TestContext testContext) {
        List<TestRecord> testRecords = testRecordRepository.findAll();
        for(TestRecord testRecord: testRecords) {
            Assert.assertNotNull(testRecord.getCommit());

            Assert.assertNotNull(testRecord.getProject());

            Assert.assertNotNull(testRecord.getBranch());

            Assert.assertTrue(testRecord.getBranch().getTestCounter().containsKey(testRecord.getTestName()));
        }
        System.out.println("TestRecords CONSISTENT. Checked " + testRecords.size() + " testrecords");
    }
}
