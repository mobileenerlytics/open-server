package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Branch;
import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import com.mobileenerlytics.eagle.testerpro.entity.TestStats;
import org.junit.Assert;
import org.springframework.test.context.TestContext;

import java.util.*;

import static java.util.stream.Collectors.*;

/**
 * This class listens to test executions and makes sure that projects and branches are in a
 * consistent state.
 */
public class BranchConsistency extends AbstractDataConsistency {

    /**
     * Check consistency of each branch. Each branch must satisfy following properties:
     */
    @Override
    public void afterTestExecution(TestContext testContext) {
        List<Branch> branches = branchRepository.findAll();

        for(Branch branch : branches) {
            // branch must have a non-null project (which also mean it exist in the projects table)
            Assert.assertNotNull(branch.getProject());

            for(Commit commit: commitRepository.findAllByBranch(branch)) {
                // all commits in the commits set are non-null (which means exist in commits branch)
                Assert.assertNotNull(commit);

                // all commits in the commits set have branch pointing to me, and have same branch name as me
                Assert.assertEquals(branch.getBranchName(), commit.getBranchName());
                Assert.assertEquals(branch, commit.getBranch());
            }

            // The count of each test in tests map shall match with TestStats
            Map<String, Integer> tests = commitRepository.findAllByBranch(branch).stream()
                    .flatMap(c -> c.getTestStats().stream())
                    .collect(groupingBy(TestStats::getTestName, reducing(0, e -> 1, Integer::sum)));
            Assert.assertEquals(tests, branch.getTestCounter());
        }

        System.out.println("Branches CONSISTENT. Checked " + branches.size() + " branches");
    }
}
