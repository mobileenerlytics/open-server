package com.mobileenerlytics.eagle.testerpro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

/**
 * Abstract base class for implementing data consistency listeners. This class shall provide all the repositories to its
 * super classes.
 */
public abstract class AbstractDataConsistency extends AbstractTestExecutionListener {
    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    BranchRepository branchRepository;

    @Autowired
    CommitRepository commitRepository;

    @Autowired
    ContributorRepository contributorRepository;

    @Autowired
    TestRecordRepository testRecordRepository;

    @Autowired
    TestStatsRepository testStatsRepository;

    public void beforeTestClass(TestContext testContext) {
        testContext.getApplicationContext()
                .getAutowireCapableBeanFactory()
                .autowireBean(this);
    }
}
