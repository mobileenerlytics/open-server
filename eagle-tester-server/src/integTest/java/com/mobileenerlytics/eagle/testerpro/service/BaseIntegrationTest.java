package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.*;
import com.mobileenerlytics.eagle.testerpro.exceptions.DeletedException;
import com.mobileenerlytics.eagle.testerpro.util.IntegrationTestEntityManager;
import com.mobileenerlytics.eagle.testerpro.util.XmlHelper;
import com.mongodb.Mongo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestExecutionListeners(
        listeners = {BranchConsistency.class, CommitConsistency.class, TestStatsConsistency.class, TestRecordsConsistency.class},
        mergeMode = MERGE_WITH_DEFAULTS)
@TestPropertySource("/test.properties")
//@Transactional
public class BaseIntegrationTest {
    @Autowired
    TestRecordService testRecordService;

    @Autowired
    TestRecordRepository testRecordRepository;

    @Autowired
    ProjectService projectService;

    @Autowired
    private Mongo mongo;

    @Autowired
    private MongoDbFactory mongoDbFactory;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private CommitRepository commitRepository;

    @Autowired
    private CommitService commitService;

    @Before
    public void setup() throws IOException, SAXException, ParserConfigurationException, DeletedException {
        cleanDB();
        readXmlAndFillDB();
    }

    private void cleanDB() {
         mongo.dropDatabase(mongoDbFactory.getDb().getName());

        // Creating collections manually here, otherwise setup crashes saying "can't create collection in a
        // multi-document transaction
        for(Class entity: new Class[]{Upload.class, Project.class, Branch.class, Commit.class, Contributor.class,
                TestStats.class, TestRecord.class}) {
            mongoTemplate.createCollection(entity);
        }
    }

    private void readXmlAndFillDB() throws ParserConfigurationException, IOException, SAXException, DeletedException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setValidating(false);
        dbFactory.setIgnoringComments(true);
        dbFactory.setIgnoringElementContentWhitespace(true);
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        dBuilder.setEntityResolver(new IntegrationTestEntityManager());
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("testdata.xml");
        Document doc = dBuilder.parse(inputStream);
        NodeList root = doc.getChildNodes();

        Node data = XmlHelper.getNode("data", root);
        NodeList projects = XmlHelper.getNode("projects",
                data.getChildNodes()).getChildNodes();
        for(int i = 0; i < projects.getLength(); i ++)
            parseProject(projects.item(i));
    }

    private Project parseProject(Node projectNode) throws DeletedException {
        String projectName = XmlHelper.getNodeAtt("projectName", projectNode);
        String userId = XmlHelper.getNodeAtt("userId", projectNode);
        final Project project = projectService.queryOneOrCreate(projectName, userId);

        NodeList branchNodes = projectNode.getChildNodes();
        for(int i = 0; i < branchNodes.getLength(); i ++)
            parseBranch(project, branchNodes.item(i));

        return project;
    }

    private void parseBranch(final Project project, Node branchNode) throws DeletedException {
        String branchName = XmlHelper.getNodeAtt("branchName", branchNode);
        NodeList commitNodes = branchNode.getChildNodes();
        for(int i = 0; i < commitNodes.getLength(); i ++)
            parseCommit(project, branchName, commitNodes.item(i));
    }

    private void parseCommit(Project project, String branchName, Node commitNode) throws DeletedException {
        String authorEmail = XmlHelper.getNodeAtt("author_email", commitNode);
        String authorName = XmlHelper.getNodeAtt("author_name", commitNode);
        String hash = XmlHelper.getNodeAtt("hash", commitNode);

        NodeList testRecords = commitNode.getChildNodes();
        for(int i = 0; i < testRecords.getLength(); i ++) {
            Upload upload = new Upload("zip", hash, "pkgName", project, branchName,
                    authorEmail, authorName, new Date());
            parseTestRecord(upload, testRecords.item(i));
        }
    }

    private TestRecord parseTestRecord(Upload upload, Node testRecordNode) throws DeletedException {
        String durationMs = XmlHelper.getNodeAtt("durationMs", testRecordNode);
        Properties properties = new Properties();
        properties.setProperty("startTime", "0");
        properties.setProperty("endTime", durationMs);

        String testname = XmlHelper.getNodeAtt("testname", testRecordNode);

        Node threadCompEnergiesNode = XmlHelper.getNode("threadcompenergies", testRecordNode.getChildNodes());
        Set<ThreadCompEnergy> threadCompEnergies = createThreadCompEnergies(threadCompEnergiesNode, durationMs);

        Node eventsNode = XmlHelper.getNode("events", testRecordNode.getChildNodes());
        Set<AggregateEvent> events = createEvents(eventsNode);

        return testRecordService.create(testname, upload, threadCompEnergies, events, properties);
    }

    private Set<ThreadCompEnergy> createThreadCompEnergies(Node threadCompEnergiesNode, String durationMs) {
        Set<ThreadCompEnergy> ret = new HashSet<>();
        NodeList threadCompEnergies = threadCompEnergiesNode.getChildNodes();
        for(int i = 0; i < threadCompEnergies.getLength(); i ++) {
            Node threadCompEnergyNode = threadCompEnergies.item(i);
            String process = XmlHelper.getNodeAtt("process", threadCompEnergyNode);
            String thread = XmlHelper.getNodeAtt("thread", threadCompEnergyNode);
            String component = XmlHelper.getNodeAtt("component", threadCompEnergyNode);
            double energy = XmlHelper.getNodeAttDouble("energy", threadCompEnergyNode);
            ret.add(new ThreadCompEnergy(process, thread, component, energy, Long.parseLong(durationMs)));
        }
        return ret;
    }

    private Set<AggregateEvent> createEvents(Node eventsNode) {
        Set<AggregateEvent> ret = new HashSet<>();
        if(eventsNode == null)
            return ret;
        NodeList events = eventsNode.getChildNodes();
        for(int i = 0; i < events.getLength(); i ++) {
            Node eventNode = events.item(i);
            String process = XmlHelper.getNodeAtt("process", eventNode);
            String eventname = XmlHelper.getNodeAtt("eventname", eventNode);
            String name = XmlHelper.getNodeAtt("name", eventNode);
            double durationMs = XmlHelper.getNodeAttDouble("durationMs", eventNode);
            ret.add(new AggregateEvent(process, eventname, name, durationMs, durationMs*durationMs));
        }
        return ret;
    }

    /**
     * This test is empty but does serve an essential purpose. The test starts with 'aa' coming first alphabetically,
     * and thus shall run before all the other tests in this class. This lets all the consistency listeners to run
     * and make sure that the initial setup was done in a consistent state
     */
    @Test
    public void aa_noop_first_test() {

    }

    /**
     * Delete all test records in a commit
     */
    @Test
    public void deleteTestRecordsOfCommit() {
        List<TestRecord> testRecords = testRecordRepository.findAll();

        // Group Test records by commit
        Map<Commit, List<TestRecord>> groupedTests = testRecords.stream()
                .collect(groupingBy(TestRecord::getCommit));

        groupedTests.keySet().stream().limit(1) // select all test records belonging to first commit
                .forEach(commit -> {
                    groupedTests.get(commit).stream().forEach(testRecord -> {
                        try {
                            testRecordService.delete(testRecord);
                        } catch (DeletedException e) {
                            Assert.fail();
                        }
                    });
                });  // delete test records
    }

    /**
     * Deletes a test record
     */
    @Test
    public void deleteATestRecord() {
        testRecordRepository.findAll().stream().limit(1).forEach(testRecord -> {
            try {
                testRecordService.delete(testRecord);
            } catch (DeletedException e) {
                Assert.fail();
            }
        });
    }

    /**
     * Deletes a commit
     */
    @Test
    public void deleteACommit() {
        commitRepository.findAll().stream().limit(1)
                .forEach(commit -> {
                    try {
                        commitService.delete(commit);
                    } catch (DeletedException e) {
                        Assert.fail();
                    }
                });
    }
//
//    /**
//     * Deletes a test stats
//     */
//    @Test
//    public void deleteATestStats() {
//        testStatsRepository.findAll().stream().limit(1).forEach(testStatsService::deleteTestStats);
//    }
}
