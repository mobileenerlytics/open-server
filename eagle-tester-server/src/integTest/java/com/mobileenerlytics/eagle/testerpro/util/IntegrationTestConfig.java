package com.mobileenerlytics.eagle.testerpro.util;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;

@Configuration
@ComponentScan({"com.mobileenerlytics", "com.att.aro"})
//@TestPropertySource("/test.properties")
public class IntegrationTestConfig {
}
