package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.Commit;
import org.junit.Assert;
import org.springframework.test.context.TestContext;

import java.util.List;

/**
 * This class listens to test executions and asserts consistency about contributors
 */
public class CommitConsistency extends AbstractDataConsistency {
    /**
     * Each commit in the db shall have a non-null contributor, project and branch (which means that contributor,
     * project and branch shall exist in their respective collections).
     */
    @Override
    public void afterTestExecution(TestContext testContext) {
        List<Commit> commits = commitRepository.findAll();
        for(Commit commit: commits) {
            Assert.assertNotNull(commit.getContributor());

            Assert.assertNotNull(commit.getBranch());
            // Branchname in commit should match with branchname of the linked branch
            Assert.assertEquals(commit.getBranch().getBranchName(), commit.getBranchName());

            Assert.assertNotNull(commit.getProject());
            // Project in commit should match with project in the linked branch
            Assert.assertEquals(commit.getBranch().getProject(), commit.getProject());

            // Commit should be in the list of commits of its linked branch
//            Assert.assertTrue(commit.getHash() + " was not found in its branch's commits",
//                    commit.getBranch().getCommits().contains(commit));
        }
        System.out.println("Contributor CONSISTENT. Checked " + commits.size() + " commits");
    }
}
