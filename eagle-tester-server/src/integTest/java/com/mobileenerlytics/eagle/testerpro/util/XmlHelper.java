package com.mobileenerlytics.eagle.testerpro.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlHelper {
    private static Logger logger = LoggerFactory.getLogger(com.mobileenerlytics.eprof.stream.util.XmlHelper.class.getSimpleName());

    public static Node getNode(String tagName, NodeList nodes) {
        for (int i = nodes.getLength() - 1; i >= 0; i--) {
            Node node = nodes.item(i);
            logger.debug("getNode: node name = {}", node.getNodeName());
            if (node.getNodeName().equalsIgnoreCase(tagName)) {
                return node;
            }
        }
        return null;
    }

    public static String getNodeAtt(String attrName, Node node) {
        NamedNodeMap attrs = node.getAttributes();
        return attrs.getNamedItem(attrName).getNodeValue();
    }

    public static int getNodeAttInt(String attrName, Node node) {
        String nodeAtt = getNodeAtt(attrName, node);
        if (nodeAtt == null || nodeAtt.equals(""))
            throw new IllegalArgumentException();
        return Integer.parseInt(nodeAtt);
    }

    public static double getNodeAttDouble(String attrName, Node node) {
        String nodeAtt = getNodeAtt(attrName, node);
        if (nodeAtt == null || nodeAtt.equals(""))
            throw new IllegalArgumentException();
        return Double.parseDouble(nodeAtt);
    }

    public static Long getNodeAttLong(String attrName, Node node) {
        String nodeAtt = getNodeAtt(attrName, node);
        if (nodeAtt == null || nodeAtt.equals(""))
            throw new IllegalArgumentException();
        return Long.parseLong(nodeAtt);
    }
}
