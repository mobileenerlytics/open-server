package com.mobileenerlytics.eagle.testerpro.service;

import com.mobileenerlytics.eagle.testerpro.entity.TestRecord;
import com.mobileenerlytics.eagle.testerpro.entity.TestStats;
import com.mobileenerlytics.eagle.testerpro.util.avging.IAvgEntry;
import org.junit.Assert;
import org.springframework.test.context.TestContext;

import java.util.*;

import static java.util.stream.Collectors.*;

/**
 * This class runs after each test execution and verifies that {@link TestStats} is consistent with other collections
 */
public class TestStatsConsistency extends AbstractDataConsistency {
    @Override
    public void afterTestExecution(TestContext testContext) {
        List<TestStats> testStatsList = testStatsRepository.findAll();
        for(TestStats testStats : testStatsList) {
            // Test stats must have a non-null commit
            Assert.assertNotNull(testStats.getCommit());

            // Test stats must be present in the list of test stats in linked commit
            Assert.assertTrue(testStats.getCommit().getTestStats().contains(testStats));

            // This queries for test records in testRecordRepository. If the testRecord doesn't
            // exist, this stream will crash
            List<TestRecord> testRecords = testStats.getTestRecordSet().stream().map(testRecordRepository::findById)
                    .map(Optional::get)
                    .collect(toList());

            // Test record must have same test name as the test stats
            testRecords.stream().forEach(t -> Assert.assertEquals(testStats.getTestName(), t.getTestName()));

            int count = testStats.getTestRecordSet().size();

            // Total energy of all test records == testStats.energy * testStats.count
            double totalEnergy = testRecords.stream().mapToDouble(TestRecord::getEnergy).sum();
            Assert.assertEquals(totalEnergy, testStats.getEnergy() * count, 0.1);

            // (Total energy)^2 of all test records == testStats.energySquared * testStats.count
            double totalEnergySquared = testRecords.stream().mapToDouble(t -> t.getEnergy() * t.getEnergy()).sum();
            Assert.assertEquals(totalEnergySquared, testStats.getEnergySquared() * count, 0.1);

            // Total test duration of all test records == testStats.testDuration * testStats.count
            long totalDurationMs = testRecords.stream().mapToLong(TestRecord::getTestDurationMS).sum();
            Assert.assertEquals(totalDurationMs, testStats.getTestDurationMS() * count);

            // Total power of all test records == testStats.power * testStats.count
            double totalPower = testRecords.stream().mapToDouble(t -> 3600 * t.getEnergy() / t.getTestDurationMS()).sum();
            Assert.assertEquals(totalPower, testStats.getPower() * count, 0.1);

            // Total power of all test records == testStats.power * testStats.count
            double totalPowerSquared = testRecords.stream().mapToDouble(t ->
                    (3600 * t.getEnergy() / t.getTestDurationMS()) * (3600 * t.getEnergy() / t.getTestDurationMS())).sum();
            Assert.assertEquals(totalPowerSquared, testStats.getPowerSquared() * count, 0.1);

            // ThreadCompEnergies in TestRecords should add up to count * ThreadCompEnergies in test stats
            Map<String, double[]> recordTCE= sumAndReturn(4, testRecords.stream()
                    .flatMap(tr -> tr.getThreadCompEnergies().stream())
                    .collect(toList()));
            Map<String, double[]> statTCE = multiplyAndReturn(testStats.getThreadCompEnergies(), count);
            assertEquals(statTCE, recordTCE);

            // ThreadUnit in TestRecords should add up to count * ThreadUnit in test stats
            Map<String, double[]> recordThreadUnit= sumAndReturn(4, testRecords.stream()
                    .flatMap(tr -> tr.getThreadUnit().stream())
                    .collect(toList()));
            Map<String, double[]> statThreadUnit = multiplyAndReturn(testStats.getThreadUnit(), count);
            assertEquals(statThreadUnit, recordThreadUnit);

            // ComponentUnit in TestRecords should add up to count * ComponentUnit in test stats
            Map<String, double[]> recordComponentUnit= sumAndReturn(4, testRecords.stream()
                    .flatMap(tr -> tr.getComponentUnit().stream())
                    .collect(toList()));
            Map<String, double[]> statComponentUnit = multiplyAndReturn(testStats.getComponentUnit(), count);
            assertEquals(statComponentUnit, recordComponentUnit);

            // AggregateEvent in TestRecords should add up to count * AggregateEvent in test stats
            Map<String, double[]> recordEvents= sumAndReturn(2, testRecords.stream()
                    .flatMap(tr -> tr.getEvents().stream())
                    .collect(toList()));
            Map<String, double[]> statEvents = multiplyAndReturn(testStats.getEvents(), count);
            assertEquals(statEvents, recordEvents);
        }
        System.out.println("TestStats CONSISTENT. Checked " + testStatsList.size() + " teststats");
    }

    static Map<String, double[]> sumAndReturn(int arrayLength, List<? extends IAvgEntry> entrySet) {
        return entrySet.stream().collect(
                groupingBy(IAvgEntry::getKey, mapping(IAvgEntry::getValues, reducing(new double[arrayLength], (u, v) -> {
                    double result[] = new double[u.length];
                    Arrays.setAll(result, i -> u[i] + v[i]);
                    return result;
                }))));
    }

    static Map<String, double[]> multiplyAndReturn(Map<String, ? extends IAvgEntry> entryMap, double factor) {
        Map<String, double[]> ret = new HashMap<>();
        for(Map.Entry<String, ? extends IAvgEntry> entry : entryMap.entrySet()) {
            ret.put(entry.getKey(), Arrays.stream(entry.getValue().getValues()).map(d -> d * factor).toArray());
        }
        return ret;
    }

    static void assertEquals(Map<String, double[]> expected, Map<String, double[]> actual) {
        Assert.assertEquals(expected.size(), actual.size());
        for(Map.Entry<String, double[]> entry: expected.entrySet()) {
            Assert.assertTrue(actual.containsKey(entry.getKey()));
            double[] expectedValue = entry.getValue();
            double[] actualValue = actual.get(entry.getKey());
            Assert.assertEquals(expectedValue.length, actualValue.length);
            for(int i = 0 ; i < expectedValue.length; i ++)
                Assert.assertEquals(expectedValue[i], actualValue[i], 0.1);
        }
    }
}
