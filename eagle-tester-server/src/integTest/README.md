This folder contains all the integration tests for `eagle-tester-server`.

### Integration testing setup

Before running any integration test, 
[BaseIntegrationTest](java/com/mobileenerlytics/eagle/testerpro/service/BaseIntegrationTest)
cleans up any existing data in test database and reads a data xml file 
to create a fresh instance of mongo database. The XML data format is 
defined in [DTD file](resources/testdata.dtd).

### Consistency listeners
[BaseIntegrationTest](java/com/mobileenerlytics/eagle/testerpro/service/BaseIntegrationTest)
also registers some custom 
[TestExecutionListener](https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/test/context/TestExecutionListener.html) 
defined as the `*Consistency` classes. 
These execution listeners run after every test and make sure 
that the database is in a consistent state. Each listener worries about 
a particular kind of consistency (consistency between just two collections 
for example).

As a good design, these listeners shall not make any assumptions about 
the actual data in the database or about the tests. They shall just check
the relationships in the data and assert consistency. 

Also, it is super important that these listeners only use `repositories` 
and not `services` to query data in db. The `repositories` are provided 
by Spring and can be assumed to be correct; `services` however are 
written by us and are the subject of these integration tests.
