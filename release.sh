echo "WARNING: Server release is not as clean cut as it should be!
1. Pull origin/master into master branch locally
2. Pull origin/release into release branch locally
3. Merge master into release
4. login the docker

Please verify that you're running this from release branch and that the stripe tokens are live instead of test."
  
read -p "Press any key to continue.." -n1 -s

echo "Deploying debian and docker images"
./scripts/deploy.sh

echo "For next development iteration, send PR from release branch to master branch"
